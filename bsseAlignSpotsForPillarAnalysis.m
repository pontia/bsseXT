function bsseAlignSpotsForPillarAnalysis(vImarisApplication, readme)
% Corrects position of spots at two z levels ('bottom' and 'top'), where
% the positions at the bottom define a rigid grid. The XTension estimates
% the grid position and uses displacement between original spots positions
% at the bottom and the estimated grid to correct the positions of the top
% as well. The top positions have additional displacement caused by other
% sources which must be quantified (externally) once grid drifts have been
% corected for.
%
% Copyright Aaron Ponti, BSSE, ETH Zurich, 2012
%
% <CustomTools>
%  <Menu>
%   <Submenu name="BSSE">
%    <Submenu name="Spots Functions">
%     <Submenu name="Align Spots for Pillar Analysis">
%      <Item name="Run" icon="Matlab" tooltip="Align Spots for Pillar Analysis.">
%       <Command>MatlabXT::bsseAlignSpotsForPillarAnalysis(%i, 0)</Command>
%      </Item>
%      <Item name="Help" icon="Matlab" tooltip="Show XTension help.">
%       <Command>MatlabXT::bsseAlignSpotsForPillarAnalysis(%i, 1)</Command>
%      </Item>
%     </Submenu>
%    </Submenu>
%   </Submenu>
%  </Menu>
% </CustomTools>

if nargin == 1
    readme = 0;
end

if readme == 1
    showReadMe();
    return;
end

% Instantiate an IceImarisConnector object
conn = IceImarisConnector(vImarisApplication);

% Is there something loaded?
if isempty(conn.mImarisApplication.GetDataSet())
    return
end

% =========================================================================
%
% GET NEEDED SPOTS OBJECTS
%
% =========================================================================

% Get the Spots objects at the root level
spots = conn.getAllSurpassChildren(0, 'Spots');
nSpots = numel(spots);
if nSpots < 2
    uiwait(errordlg('You need two Spots objects for this analysis.', ...
        'Error', 'modal'));
    return;
end

% Build a list of Spots names
names = cell(1, nSpots);
for i = 1 : nSpots
    names{i} = char(spots{i}.GetName());
end

% Ask the user to choose the BOTTOM Spots
[s, v] = listdlg( 'PromptString','Select the spots at the BOTTOM (grid):',...
    'SelectionMode', 'single', 'ListSize', [200 150], ...
    'ListString', names);
if v == 0
    return;
end
spotsBottom = spots{s};
spotsBottomName = names{s};

% If we had exactly two Spots, we know that the other one must be the TOP
% one; otherwise, we ask
if nSpots == 2
    spots(s) = [];
    spotsTop = spots{ 1 };
    spotsTopName = char(spotsTop.GetName());
else
    % Ask the user to choose the TOP Spots
    [s, v] = listdlg( 'PromptString','Select the spots at the TOP:',...
    'SelectionMode', 'single', 'ListSize', [200 150], ...
    'ListString', names);
    if v == 0
        return;
    end
    spotsTop = spots{s};
    spotsTopName = names{s};
end
clear 'spots';

% =========================================================================
%
% ESTIMATE THE MIN DISTANCE BETWEEN SPOTS: WE WILL THEN USE A CONSERVATIVE
% THRESHOLD BASED ON IT TO ESTIMATE THE GRID POSITIONS IN THE NEXT STEP
%
% =========================================================================

% Number of timepoints
nTimepoints = conn.mImarisApplication.GetDataSet().GetSizeT();

% Get all Spots coordinates, indices and time points
spotsBottomValues      = spotsBottom.Get();
spotsBottomPositions   = spotsBottomValues.mPositionsXYZ;
spotsBottomTimeIndices = spotsBottomValues.mIndicesT;
spotsBottomRadii       = spotsBottomValues.mRadii; 
spotsTopValues         = spotsTop.Get();
spotsTopPositions      = spotsTopValues.mPositionsXYZ;
spotsTopTimeIndices    = spotsTopValues.mIndicesT;
spotsTopRadii          = spotsTopValues.mRadii; 

allSpotsPosBottom = cell(1, nTimepoints);
allSpotsPosTop   = cell(1, nTimepoints);

% We estimate a good distance threshold based on the minimum distance
% between spots over all time points
minDistanceBetweenBottomSpots = Inf;

for time = 1 : nTimepoints
   
    % Extract bottom Spots positions for current time point
    indxBottom = find(spotsBottomTimeIndices == (time - 1));
    indxTop    = find(spotsTopTimeIndices == (time - 1));

    % Store the coordinates
    if ~isempty(indxBottom)
        % Get the coordinates
        currentSpotBottomPositions = spotsBottomPositions(indxBottom, :);
        allSpotsPosBottom{time} = currentSpotBottomPositions;
        % Find the min distance and store the min distance if it is the
        % shortest so far
        minD = findMinLateralDistance(currentSpotBottomPositions);
        if minD < minDistanceBetweenBottomSpots
            minDistanceBetweenBottomSpots = minD;
        end
    end
    
    if ~isempty(indxTop)
        currentSpotTopPositions = spotsTopPositions(indxTop, :);
        allSpotsPosTop{time}    = currentSpotTopPositions;
    end    

end

% Set the distance threshold as 25% of the min distance between spots
distanceThreshold = 0.25 * minDistanceBetweenBottomSpots;

% =========================================================================
%
% ESTIMATE GRID POSITION
%
% =========================================================================

% BOTTOM SPOTS SHOULD COVER THE GRID AT ALL TIMES, BUT IF SOME SPOTS ARE
% MISSING WE SHOULD STILL BE ABLE TO FIND THE GRID POSITIONS. ALSO IF
% SPURIOUS SPOTS ARE PRESENT, WE WILL TRY TO FIND THEM AND IGNORE THEM
% =========================================================================

% Allocate space to store the results
grid = cell( round(size(spotsBottomPositions, 1) / nTimepoints), 1);
for i = 1 : size(grid, 1)
    grid{i} = nan(nTimepoints, 3);
end

% Get the bottom spot positions for the first time point
spotBottomPositionsCurrent = ...
    spotsBottomPositions(spotsBottomTimeIndices == 0, :);

% We match each of the positions in the first time point with the closest
% positions in all subsequent time points
for currentPosIndx = 1 : size(spotBottomPositionsCurrent, 1)
    
    % Current grid position to investigate
    posCurrent = spotBottomPositionsCurrent(currentPosIndx, :);
    
    % Store it
    grid{currentPosIndx}(1, 1 : 3) = posCurrent;
    
    % Find the same position in all future time points
    for nextTime = 2 : nTimepoints
        
        spotBottomPositionsNext = spotsBottomPositions(...
            spotsBottomTimeIndices == (nextTime - 1), :);
        
        % Get the closest spot to posCurrent in the next time point
        D = sqrt( ...
            (posCurrent(1, 1) - ...
            spotBottomPositionsNext(:, 1)).^2 + ...
            (posCurrent(1, 2) - ...
            spotBottomPositionsNext(:, 2)).^2);
        indx = find(D == min(D(:)));
        indx= indx(1);

        % Make sure the distance is not larger than the threshold. If no
        % spots is found within the search radius, the position will be 
        % [NaN NaN NaN], and we will remove it before we estimate the grid
        % position.
        if D(indx) <= distanceThreshold
            % Store
            grid{currentPosIndx}(nextTime, 1 : 3) = ...
                spotBottomPositionsNext(indx, :);
        end
        
    end
    
end

% =========================================================================
%
% ESTIMATE THE GRID POSITIONS
%
% =========================================================================

% Number of grid positions
nGridPos = size(grid, 1);

% Allocate space for the positions
gridSpotsPositions = nan(nGridPos, 3);

for i = 1 : nGridPos
    pos = grid{i};
    pos(isnan(pos(:, 1)), :) = [];
    pos = median(pos, 1);
    gridSpotsPositions(i, :) = pos;
end

% =========================================================================
%
% STORE DISPLACEMENTS
%
% =========================================================================

displ = cell(size(grid));
for i = 1 : size(grid, 1)
    displ{i} = nan(nTimepoints, 5);
end

for i = 1 : nTimepoints
   
    % Get spot positions for current timepoint
    spotsBottomPositionsPerTimepoint = spotsBottomPositions( ...
        spotsBottomTimeIndices == (i - 1), :);

    for currentSpot = 1 : size(spotsBottomPositionsPerTimepoint, 1)
        % For each of the spots, we calculate and store the displacement
        % to the closes grid position
        d = zeros(nGridPos, 2);
        d(:, 1) = gridSpotsPositions(:, 1) - ...
            spotsBottomPositionsPerTimepoint(currentSpot, 1);
        d(:, 2) = gridSpotsPositions(:, 2) - ... 
                spotsBottomPositionsPerTimepoint(currentSpot, 2);
        
        D = sqrt(sum(d.^2, 2));
        indx = find(D == min(D(:)));
        indx= indx(1);
        
        % Store the spot position and the displacement
        displ{i}(currentSpot, 1 : 5) = ...
            [spotsBottomPositionsPerTimepoint(currentSpot, :) d(indx, :)];
            
    end
    
end


% =========================================================================
%
% CREATE A NEW SPOTS OBJECT WITH THE GRID POSITIONS
%
% =========================================================================

% Replicate the positions, time indices and radii over all time points
nSpots = size(gridSpotsPositions, 1);
gridSpotsPositions   = repmat(gridSpotsPositions, nTimepoints, 1);
gridSpotsTimeIndices = zeros(nTimepoints * nSpots, 1);
for i = 0 : nTimepoints - 1
    start = i * nSpots + 1;
    stop  = (i + 1) * nSpots;
    gridSpotsTimeIndices(start : stop) = i;
end
gridSpotsRadii = spotsBottomRadii(1) .* ones(nTimepoints * nSpots, 1);

% Add to surpass
conn.createAndSetSpots(gridSpotsPositions, gridSpotsTimeIndices, ...
    gridSpotsRadii, ['Grid (', spotsBottomName, ')'], [1 0 0 0]);

% =========================================================================
%
% CORRECT THE TOP POSITIONS USING THE ESTIMATED GRID
%
% =========================================================================

% Allocate space for the corrected positions
correctedSpotsTopPositions = spotsTopPositions;

for i = 1 : nTimepoints

    % Get spot positions at the top for current time point
    spotsTopPositionsForTimepoint = spotsTopPositions( ...
        spotsTopTimeIndices == (i - 1), :);
    
    % Allocate space for the corrected positions
    correctedSpotsTopPositionsForTimepoint = spotsTopPositionsForTimepoint;
    
    % Get the spot positions at the bottom for current time point
    spotsBottomPositionsForTimepoint = displ{i};
    
    % For each of the top spots, find the closest bottom spot, and apply
    % the same displacement (stored in 'displ') to bring it on the grid
    for currentPos = 1 : size(spotsTopPositionsForTimepoint, 1)
    
        D = sqrt( ...
            (spotsTopPositionsForTimepoint(currentPos, 1) - ...
            spotsBottomPositionsForTimepoint(:, 1)).^2 + ...
            (spotsTopPositionsForTimepoint(currentPos, 2) - ...
            spotsBottomPositionsForTimepoint(:, 2)).^2);
        indx = find(D == min(D(:)));
        indx= indx(1);
        
        % Get the displacement
        d = spotsBottomPositionsForTimepoint(indx, 4 : 5);
        
        % Store the corrected spot
        correctedSpotsTopPositionsForTimepoint(currentPos, 1 : 2) = ...
            spotsTopPositionsForTimepoint(currentPos, 1 : 2) + d;

    end
    
    % Store the corrected positions in the global array
    correctedSpotsTopPositions(spotsTopTimeIndices == (i - 1), :) = ...
        correctedSpotsTopPositionsForTimepoint;
   
end

% =========================================================================
%
% CREATE A NEW SPOTS OBJECT WITH THE CORRECTED TOP POSITIONS
%
% =========================================================================

% Add to surpass
conn.createAndSetSpots(correctedSpotsTopPositions, spotsTopTimeIndices, ...
    spotsTopRadii, ['Corrected ', spotsTopName], [0 0 1 0]);

% =========================================================================
%
% FUNCTIONS
%
% =========================================================================

% Calculate minimum lateral distance between all positions in the set
function minD = findMinLateralDistance(pos)

minD = Inf;

nIter = size(pos, 1) - 1;
for i = 1 : nIter

    current = i + 1;
    % We only calculate the lateral distance (no z)
    D = sqrt( ...
        (pos(current : end, 1) - pos(i, 1)).^2 + ...
        (pos(current : end, 2) - pos(i, 2)).^2);
    
    currentMinD = min(D);
    if currentMinD < minD
        minD = currentMinD;
    end
   
end

% =========================================================================

function showReadMe()

tmpfile = [ getTmpOutputDir(), filesep, 'help.html' ];
fid = fopen(tmpfile, 'w');
if fid == -1
    uiwait(errordlg('Could not create help file.', 'Error', 'modal'));
    return;
end

txt = [ '<html><head></head><body>', ...
    '<h1>Align Spots for Pillar Analysis</h1>', ...
    '<p>This XTension is used to correct (image) drifts in time ', ...
    'series of acquisitions used to analyze the force exterted on ', ...
    'pillars by dividing/rounding cells.</p>', ...
    '<p>A Spots object with positions at the bottom of the pillars ', ...
    'and a Spots object with positions at the top of the pillar are ', ...
    'required.</p>', ...
    '<p>The Spots object at the bottom is used to estimate the grid ', ...
    'positions (all pillar should be represented by a spot in all ', ...
    'time points). The grid position is then used to correct the top ', ...
    'spot positions.</p>', ...
    '<p>The XTension creates two additional Spots object with their ', ...
    'coordinates corrected for the drift.</p>', ...
    '<p>Aaron Ponti, 2012/11/13</p>', ...
    '</body></html>'];

fprintf(fid, '%s', txt);

fclose(fid);

web(['file://',tmpfile], '-browser', '-noaddressbox');

% =========================================================================

function OUTPUTDIR = getTmpOutputDir

% Directory where to store the file
if isunix()
    OUTPUTDIR = '/tmp';
    return;
end

% In Windows we try the TMP folders
OUTPUTDIR=getenv('TMP');
if isempty(OUTPUTDIR)
    OUTPUTDIR=getenv('TEMP');
    if isempty(OUTPUTDIR)
        OUTPUTDIR=getenv('HOME');
        if isempty(OUTPUTDIR)
            OUTPUTDIR=pwd;
        end
    end
end
