function bsseAnalyzeTracksAndAssignToLabels(vImarisApplication)
% Analyze surface tracks and assigns every time point to a label.
%
% The label is defined as the index of the channel that has higher
% intensity at the position of the surface at any given time point.
% In theory, only one of the two channels has some signal, whereas the
% other should have an intensity of zero. But to accomodate for possible
% noise, we pick the channel with the highest intensity.
%
% Copyright Aaron Ponti, BSSE, ETH Zurich, 2020
%
% <CustomTools>
%  <Menu>
%   <Submenu name="BSSE">
%    <Submenu name="Surfaces Functions">
%     <Item name="Analyse tracks and assign to labels" icon="Matlab" tooltip="Analyse tracks and assign to labels.">
%      <Command>MatlabXT::bsseAnalyzeTracksAndAssignToLabels(%i)</Command>
%     </Item>
%    </Submenu>
%   </Submenu>
%  </Menu>
% </CustomTools>

% INITIALIZATION
% =========================================================================

% Instantiate an IceImarisConnector object
conn = IceImarisConnector(vImarisApplication);

% Is there something loaded?
if isempty(conn.mImarisApplication.GetDataSet())
    return
end

% Make sure that the selected object is a surface
allSurfaces = conn.getSurpassSelection('Surfaces');
if isempty(allSurfaces)
    
    % Search for surfaces
    candidateSurfaces = conn.getAllSurpassChildren(1, 'Surfaces');
    
    if numel(candidateSurfaces) == 0
        errordlg('Could not find any surface object to use for analysis.', ...
            'Error', 'modal');
        return;
    end
    
    if numel(candidateSurfaces) == 1
        allSurfaces = candidateSurfaces{1};
    else
        
        % Collect surfaces names
        surfaceNames = cell(1, numel(candidateSurfaces));
        for i = 1 : numel(candidateSurfaces)
            surfaceNames{i} = char(candidateSurfaces{i}.GetName());
        end

       % Ask the user to pick the surface to use
       [surfaceToUse, v] = listdlg( ...
           'PromptString', 'Select surface to use for analysis:', ...
           'SelectionMode', 'single', 'ListSize', [400 300], ...
           'ListString', surfaceNames);
       
       if v == 0
           return
       end

       % Pick the surface
       allSurfaces = candidateSurfaces{surfaceToUse};

    end
end

% Make sure that there are tracks associated to the surface object
if isempty(allSurfaces.GetTrackIds())
    errordlg( ...
        ['The surface object does not have track data associated ', ...
        'to it. Either the tracker was not run, or the license is ', ...
        'not active.'], ...
        'Error', 'modal');
    return;
end

% Get dataset sizes
sz = conn.getSizes();

% Some aliases
nChannels = sz(4);
nTimepoints = sz(5);

if nChannels < 2
    errordlg('At least two channels must exist in the dataset.', ...
        'Error', 'modal');
    return;
    
elseif nChannels == 2
    channelsToUseAsLabels = [1 2];
    
else
    % If there is more than two channels, we ask the user to pick the ones
    % to be used as labels.
    
    % Collect channel names
    channelNames = cell(1, nChannels);
    for i = 1 : nChannels
        channelNames{i} = [num2str(i), ': ', char( ...
            conn.mImarisApplication.GetDataSet().GetChannelName(i - 1))];
    end
    
    % Ask the user to pick the channels to use as labels
    [channelsToUseAsLabels, v] = listdlg( ...
        'PromptString', 'Select channels to use as labels:', ...
        'SelectionMode', 'multiple', 'ListSize', [400 300], ...
        'ListString', channelNames);
    
    if v == 0
        return
    end
    
    if numel(channelsToUseAsLabels) ~= 2
        errordlg('You must select two channels to process.', ...
            'Error', 'modal');
        return;
    end
end

% Ask the user to pick a file name where to save the results
[outFile, outPath] = uiputfile( ...
    {'*.csv','Comma-separated value files (*.csv)'; ...
        '*.xlsx','Excel spreadsheets (*.xlsx)'; ...
        '*.*',  'All Files (*.*)'}, ...
        'Pick a file name to save the results', 'results.csv');
if isequal(outFile, 0)
    return;
end
[~, f, e] = fileparts(outFile);
if isempty(f)
    outFile = ['results', e];
end
outFileName = fullfile(outPath, outFile);

% Allocate caches for the data.
allStackTimepointsCh1 = cell(1, nTimepoints);
allStackTimepointsCh2 = cell(1, nTimepoints);

% Process the tracks
% =========================================================================

% Retrieve track information
trackIDs = allSurfaces.GetTrackIds();
trackEdges = allSurfaces.GetTrackEdges();

% Get unique track IDs
uTrackIDs = unique(allSurfaces.GetTrackIds());

% Allocate a map to store the results. We add a fake key-value
% pair to set the type of the keys and values.
results = containers.Map(int32(-1), zeros(1, nTimepoints));

% Initialize waitbar
hWaitbar = waitbar(0, 'Processing tracks...');

for i = 1 : numel(uTrackIDs)
    
    % Get current track
    trackId = uTrackIDs(i);
    
    % Get the indices of track edge pairs belonging to this track
    edgePairs = trackEdges(trackIDs == trackId, :);
    
    % Process the edge pairs to get the list of surface objects
    surfaceIdsPerTrack = edgePairs';
    surfaceIdsPerTrack = unique(surfaceIdsPerTrack(:));
    
    % Allocate space to store the labels for surface
    labels = zeros(1, numel(surfaceIdsPerTrack));
    
    % Process the surfaces
    for j = 1 : numel(surfaceIdsPerTrack)
        
        % Get the timepoint (0-based) of the surface
        timepoint = allSurfaces.GetTimeIndex(surfaceIdsPerTrack(j));
        
        % Do we have the channel stack already? Otherwise, retrieve
        % and cache.
        if ~isempty(allStackTimepointsCh1{timepoint + 1})
            stackCh1 = allStackTimepointsCh1{timepoint + 1};
        else
            % Get the stack from Imaris
            stackCh1 = ...
                conn.getDataVolume(channelsToUseAsLabels(1), timepoint);
            
            % Cache it
            allStackTimepointsCh1{timepoint + 1} = stackCh1;
        end
        
        if ~isempty(allStackTimepointsCh2{timepoint + 1})
            stackCh2 = allStackTimepointsCh2{timepoint + 1};
        else
            % Get the stack from Imaris
            stackCh2 = ...
                conn.getDataVolume(channelsToUseAsLabels(2), timepoint);
            
            % Cache it
            allStackTimepointsCh2{timepoint + 1} = stackCh2;
        end
        
        % Get surface center of mass position in units
        surfacePosUnits = ...
            allSurfaces.GetCenterOfMass(surfaceIdsPerTrack(j));
        
        % Convert to pixels (round to grid)
        surfacePos = round(...
            conn.mapPositionsUnitsToVoxels(surfacePosUnits));
        
        % Consider a 3-by-3 neighborhood around the position
        y0 = surfacePos(1) - 1; if y0 < 1, y0 = 1; end
        y = surfacePos(1) + 1; if y > sz(1), y = sz(1); end
        x0 = surfacePos(2) - 1; if x0 < 1, x0 = 1; end
        x = surfacePos(2) + 1; if x > sz(2), x = sz(2); end
        z0 = surfacePos(3) - 1; if z0 < 1, z0 = 1; end
        z = surfacePos(3) + 1; if z > sz(3), z = sz(3); end
        
        % Get the intensities from both channels
        ch1 = stackCh1(y0:y, x0:x, z0:z);
        ch2 = stackCh2(y0:y, x0:x, z0:z);
        
        % Now pick the channel with the highest intensity as the label for
        % the surface
        if mean(ch1(:)) >= mean(ch2(:))
            labels(j) = channelsToUseAsLabels(1);
        else
            labels(j) = channelsToUseAsLabels(2);
        end
        
    end
    
    % Add the surfaces to the map using the trackID as key
    results(trackId) = labels ;
    
    % Update waitbar
    waitbar(i / numel(uTrackIDs), hWaitbar);
    
end

% Close waitbar
close(hWaitbar);

% Remove the (-1, -1) key-value pair
remove(results, int32(-1));

% Write the results to disk
writeResultsToDisk(results, outFileName);

% All done!

% -------------------------------------------------------------------------
function writeResultsToDisk(results, outFileName)

% Prepare the columns
id = zeros(results.Count, 1, 'int32');
firstLabel = zeros(results.Count, 1);
timeOfFirstSwitch = zeros(results.Count, 1);
numOfSwitches = zeros(results.Count, 1);
trackLength = zeros(results.Count, 1);

keys = results.keys();
for i = 1 : numel(keys)
    
    % Get the labels
    labels = results(keys{i});

    % Find the switches
    nSwitches = 0;
    timeOfSwitch = -1;
    previousLabel = labels(1);
    for j = 2 : numel(labels)
        
        % We have a switch
        if labels(j) ~= previousLabel
            nSwitches = nSwitches + 1;
            if timeOfSwitch == -1
                % This is the first switch
                timeOfSwitch = j;
            end
        end
        previousLabel = labels(j);
    end
    
    % Store the data
    id(i) = keys{i};
    firstLabel(i) = labels(1);
    timeOfFirstSwitch(i) = timeOfSwitch;
    numOfSwitches(i) = nSwitches;
    trackLength(i) = numel(labels);

end

% Build a table and write it to disk
t = table(id, firstLabel, timeOfFirstSwitch, numOfSwitches, trackLength, ...
    'VariableNames', { ...
    'Track_ID', 'First_Label', 'Time_of_First_Switch', ...
    'Num_of_Switches', 'Track_Length'});
writetable(t, outFileName, 'Delimiter', ';');

