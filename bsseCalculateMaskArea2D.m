function bsseCalculateMaskArea2D(vImarisApplication, readme)
% Exports all areas (in pixels^2) of a 2D channel masked with a surface.
%
% Copyright Aaron Ponti, BSSE, ETH Zurich, 2012
%
% <CustomTools>
%  <Menu>
%   <Submenu name="BSSE">
%    <Submenu name="Surfaces Functions">
%     <Submenu name="Export all surface areas of a 2D channel">
%      <Item name="Run" icon="Matlab" tooltip="Calculate 2D mask areas.">
%       <Command>MatlabXT::bsseCalculateMaskArea2D(%i, 0)</Command>
%      </Item>
%      <Item name="Help" icon="Matlab" tooltip="Show XTension help.">
%       <Command>MatlabXT::bsseCalculateMaskArea2D(%i, 1)</Command>
%      </Item>
%     </Submenu>
%    </Submenu>
%   </Submenu>
%  </Menu>
% </CustomTools>

if nargin == 1
    readme = 0;
end

if readme == 1
    showReadMe();
    return;
end

% Instantiate an IceImarisConnector object
conn = IceImarisConnector(vImarisApplication);

% Is there something loaded?
if isempty(conn.mImarisApplication.GetDataSet())
    return
end

% We only work on 2D datasets
[width, height, nPlanes] = conn.getSizes();
if nPlanes > 1
    uiwait(errordlg('Sorry, this XTension can only be run on 2D datasets.', ...
        'Error', 'modal'));
    return;
end

% =========================================================================
%
% The Surfaces object must be selected in the surpass scene
%
% =========================================================================

% Get the selection
surface = conn.getSurpassSelection('Surfaces');
if isempty(surface)
    uiwait(errordlg('Please choose a Surfaces object!', ...
        'Error', 'modal'));
    return;
end

% =========================================================================
%
% Ask the user to specify where to save the results
%
% =========================================================================

imarisFileName = char(conn.mImarisApplication.GetCurrentFileName());
[filePath, fileName, ~] = fileparts(imarisFileName);
if isempty(fileName)
    defaultName = 'output.csv';
else
    defaultName = fullfile(filePath, [fileName, '.csv']);
end
[fName, dirName] = uiputfile('.csv', 'Save results as...', defaultName);

% =========================================================================
%
% Now mask and extract area information
%
% =========================================================================

% Area of one pixel
voxelSizes = conn.getVoxelSizes();
pixArea = prod(voxelSizes(1 : 2));

% Allocate space for storing the results
nSurfaces = surface.GetNumberOfSurfaces();
results = zeros(nSurfaces, 4);

% Get the extends for the mask
extends = conn.getExtends();

% Initializing waitbar
hWaitbar = waitbar(0,'Please wait while processing...');

for i = 1 : nSurfaces
   
    % Get the dataset mask
    maskDataset = surface.GetSingleMask(i - 1, ...
        extends(1), extends(3), extends(5), ...
        extends(2), extends(4), extends(6), ...
        width, height, nPlanes);
    
    % Get the bw mask
    bwMask = conn.getDataVolume(0, 0, maskDataset);
    
    % Does it touch the image borders?
    [y, x] = find(bwMask > 0);
    
    atBorder = 0;
    if any(y == 1) || any(y == width) || any(x == 1) || any(x == height)
        atBorder = 1;
    end
    
    % Store
    nPix = numel(y);
    results(i, 1 : 4) = [i, nPix, nPix * pixArea atBorder];
    
    % Update the waitbar
    waitbar(i / nSurfaces, hWaitbar);
    
end

% Close the waitbar
close(hWaitbar);

% =========================================================================
%
% Store the results to the user-defined output file
%
% =========================================================================

resultFile = fullfile(dirName, fName);
fid = fopen(resultFile, 'w');
if fid == -1
    uiwait(errordlg(...
        'Could not create the results file. Writing to console...', ...
        'Error', 'modal'));
    fid = 1;
end

% Write header
fprintf(fid, '%8s\t%16s\t%16s\t%8s\n', 'ID', ...
    'Area (pixels^2)', 'Area (units^2)', 'Border');

% Write results
for i = 1 : size(results, 1)
    fprintf(fid, '%8d\t%16d\t%16.4f\t%8d\n', ...
        results(i, 1), results(i, 2), results(i, 3), results(i, 4));
end

% Close file
if fid ~= 1
    fclose(fid);
end

% Display file
edit(resultFile);

% =========================================================================

function showReadMe()

tmpfile = [ getTmpOutputDir(), filesep, 'help.html' ];
fid = fopen(tmpfile, 'w');
if fid == -1
    uiwait(errordlg('Could not create help file.', 'Error', 'modal'));
    return;
end

txt = [ '<html><head></head><body>', ...
    '<h1>Spots Drift Correction</h1>', ...
    '<p>This XTension exports all areas (in pixels^2) of a 2D ', ...
    'channel masked with a surface selected by the user.</p>', ...
    '<p>The results are exported to a csv file.</p>', ...
    '<p>Masked regions that touch the border of the image are ', ...
    'labelled in the exported file.</p>', ...
    '<p>Aaron Ponti, 2012/11/13</p>', ...
    '</body></html>'];

fprintf(fid, '%s', txt);

fclose(fid);

web(['file://',tmpfile], '-browser', '-noaddressbox');

% =========================================================================

function OUTPUTDIR = getTmpOutputDir

% Directory where to store the file
if isunix()
    OUTPUTDIR = '/tmp';
    return;
end

% In Windows we try the TMP folders
OUTPUTDIR=getenv('TMP');
if isempty(OUTPUTDIR)
    OUTPUTDIR=getenv('TEMP');
    if isempty(OUTPUTDIR)
        OUTPUTDIR=getenv('HOME');
        if isempty(OUTPUTDIR)
            OUTPUTDIR=pwd;
        end
    end
end
