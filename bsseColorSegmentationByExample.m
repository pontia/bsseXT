function bsseColorSegmentationByExample(vImarisApplication)
% Performs color segmentation by example. The user places spots of varying
% diameters in positions where objects have a possibly univocal color.
%
% Copyright Aaron Ponti, BSSE, ETH Zurich, 2014
%
% <CustomTools>
%  <Menu>
%   <Submenu name="BSSE">
%    <Submenu name="Dataset Functions">
%     <Submenu name="Color segmentation by example">
%      <Item name="Run" icon="Matlab" tooltip="Perform supervised color segmentation.">
%       <Command>MatlabXT::bsseColorSegmentationByExample(%i)</Command>
%      </Item>
%     </Submenu>
%    </Submenu>
%   </Submenu>
%  </Menu>
% </CustomTools>

% SET UP WAITBAR
% =========================================================================

global SHALL_EXIT

SHALL_EXIT = 0;

currentStep = 0; totNumSteps = 5;

% INITIALIZATION
% =========================================================================

% Instantiate an IceImarisConnector object
conn = IceImarisConnector(vImarisApplication);

% Is there something loaded?
if isempty(conn.mImarisApplication.GetDataSet())
    return
end

% Get dataset sizes
sz = conn.getSizes();

nChannels = sz(4);
if nChannels ~= 3
    uiwait(errordlg('The dataset must have three channels.', ...
        'Error', 'modal'));
    return;
end
nTimepoints = sz(5);
if nTimepoints ~= 1
    uiwait(errordlg('The dataset must have one timepoint.', ...
        'Error', 'modal'));
    return;
end

% GET NEEDED SPOTS OBJECT
% =========================================================================

% Get the Spots objects at the root level
spots = conn.getSurpassSelection('Spots');
if isempty(spots)
    uiwait(errordlg('Please choose a Spots object.', ...
        'Error', 'modal'));
    return;
end

% ASK USER FOR PARAMETERS
% =========================================================================

% Ask the user for the strength of the threshold
answer = inputdlg( ...
    {'Outlier threshold strength (0 to disable):'}, ...
    'Input requested', 1, {'0'});
if isempty(answer)
    return
end
tf = str2double(answer);
if isnan(tf) || tf < 0
    quError({'Invalid value for outlier threshold!', ...
        ['It must be either 0 to disable outlier detection, ', ...
        'or a positive real number.']});
    return
end

% Ask the user for color space to use
choice = questdlg({'Segment in L*a*b or HSV color space?', '', ...
    'Please use L*a*b if there is any (black) background in the dataset.'}, ...
    'Question', 'L*a*b', 'HSV', 'L*a*b');
switch choice,
    case 'L*a*b',
        USE_LAB = 1;
    case 'HSV',
        USE_LAB = 0;
    otherwise,
        return
end % switch
   
% INITIALIZE WAITBAR
% =========================================================================

hWaitbar = waitbar(0, 'Processing...', 'CreateCancelBtn', @stopProcess);

% GET ROIS FROM SPOTS
% =========================================================================

% Get voxel sizes
voxelSizes = conn.getVoxelSizes();

% Get all values
spotsValues = spots.Get();
pos = spotsValues.mPositionsXYZ;
radii = spotsValues.mRadii;

% Number of spots
nSpots = size(pos, 1);

% Update waitbar
if SHALL_EXIT == 1
    return;
end
currentStep = currentStep + 1;
waitbar(currentStep/totNumSteps, hWaitbar);

% GET DATA
% =========================================================================

% Get the first channel
stacks = conn.getDataVolume(0, 0);

% Add the others
c = 1;
while c < nChannels
   stacks = cat(4, stacks, conn.getDataVolume(c, 0));
   c = c + 1;
end

% Update waitbar
if SHALL_EXIT == 1
    return;
end
currentStep = currentStep + 1;
waitbar(currentStep/totNumSteps, hWaitbar);

% CONVERT TO L*a*b* COLORSPACE
% =========================================================================

if USE_LAB == 1
    [A, B, L] = toLab(stacks);
else
    [H, L] = toHue(stacks);
end

% EXTRACT COLOR STATISTICS
% =========================================================================

if USE_LAB == 1
    [mnColors, sdColors] = labExtractColorStatistics(conn, ...
        sz, nSpots, pos, radii, voxelSizes, A, B);
else
    [mnColors, sdColors] = hueExtractColorStatistics(conn, ...
        sz, nSpots, pos, radii, voxelSizes, H);
end

% Update waitbar
if SHALL_EXIT == 1
    return;
end
currentStep = currentStep + 1;
waitbar(currentStep/totNumSteps, hWaitbar);

% CLASSIFY BY NEAREST NEIGHBORS
% =========================================================================

if USE_LAB == 1

    D = zeros([size(A), nSpots]);
    
    for i = 1 : nSpots
        D(:, :, :, i) = ( ...
            (A - mnColors(i, 1)) .^ 2 + ...
            (B - mnColors(i, 2)) .^ 2 ) .^ 0.5;
    end

else
    
    D = zeros([size(H), nSpots]);
    
    for i = 1 : nSpots
        D(:, :, :, i) = ...
            min(abs(H - mnColors(i, 1)), abs(H - 1 - mnColors(i, 1)));
    end
    
end

% Label colors
[value, label] = min(D, [], 4);

clear D;

% Update waitbar
if SHALL_EXIT == 1
    return;
end
currentStep = currentStep + 1;
waitbar(currentStep/totNumSteps, hWaitbar);

% THRESHOLD DISTANCE
% =========================================================================

% Threshold distance
if tf ~= 0
    th = tf .* sqrt(sdColors(:, 1) .^ 2 + sdColors(:, 2) .^ 2);
    nValue = numel(value);
    for i = 1 : nValue
        if value(i) > th(label(i))
            label(i) = Inf;
        end
    end
    clear value;
end

% Update waitbar
if SHALL_EXIT == 1
    return;
end
currentStep = currentStep + 1;
waitbar(currentStep/totNumSteps, hWaitbar);

% ADD CHANNELS
% =========================================================================

for i = 1 : nSpots
    nChannels = nChannels + 1;
    conn.mImarisApplication.GetDataSet().SetSizeC(nChannels);
    C = zeros(size(label), conn.getMatlabDatatype());
    C(label == i) = L(label == i);
    conn.setDataVolume(C, nChannels - 1, 0);
    conn.mImarisApplication.GetDataSet().SetChannelName(nChannels - 1, ...
        ['Segmented color ', num2str(i)]);
    conn.mImarisApplication.GetDataSet().SetChannelColorRGBA( ...
        nChannels - 1, ...
        conn.mapRgbaVectorToScalar([rand(1, 3), 0]));
end

% Add the outliers as well
if tf ~= 0
    nChannels = nChannels + 1;
    conn.mImarisApplication.GetDataSet().SetSizeC(nChannels);
    C = zeros(size(label), conn.getMatlabDatatype());
    C(label == Inf) = L(label == Inf);
    conn.setDataVolume(C, nChannels - 1, 0);
    conn.mImarisApplication.GetDataSet().SetChannelName(nChannels - 1, ...
        'Outliers');
    conn.mImarisApplication.GetDataSet().SetChannelColorRGBA( ...
        nChannels - 1, ...
        conn.mapRgbaVectorToScalar([0 1 1 0]));    
end

% Close waitbar
close(hWaitbar);

% =========================================================================
% =========================================================================
% =========================================================================


% EXTRACT COLOR STATISTICS
% =========================================================================

% Map to Hue
function [A, B, L] = toLab(stacks)

% Color transform
cform = makecform('srgb2lab');

% Make sure that stacks in 8-bit
switch class(stacks)
    case 'uint8',
        imax = 2^8 - 1;
        clStack = 'uint8';
    case 'uint16',
        stacks = nrm(stacks, 8);
        imax = 2^16 - 1;
        clStack = 'uint16';
    otherwise,
        error('Unsupported datatype.');
end

A = zeros([size(stacks, 1), size(stacks, 2), size(stacks, 3)], 'double');
B = zeros([size(stacks, 1), size(stacks, 2), size(stacks, 3)], 'double');
L = zeros([size(stacks, 1), size(stacks, 2), size(stacks, 3)], clStack);

for i = 1 : size(stacks, 3)
    
    % Create a 2D RGB image
    img = zeros([size(stacks, 1), size(stacks, 2), 3], class(stacks));
    img(:, :, 1) = stacks(:, :, i, 1);
    img(:, :, 2) = stacks(:, :, i, 2);
    img(:, :, 3) = stacks(:, :, i, 3);
    
    % Get the components
    img_lab = applycform(img, cform);

    % Store them
    if strcmp(clStack, 'uint8') == 1
        L(:, :, i) = img_lab(:, :, 1);
    else
        L(:, :, i) = cast(double(img_lab(:, :, 1)) ./ 255 .* imax, clStack);
    end    
    A(:, :, i) = double(img_lab(:, :, 2));
    B(:, :, i) = double(img_lab(:, :, 3));
    
end

% EXTRACT COLOR STATISTICS
% =========================================================================

% Map to Hue
function [H, L] = toHue(stacks)

% Make sure that stacks in 8-bit
switch class(stacks)
    case 'uint8',
        imax = 2^8 - 1;
        clStack = 'uint8';
    case 'uint16',
        stacks = nrm(stacks, 8);
        imax = 2^16 - 1;
        clStack = 'uint16';
    otherwise,
        error('Unsupported datatype.');
end

% Hue
H = zeros([size(stacks, 1), size(stacks, 2), size(stacks, 3)], 'double');

% Intensity
L =  zeros([size(stacks, 1), size(stacks, 2), size(stacks, 3)], clStack);

for i = 1 : size(stacks, 3)
    
    % Create a 2D RGB image
    img = zeros([size(stacks, 1), size(stacks, 2), 3], class(stacks));
    img(:, :, 1) = stacks(:, :, i, 1);
    img(:, :, 2) = stacks(:, :, i, 2);
    img(:, :, 3) = stacks(:, :, i, 3);
    
    % Get the hue and the intensity
    img_hue = rgb2hsv(img);

    % Store them
    H(:, :, i) = double(img_hue(:, :, 1));
    L(:, :, i) = cast(imax .* img_hue(:, :, 3), clStack);
    
end

% =========================================================================

function [mnColors, sdColors] = labExtractColorStatistics(conn, ...
    sz, nSpots, pos, radii, voxelSizes, A, B)

mnColors = zeros([nSpots, 2]);
sdColors = zeros([nSpots, 2]);
for i = 1 : nSpots
    
    % Get spot mask
    [mask, x0, y0, z0, x, y, z] = ...
        getSpotMask(conn, pos(i, :), radii(i), voxelSizes, sz);
    
    % Get the color channels in the ROI
    selA = A(x0 : x, y0 : y, z0 : z);
    selB = B(x0 : x, y0 : y, z0 : z);
    
    % Store mean colors and their standard deviations
    mnColors(i, :) = [mean(selA(mask ~= 0)) mean(selB(mask ~= 0))];
    sdColors(i, :) = [std(selA(mask ~= 0))  std(selB(mask ~= 0))];

end

% =========================================================================

function [mnColors, sdColors] = hueExtractColorStatistics(conn, ...
    sz, nSpots, pos, radii, voxelSizes, H)

mnColors = zeros([nSpots, 1]);
sdColors = zeros([nSpots, 1]);
for i = 1 : nSpots
    
    % Get spot mask
    [mask, x0, y0, z0, x, y, z] = ...
        getSpotMask(conn, pos(i, :), radii(i), voxelSizes, sz);

    % Get the color channels in the ROI
    selH = H(x0 : x, y0 : y, z0 : z);
    
    % Store mean colors and their standard deviations
    mnColors(i, 1) = mean(selH(mask));
    sdColors(i, 1) = std(selH(mask));

end

% =========================================================================

function [mask, x0, y0, z0, x, y, z] = getSpotMask(conn, spotPos, spotRadius, voxelSizes, sz)


% Map to pixels
spotPosPx = conn.mapPositionsUnitsToVoxels(spotPos);

% Spots: side of the containing cube in pixels.
spotRadiusPx(1, 1) = round(spotRadius ./ voxelSizes(1));
spotRadiusPx(1, 2) = round(spotRadius ./ voxelSizes(2));
spotRadiusPx(1, 3) = round(spotRadius ./ voxelSizes(3));

% Get the ROI bounding box
x0 = round(spotPosPx(1, 1) - spotRadiusPx(1, 1)); if x0 < 1, x0 = 1; end
y0 = round(spotPosPx(1, 2) - spotRadiusPx(1, 2)); if y0 < 1, y0 = 1; end
z0 = round(spotPosPx(1, 3) - spotRadiusPx(1, 3)); if z0 < 1, z0 = 1; end
x  = round(spotPosPx(1, 1) + spotRadiusPx(1, 1)); if x  > sz(1), x = sz(1); end
y  = round(spotPosPx(1, 2) + spotRadiusPx(1, 2)); if y  > sz(2), y = sz(2); end
z  = round(spotPosPx(1, 3) + spotRadiusPx(1, 3)); if z  > sz(3), z = sz(3); end
    
% Use only the values that fall in the spot radius
uX = voxelSizes(1) .* (x0 : x);
uY = voxelSizes(2) .* (y0 : y);
uZ = voxelSizes(3) .* (z0 : z);
cX = (uX(1) + uX(end)) / 2;
cY = (uY(1) + uY(end)) / 2;
cZ = (uZ(1) + uZ(end)) / 2;
dY = y - y0 + 1;
dX = x - x0 + 1;
dZ = z - z0 + 1;
mask = false([dY, dX, dZ]);
for i =  1 : numel(uY)
    for j =  1 : numel(uX)
        for k =  1 : numel(uZ)
            if sqrt( ...
                    (uY(i) - cY) ^ 2 + ...
                    (uX(j) - cX) ^ 2 + ...
                    (uZ(k) - cZ) ^ 2) <= spotRadius
                mask(i, j, k) = 1;
            end
        end
    end
end


% =========================================================================

function stopProcess(handle, ~)
global SHALL_EXIT
SHALL_EXIT = 1;
hParent = get(handle, 'Parent');
if hParent == 0
    % 'handle' is the window
    delete(handle);
else
    % 'handle' is the buttonm
    delete(hParent);
end

