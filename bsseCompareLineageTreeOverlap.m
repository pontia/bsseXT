function bsseCompareLineageTreeOverlap(imarisApplicationId)
% Calculate overlap of lineage trees
%
%    <CustomTools>
%      <Menu>
%       <Submenu name="BSSE">
%        <Submenu name="Spots Functions">
%         <Item name="Calculate lineage tree overlap" icon="Matlab">
%          <Command>MatlabXT::bsseCompareLineageTreeOverlap(%i)</Command>
%         </Item>
%        </Submenu>
%       </Submenu>
%      </Menu>
%      <SurpassTab>
%        <SurpassComponent name="bpSpots">
%          <Item name=Calculate lineage tree overlap" icon="Matlab">
%            <Command>MatlabXT::bsseCompareLineageTreeOverlap(%i)</Command>
%          </Item>
%        </SurpassComponent>
%      </SurpassTab>
%    </CustomTools>

% Instantiate IceImarisConnector object
conn = IceImarisConnector(imarisApplicationId);

% Is there something loaded?
mDataSet = conn.mImarisApplication.GetDataSet();
if isempty(mDataSet)
    return
end

% Get the spots
spots = conn.getAllSurpassChildren(1, 'Spots');
if isempty(spots)
    uiwait(errordlg('No Spots objects found in the scene!'));
    return;
end

if numel(spots) < 2
    uiwait(errordlg('You need two Spots objects!'));
    return;
end

% Get spot names
spotNames = cell(1, numel(spots));
for i = 1 : numel(spots)
    spotNames{i} = char(spots{i}.GetName());
end

% Ask the user to specify the first Spots object
[s, v] = listdlg('PromptString', ...
    'Please pick the first Spots object',...
    'SelectionMode', 'single', ...
    'ListSize', [300 100], ...
    'ListString', spotNames);
if v == 0
    return
end

% Get the "all cells" Spots object
spot1 = spots{s};
spots(s) = [];
spotNames(s) = [];

if numel(spots) == 1
    % Get the "activated cells" Spots object
    spot2 = spots{1};
else
    % Ask the user to specify the second Spots object
    [s, v] = listdlg('PromptString', ...
        'Please pick the second Spots object',...
        'SelectionMode', 'single', ...
        'ListSize', [300 100], ...
        'ListString', spotNames);
    if v == 0
        return
    end
    
    % Get the "activated cells" Spots object
    spot2 = spots{s};
    clear('spots');
    clear('spotNames');
end

% Waitbar
hWaitbar = waitbar(0, 'Building first lineage tree...');

% Build the lineage trees
lineage1 = buildLineage(spot1);
waitbar(1/3, hWaitbar, 'Building second lineage tree...');

lineage2 = buildLineage(spot2);
waitbar(2/3, hWaitbar, 'Calculating overlap...');

% Compare the trees
overlap = calculateLineageOverlap(lineage1, lineage2, spot1, spot2);
waitbar(3/3, hWaitbar, '');

% Close waitbar
close(hWaitbar);

uiwait(helpdlg(sprintf('Overlap is %.2f%%.\n', overlap * 100)));

% =========================================================================

function trees = buildLineage(spot)

% Get spot ids
spotIds = spot.GetIds();

% Get spot information
spotTimes = spot.GetIndicesT();

% Get track ids and egdes
trackIds = spot.GetTrackIds();
uTrackIds = unique(trackIds);
trackEdges = spot.GetTrackEdges();

% We create a tree per track id and store the timepoint of origin in
% the root of the tree
trees = cell(1, numel(uTrackIds));

for i = 1 : numel(uTrackIds)

    currTrackId = uTrackIds(i);
    currTrack = trackIds == currTrackId;
    currTrackEdges = trackEdges(currTrack, :);

    % Get the timepoint of origin
    spotIndx = find(spotIds == currTrackEdges(1, 1));
    originTimepoint = spotTimes(spotIndx);
    
    % We use the great tree class by Jean-Yves Tivenez
    lineage = tree(originTimepoint);

    for j = 1 : size(currTrackEdges, 1)
        
        currentEdge = currTrackEdges(j, :);
        
        if j == 1
            [lineage, parent] = lineage.addnode(1, currentEdge(1));
        else
            parent = find(lineage == currentEdge(1));
            if isempty(parent)
                parent = 1;
            end
        end
        
        % Add a node
        lineage = lineage.addnode(parent, currentEdge(2));

    end
    
    trees{i} = lineage;
end

% -------------------------------------------------------------------------

function overlap = calculateLineageOverlap(lineage1, lineage2, spot1, spot2)

% Some constants
numLineage1 = numel(lineage1);
numLineage2 = numel(lineage2);

% Get spot ids
spotId1 = spot1.GetIds();
spotId2 = spot2.GetIds();

% Get spot coordinates
spotPosXYZ1 = spot1.GetPositionsXYZ();
spotPosXYZ2 = spot2.GetPositionsXYZ();

% Get the time points of origin and the tree length
matrixLineage1 = zeros(numLineage1, 4);
matrixLineage2 = zeros(numLineage2, 4);

for i = 1 : numLineage1
    timepoint = lineage1{i}.get(1);
    id = lineage1{i}.get(lineage1{i}.getchildren(1));
    coords = spotPosXYZ1(spotId1 == id, :); 
    matrixLineage1(i, :) = [double(timepoint), coords];
end

for i = 1 : numLineage2
    timepoint = lineage2{i}.get(1);
    id = lineage2{i}.get(lineage2{i}.getchildren(1));
    coords = spotPosXYZ2(spotId2 == id, :); 
    matrixLineage2(i, :) = [double(timepoint), coords];
end

% Store the number of nodes per tree to weight the overlaps (we do not
% normalize them yet)
weights = zeros(1, max([numLineage1, numLineage2]));
for i = 1 : numLineage1
    weights(i) = lineage1{i}.nnodes();
end

% Find the matching
all_overlaps = 0 .* weights;
D = pdist2(matrixLineage1, matrixLineage2);
alreadyAssigned = zeros(1, size(D, 1));
for i = 1 : size(D, 1)
    target = find(D(i, :) == min(D(i, :)), 1);
    if isempty(find(alreadyAssigned == target, 1))
        alreadyAssigned(i) = target;
        all_overlaps(i) = compareSingleTrees(lineage1{i}, lineage2{target});
    end
end

% If we have too few trees in the first lineage, we use the number of nodes
% of the unassigned trees in the second lineage not to give too much weight
% to missing trees.
if numLineage1 < numLineage2
    indices = setdiff(1 : size(D, 2), alreadyAssigned);
    for i = 1 : numel(indices)
        weights(numLineage1 + i) = lineage2{indices(i)}.nnodes();
    end
end

% Normalize weights
weights = weights ./ sum(weights);

% Total overlap
overlap = sum(weights .* all_overlaps);

function overlap = compareSingleTrees(tree1, tree2)

% First, a simple test
it1 = tree1.depthfirstiterator();
it2 = tree2.depthfirstiterator();
if numel(it1) == numel(it2) && all(it1 == it2)
    overlap = 1;
    return;
end

% Calculate Levenshtein distance on the depth first iterator
V = LevenshteinDistance(it1, it2);
overlap_depth = 1 - V / max(numel(it1), numel(it2));

% Calculate Levenshtein distance on the breadth first iterator
it1 = tree1.breadthfirstiterator();
it2 = tree2.breadthfirstiterator();
V = LevenshteinDistance(it1, it2);
overlap_breadth = 1 - V / max(numel(it1), numel(it2));

overlap = mean([overlap_depth, overlap_breadth]);

% -------------------------------------------------------------------------

function V = LevenshteinDistance(seq1, seq2)

m = length(seq1);
n = length(seq2);
v = zeros(m + 1, n + 1);
for i = 1 : 1 : m
    v(i + 1, 1) = i;
end
for j = 1 : 1 : n
    v(1, j + 1) = j;
end
for i = 1 : m
    for j = 1 : n
        if (seq1(i) == seq2(j))
            v(i + 1, j + 1) = v(i, j);
        else
            v(i + 1, j + 1) = 1 + min(min(v(i + 1, j), v(i,j + 1)), v(i, j));
        end
    end
end
V = v(m + 1, n + 1);
