function bsseCopyChannels(vImarisApplication)
% Makes a copy of selected channels.
%
% Copyright Aaron Ponti, BSSE, ETH Zurich, 2015
%
% <CustomTools>
%  <Menu>
%   <Submenu name="BSSE">
%    <Submenu name="Dataset Functions">
%     <Submenu name="Copy channels">
%      <Item name="Run" icon="Matlab" tooltip="Copy one or more channels.">
%       <Command>MatlabXT::bsseCopyChannels(%i)</Command>
%      </Item>
%     </Submenu>
%    </Submenu>
%   </Submenu>
%  </Menu>
% </CustomTools>

% INITIALIZATION
% =========================================================================

% Instantiate an IceImarisConnector object
conn = IceImarisConnector(vImarisApplication);

% Is there something loaded?
if isempty(conn.mImarisApplication.GetDataSet())
    return
end

% Get dataset sizes
sz = conn.getSizes();

% Some aliases
nChannels = sz(4);
nTimepoints = sz(5);

% If there is more than one channel, we ask the user to pick the ones to be
% copied.
if nChannels > 1

    % Collect channel names
    channelNames = cell(1, nChannels);
    for i = 1 : nChannels
    
        channelNames{i} = char( ...
            conn.mImarisApplication.GetDataSet().GetChannelName(i - 1));
    
    end

    % Ask the user to pick the channels to copy
    [channelsToCopy, v] = listdlg( ...
        'PromptString', 'Select channels to copy:', ...
        'SelectionMode', 'multiple', 'ListSize', [400 300], ...
        'ListString', channelNames);

    if v == 0
        return
    end
    
else
    
    % The channel to copy is the first
    channelsToCopy = 1;

end


% COPY CHANNELS
% =========================================================================

% % Make the channelsToCopy indices zero-based
% channelsToCopy = channelsToCopy - 1;

for c = 1 : numel(channelsToCopy)

    % Add channel
    nChannels = nChannels + 1;
    conn.mImarisApplication.GetDataSet().SetSizeC(nChannels);

    % New channel index
    newChannelIndex = nChannels - 1;
        
    % Set the new channel name
    conn.mImarisApplication.GetDataSet().SetChannelName( ...
        newChannelIndex, ...
        ['Copy of ', channelNames{channelsToCopy(c)}]);
    
    % Set the new channel color
    conn.mImarisApplication.GetDataSet().SetChannelColorRGBA( ...
        newChannelIndex, ...
        conn.mImarisApplication.GetDataSet().GetChannelColorRGBA( ...
        channelsToCopy(c) - 1));


    for t = 1 : nTimepoints

        % Get stack
        stack = conn.getDataVolume(channelsToCopy(c) - 1, t - 1);

        % Store channel
        conn.setDataVolume(stack, newChannelIndex,  t - 1);

    
    end
    
end
