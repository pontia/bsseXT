function bsseCreateLateralProjections(id)
%
%  Creates lateral projections of current dataset and saves them to disk
%
%  Copyright Aaron Ponti (BSSE) 2014/02/18
%
%  <CustomTools>
%   <Menu>
%    <Submenu name="BSSE">
%     <Submenu name="Dataset Functions">
%      <Item name="Create and save lateral projections" icon="Matlab">
%       <Command>MatlabXT::bsseCreateLateralProjections(%i)</Command>
%      </Item>
%     </Submenu>
%    </Submenu>
%   </Menu>
%  </CustomTools>
%

% Instantiate IceImarisConnector instance
conn = IceImarisConnector(id);

% Make sure that a 3D dataset is loaded
if isempty(conn.mImarisApplication.GetDataSet())
    uiwait(errordlg('Please load a dataset first!', 'Error'), 'modal');
    return;
end
    
% Get the voxel sizes (needed for interpolation)
[voxelX, voxelY, voxelZ] = conn.getVoxelSizes();

% Get the dataset sizes
[sizeX, sizeY, sizeZ, sizeC, sizeT] = conn.getSizes();

% Get the datatype
datatype = conn.getMatlabDatatype();

% Aspect ratios
aspect_XZ = voxelZ / voxelX;
aspect_YZ = voxelZ / voxelY;

% Calculate size of projections for interpolation
sizeP_XZ = [round(sizeZ * aspect_XZ), sizeX];
sizeP_YZ = [sizeY, round(sizeZ * aspect_YZ)];

% Ask the user to pick a folder where to save the projections
dirName = uigetdir(pwd(), ...
    'Please pick a directory where to save the projections');
if isequal(dirName, 0)
    return;
end

% Process
for timepoint = 1 : sizeT
    
    for channel = 1 : sizeC

        % Get current stack
        stack = conn.getDataVolumeRM(channel - 1, timepoint - 1);
    
        % Project along Z
        stack_p_XY = max(stack, [], 3);
        
        % Project along Y
        stack_py = max(stack, [], 1);
        stack_p_XZ = squeeze(stack_py)';
        
        % Project along X
        stack_px = max(stack, [], 2);
        stack_p_YZ = squeeze(stack_px);

        % Interpolate to preserve the aspect ratio
        stack_p_XZ = imresize(stack_p_XZ, sizeP_XZ, 'lanczos3');
        stack_p_YZ = imresize(stack_p_YZ, sizeP_YZ, 'lanczos3');

        % Create merger
        totalSizeX = sizeX + sizeP_YZ(2);
        totalSizeY = sizeY + sizeP_XZ(1);
        merger = zeros([totalSizeY totalSizeX], datatype);
        merger(1 : sizeY, 1 : sizeX) = stack_p_XY;
        merger(sizeY + 1 : end, 1 : sizeX) = stack_p_XZ;
        merger(1 : sizeY, sizeX + 1 : end) = stack_p_YZ;
        
        % Build file names
        partFName = ['T_', num2str(timepoint), '_C_', num2str(channel)];
        fNameXZ = fullfile(dirName, ['proj_XZ_', partFName, '.tif']);
        fNameYZ = fullfile(dirName, ['proj_YZ_', partFName, '.tif']);
        fNameXY = fullfile(dirName, ['proj_XY_', partFName, '.tif']);
        fNameMerge = fullfile(dirName, ['proj_merge_', partFName, '.tif']);
            
        % Save the projections
        imwrite(stack_p_XZ, fNameXZ, 'Compression', 'none');
        imwrite(stack_p_YZ, fNameYZ, 'Compression', 'none');
        imwrite(stack_p_XY, fNameXY, 'Compression', 'none');
        imwrite(merger, fNameMerge, 'Compression', 'none');
        
    end
    
end





