function bsseDataSetPadToNewSize(vImarisApplication)
% Pads the dataset to a user-defined size.
%
% Copyright Aaron Ponti, BSSE, ETH Zurich, 2015
%
% <CustomTools>
%  <Menu>
%   <Submenu name="BSSE">
%    <Submenu name="Dataset Functions">
%      <Item name="Pad dataset to new size (in voxels)" icon="Matlab" tooltip="Pads the dataset to a new user-defined size.">
%       <Command>MatlabXT::bsseDataSetPadToNewSize(%i)</Command>
%      </Item>
%    </Submenu>
%   </Submenu>
%  </Menu>
% </CustomTools>

% Instantiate an IceImarisConnector object
conn = IceImarisConnector(vImarisApplication);

% =========================================================================
%
% Make sure there is a dataset loaded
%
% =========================================================================

% Is there something loaded?
aDataSet = conn.mImarisApplication.GetDataSet();
if isempty(aDataSet)
    return
end

% =========================================================================
%
% Ask the user to set the new size
%
% =========================================================================

% Get the size
answer = inputdlg({'X:', 'Y:', 'Z:'}, ...
    'Enter the new dataset size (in voxels)', 1, ...
    {num2str(aDataSet.GetSizeX()), ...
    num2str(aDataSet.GetSizeY()), ...
    num2str(aDataSet.GetSizeZ())});

if isempty(answer)
    return
end

% Get and check the new X size
newSizeX = str2double(answer{1});
if isnan(newSizeX) || newSizeX < 1
    uiwait(errordlg('Invalid size X.', 'Error', 'modal'));
    return
end

% Get and check the new Y size
newSizeY = str2double(answer{2});
if isnan(newSizeY) || newSizeY < 1
    uiwait(errordlg('Invalid size Y.', 'Error', 'modal'));
    return
end

% Get and check the new Z size
newSizeZ = str2double(answer{3});
if isnan(newSizeZ) || newSizeZ < 1
    uiwait(errordlg('Invalid size Z.', 'Error', 'modal'));
    return
end

% Set the new size
if newSizeX ~= aDataSet.GetSizeX()
    aDataSet.SetSizeX(newSizeX);
end
if newSizeY ~= aDataSet.GetSizeY()
    aDataSet.SetSizeY(newSizeY);
end
if newSizeZ ~= aDataSet.GetSizeZ()
    aDataSet.SetSizeZ(newSizeZ);
end
