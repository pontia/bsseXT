function bsseDataSetSumChannels(vImarisApplication)
% Pads the dataset to a user-defined size.
%
% Copyright Aaron Ponti, BSSE, ETH Zurich, 2015
%
% <CustomTools>
%  <Menu>
%   <Submenu name="BSSE">
%    <Submenu name="Dataset Functions">
%      <Item name="Sum selected channels" icon="Matlab" tooltip="Create a new channel as the sume of selected channels.">
%       <Command>MatlabXT::bsseDataSetSumChannels(%i)</Command>
%      </Item>
%    </Submenu>
%   </Submenu>
%  </Menu>
% </CustomTools>

% Instantiate an IceImarisConnector object
conn = IceImarisConnector(vImarisApplication);

% =========================================================================
%
% Make sure there is a dataset loaded
%
% =========================================================================

% Is there something loaded?
aDataSet = conn.mImarisApplication.GetDataSet();
if isempty(aDataSet)
    return
end

% =========================================================================
%
% Ask the user to pick the channels
%
% =========================================================================

[sizeX, sizeY, sizeZ, sizeC, sizeT] = conn.getSizes();

if sizeC < 2
    uiwait(errordlg('At least two channels needed.', 'Error', 'modal'));
    return;
end

if sizeC > 2
    
    % Build the list
    channelNames = cell(1, sizeC);
    for i = 1 : sizeC
        channelNames{i} = char(aDataSet.GetChannelName(i - 1));
    end
    
    % Ask the user
    [channels, v] = listdlg('PromptString', 'Pick at least two channels to sum:',...
        'SelectionMode', 'multi', ...
        'ListSize', [400 400], ...
        'ListString', channelNames);
    if v == 0
        return;
    end
    if numel(channels) < 2
        uiwait(errordlg('At least two channels needed.', 'Error', 'modal'));
        return;
    end

else
    
    channels = [1 2];

end

% Clone the dataset
newDataset = conn.cloneDataset();

% Add a new channel
newDataset.SetSizeC(sizeC + 1);

if sizeT > 1
    hWaitbar = waitbar(0, 'Processing timepoints...');
end

% Now go over all timepoints and add the channels
targetChannel = sizeC;  % 0-based!
for timepoint = 0 : sizeT - 1
    
    stack = zeros([sizeX, sizeY, sizeZ], 'single');
    
    for j = 1 : numel(channels)
        stack = stack + ...
            single(conn.getDataVolume(channels(j) - 1, timepoint));
    end

    mx = max(stack(:));
    switch conn.getMatlabDatatype()
        case 'uint8'           
            if mx > 255
                mn = min(stack(:));
                stack = uint8(255.* (stack - mn) ./ (mx - mn));
            else
                stack = uint8(stack);
            end
        case 'uint16'
            if mx > 65535
                mn = min(stack(:));
                stack = uint16(65535.* (stack - mn) ./ (mx - mn));
            else
                stack = uint16(stack);
            end
        case 'single'
            % Nothing to do
        otherwise
            uiwait(errordlg('Unsupported datatype.', 'Error', 'modal'));
            return;
    end
    
    % Push back
    switch conn.getMatlabDatatype()
        case 'uint8'
            newDataset.SetDataVolumeAs1DArrayBytes(stack(:), targetChannel, timepoint);
        case 'uint16'
            newDataset.SetDataVolumeAs1DArrayShorts(stack(:), targetChannel, timepoint);
        case 'single'
            newDataset.SetDataVolumeAs1DArrayFloats(stack(:), targetChannel, timepoint);
        otherwise
            uiwait(errordlg('Unsupported datatype.', 'Error', 'modal'));
            return;
    end
    
    if sizeT > 1
        waitbar((timepoint + 1) / sizeT, hWaitbar);
    end

end

if sizeT > 1
    close(hWaitbar);
end

% Set the dataset back
conn.mImarisApplication.SetDataSet(newDataset);
