function bsseDoGFilter(imarisApplicationId)
% Runs a Difference of Gaussian Filter on the dataset
%
%  <CustomTools>
%   <Menu>
%    <Submenu name="BSSE">
%     <Submenu name="Dataset Functions">
%      <Item name="Run Difference of Gaussian filter (spot enhancer)" icon="Matlab">
%       <Command>MatlabXT::bsseDoGFilter(%i)</Command>
%      </Item>
%     </Submenu>
%    </Submenu>
%   </Menu>
%  </CustomTools>
%
% Aaron Ponti, 20170809

% Instantiate IceImarisConnector object
conn = IceImarisConnector(imarisApplicationId);

% Is there something loaded?
mDataSet = conn.mImarisApplication.GetDataSet();
if isempty(mDataSet)
    return
end

% Number of channels
nChannels = conn.mImarisApplication.GetDataSet().GetSizeC();

% Ask the user to specify spot diameter in xy and z
answer = inputdlg(...
    { ...
    'Enter lateral (XY) spot size (datset units)', ...
    'Enter axial (Z) spot size (datset units)', ...
    'Enter sigma ratio of the DoG filter [ 0.5 <= gamma < 1.0]', ...
    }, ...
    'Input', 1, {'5', '5', '0.8'});
if isempty(answer)
    return
end
dxy = str2double(answer{1});
if isnan(dxy) || dxy <= 0
    uiwait(errordlg('The lateral spot size must be a positive number!'));
    return;
end
dz = str2double(answer{2});
if isnan(dz) || dz <= 0
    uiwait(errordlg('The axial spot size must be a positive number!'));
    return;
end
gamma = str2double(answer{3});
if isnan(gamma) || gamma < 0.5 || gamma >= 1.0
    uiwait(errordlg('The sigma ratio gamma size must be [ 0.5 <= gamma < 1.0]!'));
    return;
end

if nChannels > 1
    answer = inputdlg(...
        {'Enter channel(s) to filter (0-based)'}, ...
        'Input', 1, {num2str(0 : (nChannels - 1))});
    if isempty(answer)
        return
    end
    channels = str2num(answer{1}); %#ok<ST2NM>
    if isempty(channels) || any(channels) < 0 || any(channels > nChannels)
        uiwait(errordlg(...
            sprintf('The channels must be a subset of {0 .. %d}', ...
            (nChannels - 1))));
        return;
    end
else
    channels = 0;
end

% Process all time points or just current?
answer = questdlg(...
    'Process all time points or just current?', ...
    'Question', ...
    'Current', 'All', 'Current');
switch answer
    case 'Current'
        processAllTimePoints = 0;
    case 'All'
        processAllTimePoints = 1;
    otherwise
        return
end
   
% Gt the voxel sizes
[voxelSizeX, voxelSizeY, voxelSizeZ] = conn.getVoxelSizes();
vxy = mean([voxelSizeX, voxelSizeY]);
vz = voxelSizeZ;

% Calculate the DoG
sigmaxy = floor(dxy / vxy / 6);
if sigmaxy < 1
    sigmaxy = 1;
end
if mod(sigmaxy, 2) == 0
    sigmaxy = sigmaxy + 1;
end
sigmaz = floor(dz / vz / 6);
if sigmaz < 1
    sigmaz = 1;
end
if mod(sigmaz, 2) == 0
    sigmaz = sigmaz + 1;
end
sigmaD = [sigmaxy, sigmaz];
sizeD = 9 * [sigmaxy, sigmaxy, sigmaz];
D = inlineDoGMask3D(sizeD, sigmaD, gamma);

if processAllTimePoints == 1
    % Clone the existing dataset without adding it to Imaris yet
    newDataSet = mDataSet.Clone();
else
    % Just an alias to current dataset
    newDataSet = mDataSet;
end

% Waitbar
hWaitbar = waitbar(0, 'Processing...');

% Process time points
if processAllTimePoints == 1
    timePointsToProcess = 1 : mDataSet.GetSizeT();
else
    timePointsToProcess = conn.mImarisApplication.GetVisibleIndexT() + 1;
end
nTimepoints = numel(timePointsToProcess);
nOps = nTimepoints * numel(channels);
op = 0;
for timepoint = timePointsToProcess
    
    for channel = 1 : numel(channels)
        
        % Get current timepoint and channel
        stack = conn.getDataVolume(channels(channel), timepoint - 1, mDataSet);
        
        % Convolve it with the DoG
        E = inlineFourierConv(stack, D, 1);
        
        % Set the stack
        switch char(newDataSet.GetType())
            case 'eTypeUInt8'
                newDataSet.SetDataVolumeAs1DArrayBytes(E(:), ...
                    channels(channel), timepoint - 1);
            case 'eTypeUInt16'
                newDataSet.SetDataVolumeAs1DArrayShorts(E(:), ...
                    channels(channel), timepoint - 1);
            case 'eTypeFloat'
                newDataSet.SetDataVolumeAs1DArrayFloats(E(:), ...
                    channels(channel), timepoint - 1);
            otherwise
                error('Bad value for newDataSet.GetType().');
        end
        
    end
    
    % Update the waitbar
    op = op + 1;
    waitbar(op / nOps, hWaitbar);
    
end

% Close the waitbar
close(hWaitbar);

% Push the new dataset into Imaris
conn.mImarisApplication.SetDataSet(newDataSet);

% =========================================================================

function [D, F] = inlineDoGMask3D(mSize, sigma, gamma)
% D=inlineDoGMask3D(mSize, sigma, gamma)
%    mSize : [y x (z)] size of the mask. (Optional) Set mSize=[] to
%            automatically set it as a function of sigma.
%            All dimensions MUST BE ODD. If mSize is only [y x], a 2D DoG
%            mask will be returned.
%    sigma : scalar or (1x2)-vector [sigmaxy sigmaz]. Sigma(s) for the 3D
%            Difference of Gaussians mask.
%            If sigma is a scalar, the same sigma will be applied for both
%            the lateral and vertical extensions. Otherwide, please specify
%            the two numbers in a vector sigma=[sigmaxy sigmaz]. In case of
%            a 2D DoG mask, if sigmaz is passed, it will be ignored.
%    gamma: (optional) scaling factor for the second Gaussian; must be
%            0>gamma<1; default = 0.5;
%
% OUTPUT
%
%    D:     2/3D difference of Gaussians mask
%    F:     2/3D Gaussian with sigma = [sigmaxy (sigmaz)].

%--------------------------------------------------------------------------
%
%   The contents of this file are subject to the Mozilla Public License
%   Version 1.1 (the "License"); you may not use this file except in
%   compliance with the License. You may obtain a copy of the License at
%   http://www.mozilla.org/MPL/
%
%   Software distributed under the License is distributed on an "AS IS"
%   basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
%   License for the specific language governing rights and limitations
%   under the License.
%
%   The Original Code is "Qu for MATLAB".
%
%   The Initial Developer of the Original Code is Aaron Ponti.
%   All Rights Reserved.
%
%--------------------------------------------------------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% CHECK INPUT PARAMETERS
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if nargin < 2 || nargin > 3
    error('Two or three input parameters expected.');
end

if numel(sigma) < 1 || numel(sigma) > 2
    error('sigma must be either a scalar or a (1x2) vector.');
end

if nargin == 2
    gamma = 0.5;
end

if gamma <= 0 || gamma >= 1
    error('gamma must be in the range 0>gamma<1');
end

% If a 2D DoG mask is requested, calculate a 3D mask with z=1
if numel(mSize) == 2
    
    mSize = [mSize 1];
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% CALCULATE DoG MASK
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if numel(sigma) == 1
    sigmaxy = sigma;
    sigmaz = sigma;
else
    sigmaxy = sigma(1);
    sigmaz = sigma(2);
end


% Cutoff radius of the gaussian kernel
if isempty(mSize)
    Rx = ceil(5*sigmaxy);
    Ry = ceil(5*sigmaxy);
    Rz = ceil(5*sigmaz);
else
    Rx = mSize(2)/2-0.5;
    Ry = mSize(1)/2-0.5;
    Rz = mSize(3)/2-0.5;
end

% Initialize 3D masks
F = zeros([2*Ry+1, 2*Rx+1, 2*Rz+1]);
S = F;
[x, y] = meshgrid(-Rx:Rx, -Ry:Ry);
z = -Rz:Rz;
is3D = 1;
if numel(z) == 1 && z(1) == 0
    is3D = 0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% First Gauss mask
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Fxy = (1./gamma^2).*exp(-(x.*x+y.*y)/(2.*gamma.^2.*sigmaxy.*sigmaxy));
if is3D == 1
    Fz = (1./gamma^2).*exp(-(z.*z)/(2.*gamma.^2.*sigmaz.*sigmaz));
    F(:) = Fxy(:)*Fz;
else
    F = Fxy;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Second Gauss mask
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Sxy = exp(-(x.*x + y.*y)/(2.*sigmaxy.*sigmaxy));
if is3D == 1
    Sz = exp(-(z.*z)/(2.*sigmaz.*sigmaz));
    S(:) = Sxy(:)*Sz;
else
    S = Sxy;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Difference of Gaussians
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
D = F - S;
if nargout == 1
    clear('F');
end
clear('S');

% All positive values in the final mask must sum up to 1 ...
pIndx = find(D > 0);
pSum = sum(D(pIndx));
D(pIndx) = D(pIndx)/pSum;

% ... and the negative values to -1
nIndx = find(D<0);
nSum = abs(sum(D(nIndx)));
D(nIndx) = D(nIndx)/nSum;

% =========================================================================

function C = inlineFourierConv(S, K, pad)
% fourierConv calculates convolution in Fourier space
%
% SYNOPSIS   C = fourierConv(S, K)
%
% INPUT      S   : signal, 2- or 3-dimensional
%            K   : kernel, 2- or 3-dimensional
%            pad : (optional, default is 0). If pad is 1, the signal S
%                  will be padded along all direction for half of the
%                  kernel support to circumvent border effects given by the
%                  convolution. Please notice that the necessary padding
%                  for performing circular convolution is independent of
%                  this setting and is applied in any case.
%
% OUTPUT     C   : convolution with the same size and class as the
%                  signal S
%
% Aaron Ponti, 2009/02/25
%              2009/03/27 : set the default value for pad to 0

%--------------------------------------------------------------------------
%
%   The contents of this file are subject to the Mozilla Public License
%   Version 1.1 (the "License"); you may not use this file except in
%   compliance with the License. You may obtain a copy of the License at
%   http://www.mozilla.org/MPL/
%
%   Software distributed under the License is distributed on an "AS IS"
%   basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
%   License for the specific language governing rights and limitations
%   under the License.
%
%   The Original Code is "Qu for MATLAB".
%
%   The Initial Developer of the Original Code is Aaron Ponti.
%   All Rights Reserved.
%
%--------------------------------------------------------------------------

% Check the inputs
if nargin < 2 || nargin > 3
    error('2 or 3 input parameters expected.');
end

if nargin == 2
    pad = 0;
end

if pad ~= 0 && pad ~= 1
    error('pad must be either 0 or 1.');
end

% This function only calculates the convolution of real numbers
if ~isreal(S) || ~isreal(K)
    error('This function does not compute the convolution of complex numbers.');
end

% Get the original class of the signal
classS = class(S);

% Convert
if ~isa(S, 'single') && ~isa(S, 'double')
    S = double(S);
    K = double(K);
end

% Get and check the sizes
dims1 = ndims(S);
dims2 = ndims(K);

if dims1 < 2 || dims1 > 3 || dims2 < 2 || dims2 > 3
    error('Only 2- or 3D stacks are supported.');
end

if dims1 ~= dims2
    error('Incompatible dimensionalities.');
end

% Should we pad?
if pad == 1
    
    % Convolution creates a 'border' effect. To prevent this, we pad the
    % signal S by half of the size of the kernel K on all dimensions
    padSize = fix(size(K) / 2) + 1;
    S = padarray(S, padSize, 'both', 'symmetric');
    
end

% Calculate the minimum size needed for performing circular convolution
% by FFT
sz1 = size(S);
sz2 = size(K);
sz = sz1 +sz2 - 1;

% Calculate how much 'border' must be removed to get the same result as
% when running conv with the SHAPE parameter set to 'same' (with the
% exception of different values at the border)
if pad == 1
    p = padSize + (sz2 - 1 + mod(sz2 - 1, 2)) / 2;
else
    p = (sz2 - 1 + mod(sz2 - 1, 2)) / 2;
    padSize = zeros(1, dims1);
end

% Since this is a convolution of real numbers, we take the real part of
% the inverse fourier transform
switch dims1
    
    case 2
        
        f1 = fft2(S, sz(1), sz(2));
        f2 = fft2(K, sz(1), sz(2));
        C  = real(ifft2(f2 .* f1));
        C = C(...
            p(1) + 1 : p(1) + sz1(1) - (2 * padSize(1)), ...
            p(2) + 1 : p(2) + sz1(2) - (2 * padSize(2)));
        
    case 3
        
        f1 = fftn(S, sz);
        f2 = fftn(K, sz);
        C  = real(ifftn(f2 .* f1));
        C  = C(...
            p(1) + 1 : p(1) + sz1(1) - (2 * padSize(1)), ...
            p(2) + 1 : p(2) + sz1(2) - (2 * padSize(2)), ...
            p(3) + 1 : p(3) + sz1(3) - (2 * padSize(3)));
        
    otherwise, error('Only 2- or 3D stacks are supported.');
        
end

% Cast back
C = cast(C, classS);
