function bsseExtrapolateFromMask(vImarisApplication)
% Extrapolate from masked channel.
%
% Copyright Aaron Ponti, BSSE, ETH Zurich, 2017
%
% <CustomTools>
%  <Menu>
%   <Submenu name="BSSE">
%    <Submenu name="Dataset Functions">
%     <Submenu name="Extrapolate from mask">
%      <Item name="Run" icon="Matlab" tooltip="Extrapolate from mask.">
%       <Command>MatlabXT::bsseExtrapolateFromMask(%i)</Command>
%      </Item>
%     </Submenu>
%    </Submenu>
%   </Submenu>
%  </Menu>
% </CustomTools>

% INITIALIZATION
% =========================================================================

% Instantiate an IceImarisConnector object
conn = IceImarisConnector(vImarisApplication);

% Is there something loaded?
if isempty(conn.mImarisApplication.GetDataSet())
    return
end

% Ask the user to specify the output folder
currentFileName = char(conn.mImarisApplication.GetCurrentFileName());
inputDir = fileparts(currentFileName);
outputDir = uigetdir(inputDir, 'Pick an output folder');
if isequal(outputDir, 0)
    return
end

% Get dataset sizes
sz = conn.getSizes();

% Some aliases
nPlanes = sz(3);
nChannels = sz(4);

% If there is more than one channel, we ask the user to pick one
if nChannels > 1

    % Collect channel names
    channelNames = cell(1, nChannels);
    for i = 1 : nChannels
    
        channelNames{i} = char( ...
            conn.mImarisApplication.GetDataSet().GetChannelName(i - 1));
    
    end

    % Ask the user to pick the channels to copy
    [channelToWorkOn, v] = listdlg( ...
        'PromptString', 'Select channel:', ...
        'SelectionMode', 'single', 'ListSize', [400 300], ...
        'ListString', channelNames);

    if v == 0
        return
    end
    
    % Store the channel name
    channelName = channelNames{channelToWorkOn};
    
else
    
    % The channel to copy is the first
    channelToWorkOn = 1;
    channelName = char(...
        conn.mImarisApplication.GetDataSet().GetChannelName(0));

end

% Build an incremental projection
% =========================================================================

% Create waitbar
hWaitbar = waitbar(0, 'Creating incremental max projection...');

maxProj = [];
for z = 0 : (nPlanes - 1)
    
    % Get current slice
    slice = conn.getDataSlice(z,channelToWorkOn - 1, 0);
    
    if z == 1
        maxProj = slice;
    else
        maxProj = max(cat(3, maxProj, slice), [], 3);
    end

    % Update the waitbar
    waitbar(z/nPlanes, hWaitbar);
end

% Close the waitbar
close(hWaitbar);

% Now process the projection (partially on the GPU)
maxProj = gpuArray(maxProj);
maxProjMask = imdilate(gpuArray(maxProj), strel('disk', 25));
maxProjMask = gather(maxProjMask);
maxProjMask = imfill(maxProjMask, 'holes');
maxProjMask = maxProjMask > 0;
maxProjMask = gather(imerode(gpuArray(maxProjMask), strel('disk', 25)));
L = bwlabel(maxProjMask, 8);
s = regionprops(maxProjMask, {'Area'});
label = find([s.Area] == max([s.Area]));
L(L ~= label) = 0;
L = L > 0;
Le = imerode(gpuArray(L), strel('disk', 25));
B = imdilate(Le, strel('disk', 25));
B = imdilate(B, strel('disk', 25));
gB = gather(B - Le);

% Save projection to disk (quality conrol)
qcImg = cast(255 .* gB, 'uint8');
imwrite(qcImg', fullfile(outputDir, 'outline.tif'));

% Create waitbar
hWaitbar = waitbar(0, 'Saving augmented planes to disk...');

% Format string
strg = sprintf('%%.%dd', length(num2str(nPlanes)));

% Save the augmented images
for z = 0 : (nPlanes - 1)
    
    % Get current slice
    slice = conn.getDataSlice(z, channelToWorkOn - 1, 0);
    
    % Estimate the signal strength
    m = single(median(slice(slice > 0)));
    
    % Blend and cast
    slice = cast(single(slice) + m .* single(gB), class(slice));
    slice = slice';

    % Output file name
    currentFileName= fullfile(outputDir, ...
        [channelName, '_aug_', sprintf(strg, z), '.tif']);
    imwrite(slice, currentFileName);

    % Update the waitbar
    waitbar(z/nPlanes, hWaitbar);

end

% Close the waitbar
close(hWaitbar);
