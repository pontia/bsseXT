function bsseFilamentsClassifyByDistance(aImarisApplicationID)
% Classifies the Filaments by distance from a selected Surface.
%
% The distance limits are specified by the user via input dialogs. The
% classified Filaments are copied to new Filaments objects.
%
%    <CustomTools> 
%      <Menu> 
%       <Submenu name="BSSE">
%        <Submenu name="Filaments Functions">
%         <Item name="Classify filaments by distance from surface" icon="Matlab"> 
%          <Command>MatlabXT::bsseFilamentsClassifyByDistance(%i)</Command> 
%         </Item> 
%        </Submenu>
%       </Submenu>
%      </Menu> 
%      <SurpassTab> 
%        <SurpassComponent name="bpFilaments"> 
%          <Item name="Classify filaments by distance from surface" icon="Matlab"> 
%            <Command>MatlabXT::bsseFilamentsClassifyByDistance(%i)</Command> 
%          </Item> 
%        </SurpassComponent> 
%      </SurpassTab> 
%    </CustomTools> 
%
% Copyright (c) 2015, Aaron Ponti

% Set up connection between Imaris and MATLAB
conn = IceImarisConnector(aImarisApplicationID);

% Get the currently selected Surfaces object in the surpass scene
sourceFilaments = conn.getSurpassSelection('Filaments');
if isempty(sourceFilaments)
    errordlg('Please select a Filaments object!');
    return
end

% Get the Surfaces objects
surfaces = conn.getAllSurpassChildren(0, 'Surfaces');
nSurfaces = numel(surfaces);
if nSurfaces == 0
        errordlg('There are no other Surfaces in the scene!');
    return
end
if nSurfaces > 1
    % Ask the user to pick one surface
    vSurface = askUserToPickSurface(surfaces);
    if isempty(vSurface)
        return
    end
else
    vSurface = surfaces{ 1 };
end

% Collect information about the filaments that we will use later
colors(1, 1 : 4) = ...
    conn.mapRgbaScalarToVector(sourceFilaments.GetColorRGBA());
names = char(sourceFilaments.GetName());
coords = [];
for j = 1 : sourceFilaments.GetNumberOfFilaments()
    coords = cat(1, coords, sourceFilaments.GetPositionsXYZ(j - 1));
end
   

% Target surface vertex coordinates (subsampled if requested)
surfaceVertexCoords = getAndSubsampleSurfaceVertes(vSurface);

% Ask the user to specify the distance limits to use
distanceLimits = askUserToSetDistanceLimits();
if isempty(distanceLimits)
    return
end

nFilamentsToProcess = sourceFilaments.GetNumberOfFilaments();
if nFilamentsToProcess > 1
    hWaitbar = waitbar(0, 'Calculating distances...');
end

% Process all the filaments
indices = zeros(1, nFilamentsToProcess);
for s = 0 : nFilamentsToProcess - 1
    
    % Current surface vertex coordinates
    currentCoords = sourceFilaments.GetPositionsXYZ(s);

    % Calculate all spot distances from the measurement point
    D = createDistanceMatrix(double(currentCoords), surfaceVertexCoords);
    D = min(D(:));

    % Now classify the Surfaces by distance and create new Surfaces objects
    % containing the Surfaces that fall in current distance bin
    indices(s + 1) = find(distanceLimits >= D, 1) - 1;

    % Update waitbar
    if nFilamentsToProcess > 1
        hWaitbar = waitbar((s + 1) / nFilamentsToProcess, hWaitbar);
    end

end

% Close waitbar
if nFilamentsToProcess > 1
    close(hWaitbar);
end

% Get unique bins
bins = unique(indices);

% Number of new surfaces to create
nSurfacesToCreate = numel(bins);
if nSurfacesToCreate > 1
    hWaitbar = waitbar(0, 'Creating new Surfaces...');
end

% Create and add surfaces
for i = 1 : numel(bins)

    % Create a new Surfaces bject
    newFilaments = conn.mImarisApplication.GetFactory().CreateFilaments();

    % Get the indices of the surfaces that fall il current bin
    indx = find(indices == bins(i));

    % Name for the new surfaces object
    name = ['Filaments ', num2str(distanceLimits(bins(i))), ...
        ' <= D < ', num2str(distanceLimits(bins(i) + 1)), ' [', ...
        num2str(numel(indx)), ']'];

    % Set the name
    newFilaments.SetName(name);

    for j = 1 : numel(indx)

        newFilaments.AddFilament( ...
            sourceFilaments.GetPositionsXYZ(indx(j) - 1), ...
            sourceFilaments.GetRadii(indx(j) - 1), ...
            sourceFilaments.GetTypes(indx(j) - 1), ...
            sourceFilaments.GetEdges(indx(j) - 1), ...
            0);
    end

    % Set the color
    newFilaments.SetColorRGBA(conn.mapRgbaVectorToScalar([rand(1, 3) 0]));

    % Add the new Spots object to the container
    conn.mImarisApplication.GetSurpassScene().AddChild(newFilaments, -1);
    
    % Update waitbar
    if nSurfacesToCreate > 1
        hWaitbar = waitbar(i / nSurfacesToCreate, hWaitbar);
    end

end

% Close waitbar
if nSurfacesToCreate > 1
    close(hWaitbar);
end

% ==== Helper functions ===================================================

% Ask the user to pick a surface from a list
function vSurface = askUserToPickSurface(surfaces)
nSurfaces = numel(surfaces);
surfaceNames = cell(1, nSurfaces);
for i = 1 : nSurfaces
    surfaceNames{i} = char(surfaces{i}.GetName());
end
[s, v] = listdlg( ...
    'Name', 'Question', ...
    'PromptString', ...
    'Choose surface from which the distances will be calculated', ...
    'SelectionMode', 'single', ...
    'ListSize', [400 300], ...
    'ListString', surfaceNames);
if v == 0
    vSurface = [];
    return;
end
vSurface = surfaces{ s };

% Ask the user to set the distance limits a surface from a list
% (we provide reasonable defaults)
function distanceLimits = askUserToSetDistanceLimits()
answer = inputdlg({'Please set distance limits:'}, 'Input', ...
    1, {'10, 20, 30, 40, 50'});
if isempty(answer)
    return;
end
distanceLimits = str2num(answer{1}); %#ok<ST2NM>
if isempty(distanceLimits)
    errordlg('Invalid distance limits!');
    distanceLimits = [];
    return;
end
distanceLimits = [0 distanceLimits Inf];

% -------------------------------------------------------------------------

function surfaceVertexCoords = getAndSubsampleSurfaceVertes(vSurface)

% Get all vertices for the selected surface
surfaceVertexCoords = [];
for i = 0 : vSurface.GetNumberOfSurfaces() - 1
    surfaceVertexCoords = cat(1, surfaceVertexCoords, vSurface.GetVertices(i));
end
vNumberOfVertices = size(surfaceVertexCoords, 1);

nEntries = fix(log10(vNumberOfVertices)) + 2;

% Prepare the listdlg
numberOfTriangleList = zeros(1, nEntries);
numberOfTriangleList(1) = 1;
numberOfTriangleList(nEntries) = vNumberOfVertices;
str = cell(1, nEntries);
str{1} = 'Use center of mass';
for i = 2 : nEntries - 1
    numberOfTriangleList(i) = 10 ^ (i - 1);
    str{i} = ['Use ', num2str(numberOfTriangleList(i)), ' triangles'];
end
str{nEntries} = ['Use ', num2str(vNumberOfVertices), ' (all) triangles'];
[s, v] = listdlg('PromptString', ...
    'Sub-sample surface:', ...
    'SelectionMode', 'single', ...
    'ListString', str);
if v == 0
    return
end
MAX_SURFACE_VERTICES = numberOfTriangleList(s);

% Sub-sample the surface(s)
if MAX_SURFACE_VERTICES == 1
    
    % Get the centers of mass
    allSurfaceVertexCoords = [];
    for i = 1 : vSurface.GetNumberOfSurfaces
        allSurfaceVertexCoords = cat(1, ...
            allSurfaceVertexCoords, vSurface.GetCenterOfMass(i - 1));
    end
    surfaceVertexCoords = double(mean(allSurfaceVertexCoords, 1));
    
else
    
    % Limit the number of surface vertices used to MAX_SURFACE_VERTICES
    if vNumberOfVertices > MAX_SURFACE_VERTICES
        vIndices = round(linspace(1, vNumberOfVertices, MAX_SURFACE_VERTICES));
        surfaceVertexCoords = surfaceVertexCoords(vIndices, :);
    end
    
    % Make sure the coordinates are of type 'double', or createDistanceMatrix
    % will fail
    surfaceVertexCoords = double(surfaceVertexCoords);

end

