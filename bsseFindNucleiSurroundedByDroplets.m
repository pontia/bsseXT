function bsseFindNucleiSurroundedByDroplets( aImarisApplicationID, showInfo )
%  Scores Nuclei (represented by a Spots object) as positive if they are
%  surrounded by a minimum, user-defined number of Droplets (represented 
% by another Spots objct).'
%
%  Installation:
%
%  - Copy this file into the XTensions folder in the Imaris installation directory
%  - You will find this function in the Image Processing menu
%
%    <CustomTools>
%      <Menu>
%       <Submenu name="BSSE">
%        <Submenu name="Spots Functions">
%         <Submenu name="Find nuclei surrounded by droplets">
%          <Item name="Run" icon="Matlab">
%            <Command>MatlabXT::bsseFindNucleiSurroundedByDroplets(%i, 0)</Command>
%          </Item>
%          <Item name="Instructions" icon="Matlab">
%            <Command>MatlabXT::bsseFindNucleiSurroundedByDroplets(%i, 1)</Command>
%          </Item>
%         </Submenu>
%        </Submenu>
%       </Submenu>
%      </Menu>
%      <SurpassTab>
%        <SurpassComponent name="bpSpots">
%          <Item name="BSSE :: Find nuclei surrounded by droplets" icon="Matlab" tooltip="Find nuclei surrounded by droplets.">
%            <Command>MatlabXT::bsseFindNucleiSurroundedByDroplets(%i, 0)</Command>
%          </Item>
%        </SurpassComponent>
%      </SurpassTab>
%    </CustomTools>
%
%  Description:
%
%  Copyright Aaron Ponti (FMI) 2013/04/03

% Instantiate the IceImarisConnector object
conn = IceImarisConnector(aImarisApplicationID);

if nargin == 1
    showInfo = 0;
elseif nargin == 2
    if showInfo ~= 0 && showInfo ~= 1
        error( 'Wrong value for showInfo (must be either 0 or 1).' );
    end
else
    error( 'Wrong number of input arguments.' );
end

if showInfo == 1
    show;
    return
end

% Check for existance of the Surpass Scene
if isempty( conn.mImarisApplication.GetSurpassScene() )
    msgbox( 'Please create the relevant objects in the surpass scene!' )
    return
end

% =========================================================================
%
% Get spot objects ad assign them to nuclei vs. droplets
%
% =========================================================================

% Find all spots objects at the root level
spots = conn.getAllSurpassChildren(0, 'Spots');
nSpots = numel(spots);

if nSpots < 2
    uiwait( errordlg( ...
        'You need two "Spots" objects for this XTensions!', ...
        'Error', 'modal' ) );
    return;
end


spotNames = cell( 1, nSpots );
for i = 1 : nSpots
    spotNames{ i } = char(spots{ i }.GetName());
end

[ s, v ] = listdlg( ...
    'Name', 'Question', ...
    'PromptString', ...
    'Choose Spots object representing nuclei', ...
    'SelectionMode', 'single', ...
    'ListSize', [ 400 200 ], ...
    'ListString', spotNames );
if v == 0
    return;
end
spotsNuclei = spots{ s };

% Now remove the selected Spots object from the list
spots(s) = [];
spotNames(s) = [];
nSpots = numel(spots);

% It there is only one spot left, we take it as the one representing the
% droplets.
if nSpots == 1
    spotsDroplets = spots{1};
else
    % We have to ask the user to pick the droplets
    [ s, v ] = listdlg( ...
        'Name', 'Question', ...
        'PromptString', ...
        'Choose Spots object representing droplets', ...
        'SelectionMode', 'single', ...
        'ListSize', [ 400 200 ], ...
        'ListString', spotNames );
    if v == 0
        return;
    end
    spotsDroplets = spots{ s };
end

% =========================================================================
%
% Ask the user to define max allowed distance from droplets to nuclei for a
% positive score, and minumum number of droplets to be assign to a nucleus
% for it to be scored as positive.
%
% =========================================================================

answer = inputdlg( ...
    {['Maximum allowed distance (units) from the Nucleus for a Droplet to be ', ...
    'assignd to it:'], ...
    ['Minimum number of Droplets assigned to a Nucleus to consider ', ...
    'it positive:']}, ...
    'Input requested', ...
    1, {'100', '3'});

if isempty(answer)
    return
end

maxDistance = str2double(answer{1});
if isnan(maxDistance)
    uiwait(errordlg('Bad value for maximum allowed distance.', ...
        'Error', 'modal'));
    return
end

minNumAssigned = str2double(answer{2});
if isnan(minNumAssigned)
    uiwait(errordlg('Bad value for minimum number of assigned Droplets.', ...
        'Error', 'modal'));
    return
end

% =========================================================================
%
% Process the spots
%
% =========================================================================

% Get all Nuclei coordinates
measNuclei = spotsNuclei.Get();
nucleiPos = measNuclei.mPositionsXYZ;
nucleiTimepoints = measNuclei.mIndicesT;
nucleiRadii = measNuclei.mRadii;

% Get all Droplets coordinates
measDroplets = spotsDroplets.Get();
dropletsPos = measDroplets.mPositionsXYZ;
dropletsTimepoints= measDroplets.mIndicesT;
dropletsRadii = measDroplets.mRadii;

numDropletsPerNucleus = zeros(size(nucleiPos, 1), 1);
assignedDroplets      = zeros(size(dropletsPos, 1), 1);

% Now go over all spotDroplets and assign them to the closest nucleus
for i = 1 : size(dropletsPos, 1)
   
    D = createDistanceMatrix(double(dropletsPos(i, :)), double(nucleiPos));
    
    % Find the closest nucleus
    minD = min(D);
    if minD <= maxDistance
        nucleusIndex = find(D == minD, 1);
        
        % Add the droplet to the number of assigned droplets to the nucleus
        numDropletsPerNucleus(nucleusIndex) = ... 
        numDropletsPerNucleus(nucleusIndex) + 1;
        
        % Mark the droplet as assigned
        assignedDroplets(i) = 1;
    end
    
end

% Now find all nuclei that have the min requested number of droplets 
% associated and the droplets that were asigned
positiveNuclei = find(numDropletsPerNucleus >= minNumAssigned);
assignedDroplets = find(assignedDroplets);

% =========================================================================
%
% Create a Spots object with positive Nuclei and one with the assigned
% particles (add them to a result subfolder)
%
% =========================================================================

% % Create a new "Result" folder and add it to the Surpass Scene
newGroup = conn.mImarisApplication.GetFactory().CreateDataContainer();
newGroup.SetName('Results');
conn.mImarisApplication.GetSurpassScene().AddChild(newGroup, -1);

% Create and add the "positiveNuclei" spots to the container
positiveNucleiPos = nucleiPos(positiveNuclei, :);
positiveNucleiTimepoints = nucleiTimepoints(positiveNuclei);
positiveNucleiRadii = nucleiRadii(positiveNuclei);

conn.createAndSetSpots(positiveNucleiPos, positiveNucleiTimepoints, ...
    positiveNucleiRadii, 'Positive Nuclei', ...
    [rand( 1 ), rand( 1 ), rand( 1 ), 0], newGroup);

% Create and add the "assignedDroplets" spots to the container
positiveDropletsPos = dropletsPos(assignedDroplets, :);
positiveDropletsTimepoints = dropletsTimepoints(assignedDroplets);
positiveDropletsRadii = dropletsRadii(assignedDroplets);

conn.createAndSetSpots(positiveDropletsPos, positiveDropletsTimepoints, ...
    positiveDropletsRadii, 'Assigned Droplets', ...
    [rand( 1 ), rand( 1 ), rand( 1 ), 0], newGroup);

% =========================================================================

function show

msg = { ...
    'This XTension scores Nuclei (represented by a Spots object) ', ...
    'as positive if they are surrounded by a minimum, user-defined ', ...
    'number of Droplets (represented by another Spots objct).', ...
    '',...
    'The positive Nuclei are copied into a new Spots object.', ...
    '', ...
    '' };

uiwait( msgbox( msg , 'Help' ) );

