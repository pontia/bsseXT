function bsseGetCantileverAndCellCenterOfMassCoords(vImarisApplication)
% Extracts and calculates coordinates of measurement points and surface outline center of mass
%
% Copyright Aaron Ponti, BSSE, ETH Zurich, 2015
%
% <CustomTools>
%  <Menu>
%   <Submenu name="BSSE">
%    <Submenu name="Surfaces Functions">
%      <Item name="Export measurement points and surface center of mass coordinates" icon="Matlab" tooltip="Extracts and calculates coordinates of measurement points and surface outline center of mass.">
%       <Command>MatlabXT::bsseGetCantileverAndCellCenterOfMassCoords(%i)</Command>
%      </Item>
%    </Submenu>
%    <Submenu name="Measurement Points Functions">
%      <Item name="Export measurement points and surface center of mass coordinates" icon="Matlab" tooltip="Extracts and calculates coordinates of measurement points and surface outline center of mass.">
%       <Command>MatlabXT::bsseGetCantileverAndCellCenterOfMassCoords(%i)</Command>
%      </Item>
%    </Submenu>
%   </Submenu>
%  </Menu>
%      <SurpassTab> 
%        <SurpassComponent name="bpSurfaces"> 
%         <Item name="Export measurement points and surface center of mass coordinates" icon="Matlab" tooltip="Extracts and calculates coordinates of measurement points and surface outline center of mass.">
%          <Command>MatlabXT::bsseGetCantileverAndCellCenterOfMassCoords(%i)</Command>
%         </Item>
%        </SurpassComponent> 
%        <SurpassComponent name="bpMeasurementPoints"> 
%         <Item name="Export measurement points and surface center of mass coordinates" icon="Matlab" tooltip="Extracts and calculates coordinates of measurement points and surface outline center of mass.">
%          <Command>MatlabXT::bsseGetCantileverAndCellCenterOfMassCoords(%i)</Command>
%         </Item>
%        </SurpassComponent> 
%      </SurpassTab> 
% </CustomTools>

% Instantiate an IceImarisConnector object
conn = IceImarisConnector(vImarisApplication);

% =========================================================================
%
% This only works on 2D datasets!
%
% =========================================================================

% Is there something loaded?
aDataset = conn.mImarisApplication.GetDataSet();
if isempty(aDataset)
    return
end

if (aDataset.GetSizeZ() > 1)
    uiwait(errordlg('This function only works on 2D datasets!', ...
        'Error', 'modal'));
    return;
end


% =========================================================================
%
% Get all measurement points and surface objects
%
% =========================================================================

% Get the mesaurement points
measurementPoints = conn.getAllSurpassChildren(1, 'MeasurementPoints');
if numel(measurementPoints) ~= 1
    uiwait(errordlg('One measurement points object expected in the scene!', ...
        'Error', 'modal'));
    return;
end

% Get the surfaces
surfaces = conn.getAllSurpassChildren(1, 'Surfaces');
if numel(surfaces) ~= 1
    uiwait(errordlg('One surfaces object expected in the scene!', ...
        'Error', 'modal'));
    return;
end

% =========================================================================
%
% Ask the user whether the center of mass should be geometric or weighted
%
% =========================================================================

choice = questdlg(...
    'How should the center of mass of the surfaces be calculated?', ...
    'Question', ...
	'Geometrically', 'Weighted by the signal', 'Geometrically');
switch choice
    case 'Geometrically', calcCMgeometrically = 1;
    case 'Weighted by the signal', calcCMgeometrically = 0;
    otherwise, return
end

% =========================================================================
%
% Ask the user to pick the file where the coordinates will be saved
%
% =========================================================================

[outFileName, outDirName] = uiputfile('coords.tsv', ...
    'Please choose where to store the results...');
if isequal(outFileName, 0)
    return
end
outFile = fullfile(outDirName, outFileName);

% =========================================================================
%
% Extract measurement point coordinates
%
% =========================================================================

% Get the measurement point
currentPoint = measurementPoints{1};
    
% Get the point names
name = currentPoint.GetName();
nNames = currentPoint.GetNames();

% Get the coordinates
coords = currentPoint.GetPositionsXYZ();

% Check that there are exactly two points
if size(coords, 1) ~= 2
    uiwait(errordlg('Please set two points in the scene!', ...
        'Error', 'modal'));
    return;
end

% Starting point of the direction vector
P = coords(1, :);

% Calculate the direction vector
A = coords(2, 1:2) - coords(1, 1:2);
A = A ./ norm(A);

% =========================================================================
%
% Go over all time points and extract all surface coordinates
%
% =========================================================================

% Get the surface
currentSurface = surfaces{1};

% Make sure there are surfaces and not just outlines
nSurfs = currentSurface.GetNumberOfSurfaces;
    
if nSurfs == 0
    uiwait(errordlg('Could not create result file.', 'Error', 'modal'));
    return;
end

% Prepare space to store the results
results = cell(1, aDataset.GetSizeT());

for surfIndex = 1 : nSurfs

    % Initialize current result
    res = struct();
    
    % Get the surface time point
    res.timeIndex = currentSurface.GetTimeIndex(surfIndex - 1);
    
    if ~isempty(results{res.timeIndex + 1})
        disp(['There is already an analyzed surface for time index ', ...
            num2str(res.timeIndex), '. Skipping...']);
        continue;
    end
    
    timeString = char(currentSurface.GetTimePoint(surfIndex - 1));
    [Y, M, D, h, m, s] = parseTimeString(timeString);
     res.timePoint = datetime([Y, M, D, h, m, s]);
    
    % Calculate the center of mass of the surface
    if calcCMgeometrically == 1
        CMcoords = mean(currentSurface.GetVertices(surfIndex - 1), 1);
    else
        CMcoords = currentSurface.GetCenterOfMass(surfIndex - 1);
    end

    % We ignore the third coordinate
    C = CMcoords(1 : 2);

    % The center of mass of the surface should theoretically fall on the 
    % main axis of the cantilever. What we need is the distance from point
    % 'A' extracted above. So we find the point of intersection of the line
    % given by the major axis and its perpendicular line goung throug the 
    % center of mass of the surface. We then calculate the distance of the
    % interestion from point 'A'.

    % Vector perpendicular to dV
    B = [A(2) -A(1)];

    % Calculate the intercept
    u = (C(2)*A(1) - P(2)*A(1) - C(1)*A(2) + P(1)*A(2)) / (B(1)*A(2) - B(2)*A(1));
    t = (C(1) + u*B(1) - P(1)) / A(1);
    x = P(1) + t*A(1);
    y = P(2) + t*A(2);

    % Shifted center of mass
    res.shiftedCenterOfMass = single([x, y, CMcoords(1, 3)]);

    % Create a new spot to show the point
    vSizes = conn.getVoxelSizes();

    % Distance of the shifted center of mass from the
    res.dSCMFromP1 = norm(res.shiftedCenterOfMass - P);
    
    % Store the result
    results{res.timeIndex + 1} = res;

end

% Make sure that we did not skip any surfaces
results(cellfun(@isempty, results)) = [];

% Create a quality control spot
xyzPos = [];
timeIndices = [];
for i = 1 : numel(results)
    xyzPos = cat(1, xyzPos, results{i}.shiftedCenterOfMass);
    timeIndices = cat(1, timeIndices, results{i}.timeIndex);
end
radii = round(5 * vSizes(1)) .* ones(numel(results), 1);
conn.createAndSetSpots(xyzPos, timeIndices, radii, 'Intercepts', ...
    [rand(1,3), 0]);

% =========================================================================
%
% Write to file
%
% =========================================================================

% Open file
fid = fopen(outFile, 'w');
if fid == -1
    uiwait(errordlg('Could not create result file.', 'Error', 'modal'));
    return;
end

% Write header
fprintf(fid, '%32s\t%16s\t%16s\t%16s\t%16s\t%16s\t%16s\t%16s\n', ...
    '', 'Time index', 'Time (s)', 'Label', 'X', 'Y', 'Z', ...
    ['Distance from ', char(nNames(1))]);

% Store the measurement point
fprintf(fid, '%32s\t%16d\t%16.4f\t%16s\t%16.4f\t%16.4f\t%16.4f\t%16.4f\n', ...
    char(name), 0, 0, char(nNames(1)), P(1), P(2), P(3), 0);

fprintf(fid, '\n');

% Store the surfaces
for i = 1 : numel(results)
    fprintf(fid, '%32s\t%16d\t%16.4f\t%16s\t%16.4f\t%16.4f\t%16.4f\t%16.4f\n', ...
        'Corrected surface center of mass', ...
        results{i}.timeIndex, ...
        seconds(results{i}.timePoint - results{1}.timePoint), ...
        '', ...
        results{i}.shiftedCenterOfMass(1), ...
        results{i}.shiftedCenterOfMass(2), ...
        results{i}.shiftedCenterOfMass(3), ...
        results{i}.dSCMFromP1);
end

% Close file
fclose(fid);

% =========================================================================

function [Y, M, D, h, m, s] = parseTimeString(timeString)

Y = str2double(timeString(1:4));
M = str2double(timeString(6:7));
D = str2double(timeString(9:10));
h = str2double(timeString(12:13));
m = str2double(timeString(15:16));
s = str2double(timeString(18:end));
