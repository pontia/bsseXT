%
%
%  Reads the timestamps from file and sets them to the dataset.
%
%
%  The time stamps must be in an XLSX file with two columns, one with the 
%  time stamp, and one with the integer index of the time point. The time
%  stamps are expected to be in seconds (with resolution in the ms range).
%  (Precisely, this is the format exported by the Nikon Elements software).
%
%  Copyright Aaron Ponti (FMI, BSSE) 2014/10/24
%
%  Installation:
%
%  - Copy this file into the XTensions folder in the Imaris installation directory
%  - You will find this function in the Image Processing menu
%
%    <CustomTools>
%      <Menu>
%       <Submenu name="BSSE">
%        <Submenu name="Dataset Functions">
%         <Item name="Import timestamps from file" icon="Matlab">
%           <Command>MatlabXT::bsseImportTimeStamps(%i)</Command>
%         </Item>
%        </Submenu>
%       </Submenu>
%      </Menu>
%    </CustomTools>
%
%

function bsseImportTimeStamps(aImarisApplicationID)

% Instantiate an IceImarisConnector object
conn = IceImarisConnector(aImarisApplicationID);

% Check that the datatset exists and has more than one timepoint
aDataset = conn.mImarisApplication.GetDataSet();
if isempty(aDataset)
    uiwait(errordlg('Please load a dataset first!', 'Error', 'modal'));
    return;
end

nTimepoints = aDataset.GetSizeT();
if nTimepoints < 2
    uiwait(errordlg('A time series is expected!', 'Error', 'modal'));
    return;
end

% Ask the user to pick the file
[fName, dirName] = ...
    uigetfile('*.xlsx', 'Please pick the tab-separated value file with the time stamps');
if isequal(fName, 0);
    return;
end
fileName = fullfile(dirName, fName);

% Try importing the timestamps
% data = xlsread(fileName, 1);
data = customXLSRead(fileName);

% Check the content
[nImportedTimestamps, nCols] = size(data);

if nImportedTimestamps ~= nTimepoints
    uiwait(errordlg(['The file contains ', num2str(nImportedTimestamps), ...
        ' timestamps. Expected were ', num2str(nTimepoints), '. ', ...
        'Aborting.'], ...
        'Error', 'modal'));
    return;
end

if nCols ~= 2
    uiwait(errordlg(['Found ', num2str(nCols), ' columns but expecting 2: ', ...
        'one with the timestamps, and one with the corresponding time ', ...
        'index. Aborting.'], ...
        'Error', 'modal'));
    return;
end

% Make sure that the timestamps are sorted
[~, p] = sort(data(:, 2));
data = data(p, :);

% Since the time stamps are relative times only, we build the time stamps 
% on todays date starting at 0:00:00
today = datevec(now());
today(4:6) = [0 0 0];

% Initialize waitbar
hWaitbar = waitbar(0, 'Setting timestamps...');

% Build and set the timestamps
for i = 1 : nTimepoints
    
    % Set current timestamp
    aDataset.SetTimePoint(i - 1, mapTimeToTimeString(data(i, 1), today));
    
    % Update waitbar
    waitbar(i/nTimepoints, hWaitbar);
end

% Close waitbar
close(hWaitbar);

function timeString = mapTimeToTimeString(time, today)

today(6) = today(6) + time;
timeString = datestr(datenum(today), 'yyyy-mm-dd HH:MM:SS.FFF');

function data = customXLSRead(filename, indices)

if nargin == 1
    sheet_name = 'Recorded Data';
    indices = [1 2];
end

%% Import the data
[~, ~, raw] = xlsread(filename);

% Drop the header and the first empty row
raw = raw(3:end, :);

%% Create output variable
data = reshape([raw{:}],size(raw));

%% Allocate imported array to column variable names
data = data(:, indices);
