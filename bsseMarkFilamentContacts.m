%
%
%  Marks point of contact (proximity) between filaments by placing spots.
%  Only visible filaments will be considered.
%
%  Copyright Aaron Ponti (FMI, BSSE) 2014/10/29
%
%  Installation:
%
%  - Copy this file into the XTensions folder in the Imaris installation directory
%  - You will find this function in the Image Processing menu
%
%    <CustomTools>
%      <Menu>
%       <Submenu name="BSSE">
%        <Submenu name="Filament Functions">
%         <Item name="Marks points of contact between filaments" icon="Matlab">
%           <Command>MatlabXT::bsseMarkFilamentContacts(%i)</Command>
%         </Item>
%        </Submenu>
%       </Submenu>
%      </Menu>
%    </CustomTools>
%

function bsseMarkFilamentContacts(id)

DEBUG = true;

% Instantiate IceImarisConnector
conn = IceImarisConnector(id);

% Get all filaments in the surpass scene
filaments = conn.getAllSurpassChildren(1, 'Filaments');

% Number of filaments
nFilaments = numel(filaments);

% Only visible filamenbts will be used
toBeDel = [];
n = 0;
for i = 1 : nFilaments
    if filaments{i}.GetVisible() == 0
        n = n + 1;
        toBeDel(n) = i;
    end
end
if ~isempty(toBeDel)
    filaments(toBeDel) = [];
    nFilaments = numel(filaments);
end

% Check that we have the expected number
if numel(filaments) < 2
    uiwait(errordlg('At least two (enabled) filament objects needed.', ...
        'Error', 'modal'));
    return;
end

% Ask the user to specify a distance threshold
answer = inputdlg(...
    'Distance threshold to consider two filaments close to each other', ...
    'Input requested', 1, {'1'});
if isempty(answer)
    return
end
threshold = str2double(answer);
if isnan(threshold)
    uiwait(errordlg('Invalid value for the distance thershold!', ...
        'Error', 'modal'));
    return;
end    

% Collect information about the filaments that we will use later
colors = zeros(nFilaments, 4);
coords = cell(1, nFilaments);
names = cell(1, nFilaments);

for i = 1 : nFilaments
    
    colors(i, 1 : 4) = ...
        conn.mapRgbaScalarToVector(filaments{i}.GetColorRGBA());

    names{i} = char(filaments{i}.GetName());

    tmp = [];
    for j = 1 : filaments{i}.GetNumberOfFilaments()
        
        tmp = cat(1, tmp, filaments{i}.GetPositionsXYZ(j - 1));
        
    end
    
    coords{i} = tmp;
    
end

% Now process all filaments
for i = 1 : nFilaments - 1
    for j = i + 1 : nFilaments

        % Calculate distances
        d = createSparseDistanceMatrix(double(coords{i}), ...
            double(coords{j}), threshold);

        % Get all the close positions
        [y, x] = find(d);
        
        % Now simplify the clusters
        A = double(coords{i}(y, :));
        B = double(coords{j}(x, :));
        
        D = createDistanceMatrix(A, B);
        
        intThreshold = 2 * threshold + threshold / 1000; 
        if any(D > intThreshold)
            
            % There is more than one cluster
            ind = knnsearch(A, B);
            
            coords_Spot_first = [];
            
            % Get the closest matches per cluster
            uInd = unique(ind);
            for k = 1 : numel(uInd)

                row = D(uInd(k), :);
                
                M = A(uInd(k), :);
                N = B(find(row == min(row), 1), :);
                coords_Spot_first = cat(1, coords_Spot_first, 0.5 .* (M + N));

            end

            % Simplify centers of clusters
            Dr = createDistanceMatrix(coords_Spot_first, coords_Spot_first);
            
            coords_Spot = [];
            
            finished = false;
            l = 1;
            while ~finished
               
                % Get lth row
                row = Dr(l, :);
                
                % Extract all positions within threshold (that haven't been
                % processed yet).
                cluster = find(row <= intThreshold);
                cluster(cluster < l) = [];
                centroid = mean(coords_Spot_first(cluster, :), 1);
                
                % Update centroid
                coords_Spot = cat(1, coords_Spot, centroid);
                
                l = l + numel(cluster);

                if l > size(coords_Spot_first, 1)
                    finished = true;
                end

            end
            
            nSpots = size(coords_Spot, 1);
            
        else

            % Only one cluster
            [a, b] = find(D == min(D( D > 0)), 1);
            
            coords_Spot = 0.5 .* (A(a, :) + B(b, :));
            nSpots = numel(a);
            
        end
        
        if DEBUG == true
            h = figure;
            plot(A(:, 2), A(:, 1), 'ro');
            hold on;
            plot(B(:, 2), B(:, 1), 'bo');
            plot(coords_Spot(:, 2), coords_Spot(:, 1), 'kx');
            title('Press a key to continue');
            close(h);
        end

        % Create spots
        conn.createAndSetSpots(...
            single(coords_Spot), zeros(nSpots, 1), ...
            ones(nSpots, 1), [names{i}, ' - ', names{j}], ...
            colors(j, :));

    end
end



