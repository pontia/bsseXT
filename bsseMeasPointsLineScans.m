%
%
%  Extracts 3D linescans along lines between consecutive measurement points
%
%  Copyright Aaron Ponti (FMI, BSSE) 2011/01/18
%
%  Installation:
%
%  - Copy this file into the XTensions folder in the Imaris installation directory
%  - You will find this function in the Image Processing menu
%
%    <CustomTools>
%      <Menu>
%       <Submenu name="BSSE">
%        <Submenu name="Measurement Points Functions">
%         <Item name="Extract line scans" icon="Matlab">
%           <Command>MatlabXT::bsseMeasPointsLineScans(%i)</Command>
%         </Item>
%        </Submenu>
%       </Submenu>
%      </Menu>
%    </CustomTools>
%
%

function bsseMeasPointsLineScans(aImarisApplicationID)

% -------------------------------------------------------------------------
%
% GET THE IMARIS APPLICATION ID
%
% If this function is started from MATLAB instead than from Imaris, first
% start the Imaris server and Imaris).
%
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
%
% Pass the aImarisApplicationID to the ImarisConnector
%
% -------------------------------------------------------------------------

conn = IceImarisConnector( aImarisApplicationID );

% -------------------------------------------------------------------------
%
% Check for the existing objects in the surpass scene
%
% If this function is started from MATLAB instead than from Imaris, first
% start the Imaris server and Imaris).
%
% -------------------------------------------------------------------------

vSurpassScene = conn.mImarisApplication.GetSurpassScene();
if isempty( vSurpassScene )
    msgbox( 'No surpass scene found!' );
    return;
end

% The Measurement Point must be selected
vMeasPoints = conn.getSurpassSelection('MeasurementPoints');
if isempty( vMeasPoints )
    uiwait( errordlg( 'Please select a ''Measurement Points'' object!', ...
        'Error', 'modal' ) );
    return
end

% -------------------------------------------------------------------------
%
% Create linescans between Measurement Points over all channels
%
% -------------------------------------------------------------------------

% Get all information from the measurement points
meas =  vMeasPoints.Get( );
positions = meas.mPositionsXYZ;
timepoints = meas.mIndicesT;
names = cell(meas.mNames);

% Get the positions in voxels
positionsVoxels = conn.mapPositionsUnitsToVoxels( positions );

% Start position 
startPosition = positions( 1, : );

% Number of measurement points
nMeasPoints = size( positionsVoxels, 1 );

% Only one  timepoint supported
if numel( unique( timepoints ) ) > 1
    uiwait( errordlg( ...
        [ 'Building linescans over more than one time point is not ', ...
        'supported!' ], 'Error', 'modal' ) );
    return
end

% Allocate a cell array to store the results
allPositions   = cell( nMeasPoints - 1, 1 );
allIntensities = cell( nMeasPoints - 1, conn.mImarisApplication.GetDataSet().GetSizeC() );

% Go over all segments and extracts the intensities from all channels
for i = 1 : nMeasPoints - 1
    
    % Get the number of steps along the linescan in all directions
    x0 = positionsVoxels( i, 1 ); x = positionsVoxels( i + 1, 1 );
    y0 = positionsVoxels( i, 2 ); y = positionsVoxels( i + 1, 2 );
    z0 = positionsVoxels( i, 3 ); z = positionsVoxels( i + 1, 3 );
    n = round( sqrt( ( x - x0 ) ^ 2 +  ( y - y0 ) ^ 2 + ( z - z0 ) ^ 2 ) );

    % Get n coordinates along the linescan
    x = linspace( x0, x, n ); x = x( : );
    y = linspace( y0, y, n ); y = y( : );
    z = linspace( z0, z, n ); z = z( : );
    
    % Drop the last one not to repeat positions at the transition
    x = x( 1 : end - 1 );
    y = y( 1 : end - 1 );
    z = z( 1 : end - 1 );

    % Store the interpolated positions in dataset units
    allPositions{ i } = conn.mapPositionsVoxelsToUnits( [ x y z ] );
    
    % Extract all intensities (round coordinates onto the voxel grid)
    for k = 1 : conn.mImarisApplication.GetDataSet().GetSizeC()

        % Calculate and store the results in the global array
        allIntensities{ i, k } = getWeightedIntensities( k, [ x y z ] );
        
    end

end

% -------------------------------------------------------------------------
%
% Store the results to file
%
% -------------------------------------------------------------------------

% Prepare the report
OUTPUTDIR = getOutputDir;
tmpfile = [ OUTPUTDIR, filesep, 'LineScans.txt' ];
fid = fopen( tmpfile, 'w' );
if fid == -1
    disp( 'Could not create report file. Writing to console.' );
    fid = 1;
end

% Write the header
fprintf( fid, '%4s\t%8s\t%8s\t%8s\t%8s\t', 'Name', 'x', 'y', 'z', 'Distance' );
for i = 1 : conn.mImarisApplication.GetDataSet().GetSizeC()
    fprintf( fid, '%12s\t', [ 'Channel ', num2str( i ) ] );
end
fprintf( fid, '\n' );

% Write the data
totalDist = 0;
for i = 1 : nMeasPoints - 1
    for j = 1 : numel( allIntensities{ i, 1 } )
        if j == 1
            fprintf( fid, '%4s\t', names{ i } );
        else
            fprintf( fid, '%4s\t', '' );
        end
        d = allPositions{ i }( j, : ) - startPosition;
        dist = sqrt( d( 1 ) ^ 2 + d( 2 ) ^ 2 + d( 3 ) ^ 2 );
        totalDist = totalDist + dist;
        fprintf( fid, '%8.4f\t%8.4f\t%8.4f\t%8.4f\t', ...
            allPositions{ i }( j, 1 ), ...
            allPositions{ i }( j, 2 ), ...
            allPositions{ i }( j, 3 ), ...
            totalDist );
        for k = 1 : conn.mImarisApplication.GetDataSet().GetSizeC()
            fprintf( fid, '%12.4f\t', allIntensities{ i, k }( j ) );
        end
        fprintf( fid, '\n' );
        
        % Update 'startPosition' to point to current one, so that the distance 
        % step is alwar related to last position
        startPosition = allPositions{ i }( j, : );
    end
end

% Close the file
if fid ~= 1
    fclose( fid );
end

% Show the file
if fid ~= 1
    edit( tmpfile );
end

% -------------------------------------------------------------------------
%
% Functions
%
% -------------------------------------------------------------------------

% ========================================================================= 

    function OUTPUTDIR = getOutputDir
        
        % Directory where to store the file
        OUTPUTDIR=getenv('TMP');
        if isempty(OUTPUTDIR)
            OUTPUTDIR=getenv('TEMP');
            if isempty(OUTPUTDIR)
                OUTPUTDIR=getenv('HOME');
                if isempty(OUTPUTDIR)
                    OUTPUTDIR=pwd;
                end
            end
        end
    end

% =========================================================================

    function intensities = getWeightedIntensities( channel, positions )
        
        % Since the dataset is transposed (see call to
        % GetDataSubVolumeAs1DArray below), we use x
        % as the first dimension

        % A constant
        nPositions = size( positions, 1 );
        
        % Allocate memory to store all intensities
        intensities = zeros( nPositions, 1 );
        
        % Coordinate of the pixel on the left, up, above. They must be of
        % type integer to be passed on to the GetDataSubVolume() methods.
        pos0 = fix( positions );
        
        for pos = 1 : nPositions
            
            % Weights
            dX = positions( pos, 1 ) - pos0( pos, 1 );
            dY = positions( pos, 2 ) - pos0( pos, 2 );
            dZ = positions( pos, 3 ) - pos0( pos, 3 );

            % Make sure to stay within the dataset bounderies
            extX = 2; extY = 2; extZ = 2;
            if pos0( pos, 1 ) == conn.mImarisApplication.GetDataSet().GetSizeX()
                extX = 1;
            end
            if pos0( pos, 2 ) == conn.mImarisApplication.GetDataSet().GetSizeY()
                extY = 1;
            end
            if pos0( pos, 3 ) == conn.mImarisApplication.GetDataSet().GetSizeZ()
                extZ = 1;
            end

            % Create weight matrix
            mask = ones( extX, extY, extZ );
            if extX == 2
                mask( 1, :, : ) = mask( 1, :, : ) .* ( 1 - dX );
                mask( 2, :, : ) = mask( 2, :, : ) .* dX;
            end
            if extY == 2
                mask( :, 1, : ) = mask( :, 1, : ) .* ( 1 - dY );
                mask( :, 2, : ) = mask( :, 2, : ) .* dY;
            end
            if extZ == 2
                mask( :, :, 1 ) = mask( :, :, 1 ) .* ( 1 - dZ );
                mask( :, :, 2 ) = mask( :, :, 2 ) .* dZ;
            end

            % Get the subvolume in the right shape (although 'transposed')
            vol = conn.getDataSubVolume(pos0( pos, 1 ) - 1, ...
                pos0( pos, 2 ) - 1, pos0( pos, 3 ) - 1, ...
                ( channel - 1 ), 0, extX, extY, extZ );      
            
            % Now use this mask to calculate a weighted average of the spot
            % intensity
            weighted = mask .* double( vol );
            intensities( pos ) = sum( weighted( : ) );
            
        end
    end

end

