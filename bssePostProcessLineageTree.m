function bssePostProcessLineageTree(imarisApplicationId)
% Calculate overlap of lineage trees
%
%    <CustomTools>
%      <Menu>
%       <Submenu name="BSSE">
%        <Submenu name="Spots Functions">
%         <Item name="Post-process lineage tree" icon="Matlab">
%          <Command>MatlabXT::bssePostProcessLineageTree(%i)</Command>
%         </Item>
%        </Submenu>
%       </Submenu>
%      </Menu>
%    </CustomTools>

% Instantiate IceImarisConnector object
conn = IceImarisConnector(imarisApplicationId);

% Is there something loaded?
mDataSet = conn.mImarisApplication.GetDataSet();
if isempty(mDataSet)
    return
end

% Get the spots
spots = conn.getAllSurpassChildren(1, 'Spots');
if isempty(spots)
    uiwait(errordlg('No Spots objects found in the scene!'));
    return;
end

if isempty(spots)
    uiwait(errordlg('No Spots objects in the scene!'));
    return;
end

if numel(spots) == 1
    
    spot = spots{1};
    
else
    
    % Get spot names
    spotNames = cell(1, numel(spots));
    for i = 1 : numel(spots)
        spotNames{i} = char(spots{i}.GetName());
    end
    
    % Ask the user to specify the first Spots object
    [s, v] = listdlg('PromptString', ...
        'Please pick the first Spots object',...
        'SelectionMode', 'single', ...
        'ListSize', [300 100], ...
        'ListString', spotNames);
    if v == 0
        return
    end
    
    % Get the "all cells" Spots object
    spot = spots{s};

end

% Build the lineage tree
trees = buildLineage(spot);

% Find branches that die immediately and purge them from the Spot object.
% The updated spot object is added to Imaris
spotIdsToDelete = processTree(trees, spot);
if isempty(spotIdsToDelete)
    uiwait(helpdlg('No branches needed to be removed.'));
    return;
end

% Get the spot IDs
spotIDs = spot.GetIds();

% Get the spot values
positionsXYZ = spot.GetPositionsXYZ();
timeIndices = spot.GetIndicesT();
radiiXYZ = spot.GetRadiiXYZ();

% Remove the spots selected above
indices = false(size(spotIDs));
for i = 1 : numel(spotIdsToDelete)
    indices = indices | (spotIDs == spotIdsToDelete(i));
end
positionsXYZ(indices, :) = [];
timeIndices(indices, :) = [];
radiiXYZ(indices, :) = [];

% Push the Spots object back
conn.createAndSetSpots(positionsXYZ, timeIndices, radiiXYZ, ...
    ['Processed branches for', char(spot.GetName())], [rand(1, 3), 0]);

% Inform
uiwait(helpdlg(['Deleted ', num2str(numel(spotIdsToDelete)), ' spots.']));

% =========================================================================

function trees = buildLineage(spot)

% Get spot ids
spotIds = spot.GetIds();

% Get spot information
spotTimes = spot.GetIndicesT();

% Get track ids and egdes
trackIds = spot.GetTrackIds();
uTrackIds = unique(trackIds);
trackEdges = spot.GetTrackEdges();

% We create a tree per track id and store the timepoint of origin in
% the root of the tree
trees = cell(1, numel(uTrackIds));

for i = 1 : numel(uTrackIds)

    currTrackId = uTrackIds(i);
    currTrack = trackIds == currTrackId;
    currTrackEdges = trackEdges(currTrack, :);

    % Get the timepoint of origin
    spotIndx = find(spotIds == currTrackEdges(1, 1));
    originTimepoint = spotTimes(spotIndx);
    
    % We use the great tree class by Jean-Yves Tivenez
    lineage = tree(originTimepoint);

    for j = 1 : size(currTrackEdges, 1)
        
        currentEdge = currTrackEdges(j, :);
        
        if j == 1
            [lineage, parent] = lineage.addnode(1, currentEdge(1));
        else
            parent = find(lineage == currentEdge(1));
            if isempty(parent)
                parent = 1;
            end
        end
        
        % Add a node
        lineage = lineage.addnode(parent, currentEdge(2));

    end
    
    trees{i} = lineage;
end

% -------------------------------------------------------------------------

function spotIdsToDelete = processTree(trees, spot)

spotIdsToDelete = [];
counter = 0;
for i = 1 : numel(trees)
    
    lineage = trees{i};
    
    branches = lineage.findbranchpoints();
    
    for j = 1 : numel(branches)
       
        % Get the children of the branch point
        children = lineage.getchildren(branches(j));
        
        % Check if those children have in turn children
        for k = 1 : numel(children)
            c = lineage.getchildren(children(k));
            if numel(c) == 0
                % This spot must be deleted
                counter = counter + 1;
                spotIdsToDelete(counter) = lineage.get(children(k));
            end
        end
    end
end
