function bsseProjectConvexHullAndGetArea(vImarisApplication)
% Project channel and calculates area of connected component.
%
% This function was written explicitly to calculate the area of a projected
% 3D mask obtained from masking the convex hull of a filament. It will
% project and calculated the area of all channels where there is only ONE
% connected component.
%
% Copyright Aaron Ponti, BSSE, ETH Zurich, 2014
%
% <CustomTools>
%  <Menu>
%   <Submenu name="BSSE">
%    <Submenu name="Dataset Functions">
%      <Item name="Calculare area of projected single connected component" icon="Python" tooltip="Calculate projected area.">
%       <Command>MATLABXT::bsseProjectConvexHullAndGetArea(%i)</Command>
%      </Item>
%    </Submenu>
%   </Submenu>
%  </Menu>
% </CustomTools>

% Instantiate an IceImarisConnector object
conn = IceImarisConnector(vImarisApplication);

% Is there something loaded?
aDataSet = conn.mImarisApplication.GetDataSet();
if isempty(aDataSet)
    return
end

% Get the number of channels
nChannels = aDataSet.GetSizeC();

% Get the number of planes
nPlanes = aDataSet.GetSizeZ();

% Get datatype
datatype = conn.getMatlabDatatype();

% Get the voxel size
[vX, vY] = conn.getVoxelSizes();
areaPixel = vX * vY;

% Allocate space to store the results
results = cell(nChannels , 5);

% Initialize waitbar
hWaitbar = waitbar(0, 'Processing...');

% Total number of oper
nOperations = nChannels * nPlanes;

% Process all channels
n = 0;
for c = 1 : nChannels

    % We know that this will be used on a very large data volume that
    % exceeds the size of the array that can be transferred, so we just
    % transfer plane by plane...
    
    % Skip the channel if disabled
    if conn.mImarisApplication.GetChannelVisibility(c - 1) == 0
        
        n = n + 1;       
        continue;

    end

    % Initialize mip
    mip = zeros([aDataSet.GetSizeX(), aDataSet.GetSizeY()], datatype);
    
    for z = 1 : nPlanes
      
        % Allocate space to retrieve the slice
        slice = zeros([aDataSet.GetSizeX(), aDataSet.GetSizeY()], datatype);
       
        switch datatype
        
            case 'uint8',
                
                arr = aDataSet.GetDataSliceBytes(z - 1, c - 1, 0);
                
            case 'uint16',
            
                arr = aDataSet.GetDataSliceShorts(z - 1, c - 1, 0);

            otherwise,
            
                error('Unsupported data type!');
            
        end

        % Type cast
        slice(:) = typecast(arr(:), datatype);
        
        % Update mip
        mip = max(cat(3, mip, slice), [], 3);
    
        % Update waitbar
        n = n + 1;
        waitbar(n / nOperations, hWaitbar);
    
    end
    
    % Only consider the masked channels
    if numel(unique(mip(:))) > 2
        
        results{c, 1} = char(aDataSet.GetChannelName(c  - 1));
        results{c, 2} = NaN;
        results{c, 3} = NaN;
        results{c, 4} = NaN;
        results{c, 5} = NaN;
        
    else
        
        % How many connected components?
        L = bwlabel(mip);
        
        % There should always be only one. If there are more, we just
        % pick the biggest and inform.
        if max(L(:)) > 1
            echo([char(aDataSet.GetChannelName(c  - 1)), ...
                ' has more than one connected components. Skipping.']);
            
            results{c, 1} = char(aDataSet.GetChannelName(c  - 1));
            results{c, 2} = NaN;
            results{c, 3} = NaN;
            results{c, 4} = NaN;
            results{c, 5} = NaN;

        else
        
            % measure properties
            props = regionprops(L == 1, ...
                {'Area', 'MajorAxisLength', 'MinorAxisLength', 'Perimeter'});
            
            results{c, 1} = char(aDataSet.GetChannelName(c  - 1));
            results{c, 2} = props.Area * areaPixel;
            results{c, 3} = props.Perimeter * vX;
            results{c, 4} = props.MajorAxisLength * vX;
            results{c, 5} = props.MinorAxisLength * vX;
            
        end
            
    end
    
end

% Close waitbar
close(hWaitbar);

% Save results
% =========================================================================

resultFile = fullfile(getTmpOutputDir(), 'filament_measurements.txt');
fid = fopen(resultFile, 'w');
if fid == -1
    uiwait(errordlg(...
        'Could not create the results file. Writing to console...', ...
        'Error', 'modal'));
    fid = 1;
end

% Write header
fprintf(fid, '%32s\t%32s\t%32s\t%32s\t%32s\n', 'Channel', ...
    'Area (units^2)', 'Perimeter (units)', 'Major axis length (units)', ...
    'Minor axis length (units)');

% Write results
for i = 1 : size(results, 1)
    fprintf(fid, '%32s\t%32.4f\t%32.4f\t%32.4f\t%32.4f\n', ...
        results{i, 1}, results{i, 2}, results{i, 3}, results{i, 4}, ...
        results{i, 5});
end

% Close file
if fid ~= 1
    fclose(fid);
end

% Display file
edit(resultFile);

% =========================================================================

function OUTPUTDIR = getTmpOutputDir

% Directory where to store the file
if isunix()
    OUTPUTDIR = '/tmp';
    return;
end

% In Windows we try the TMP folders
OUTPUTDIR=getenv('TMP');
if isempty(OUTPUTDIR)
    OUTPUTDIR=getenv('TEMP');
    if isempty(OUTPUTDIR)
        OUTPUTDIR=getenv('HOME');
        if isempty(OUTPUTDIR)
            OUTPUTDIR=pwd;
        end
    end
end

