function bsseProjection(imarisId)
% Project the dataset to 2D in a series of ways
%
% Slighlty extends Bitplane's XT3Dto2DProjection function.
%
%    <CustomTools>
%      <Menu>
%       <Submenu name="BSSE">
%        <Submenu name="Dataset Functions">
%         <Item name="Project to 2D" icon="Matlab">
%          <Command>MatlabXT::bsseProjection(%i)</Command>
%         </Item>
%        </Submenu>
%       </Submenu>
%      </Menu>
%    </CustomTools>
%

% Instantiate IceImarisConnector object
conn = IceImarisConnector(imarisId);

% Is there something loaded?
mDataSet = conn.mImarisApplication.GetDataSet();
if isempty(mDataSet)
    return
end

% Get the data set
dataset = conn.cloneDataset();

% Get sizes
[sizeX, sizeY, sizeZ, sizeC, sizeT] = conn.getSizes();

% Projection plane
planeNames = {'XY'; 'XZ'; 'YZ'};

% Direction of projection
answer = listdlg( ...
    'ListString', planeNames, ...
    'SelectionMode', 'single',...
    'ListSize', [200 50], ...
    'Name', '3D Projection', ...
    'PromptString', {'Please select  the projection plane:'});
if isempty(answer)
    return;
end
projectionPlane = answer;
projectionAxis = 4 - projectionPlane;

switch projectionPlane
    case 1
        sizeZ = 1;
    case 2
        sizeY = 1;
    case 3
        sizeX = 1;
end

% Type of projection
answer = questdlg( ...
    'Please select the projection mode:', ...
    'Input requested', ...
    'MIP', 'Mean', 'Sum', 'MIP');
if isempty(answer)
    return;
end
switch answer
    case 'MIP'
        projection = 1;
    case 'Mean'
        projection = 2;
    case 'Sum'
        projection = 3;
    otherwise
        error('Bad value for projection!');
end

% If we calculate a sum projection, we change the
% dataset type to float32.
if projection == 3
    dataset.SetType(Imaris.tType.eTypeFloat);
end


totalCount = sizeC * sizeT;
hWaitbar = waitbar(0, 'Projecting...');
count = 0;

for channel = 0: sizeC - 1

    for time = 0 : sizeT - 1
        
        stack = conn.getDataVolume(channel, time);
        
        if projection == 1
            slice = max(stack, [], projectionAxis);
        elseif projection == 2
            slice = mean(stack, projectionAxis);
        elseif projection == 3
            slice = sum(stack, projectionAxis);
        else
            error('Bad value for projection!');
        end
        
        if strcmp(dataset.GetType, 'eTypeUInt8')
            dataset.SetDataSubVolumeAs1DArrayBytes(slice(:), ...
                0, 0, 0, channel, time, sizeX, sizeY, sizeZ);
        elseif strcmp(dataset.GetType, 'eTypeUInt16')
            dataset.SetDataSubVolumeAs1DArrayShorts(slice(:), ...
                0, 0, 0, channel, time, sizeX, sizeY, sizeZ);
        elseif strcmp(dataset.GetType, 'eTypeFloat')
            dataset.SetDataSubVolumeAs1DArrayFloats(slice(:), ...
                0, 0, 0, channel, time, sizeX, sizeY, sizeZ);
        end
      
        % Update waitbar
        count = count + 1;
        waitbar(count / totalCount, hWaitbar);
    end
    
end

% Resize layers
switch projectionPlane
    case 1
        dataset.SetSizeZ(1);
    case 2
        dataset.SetSizeY(1);
    case 3
        dataset.SetSizeX(1);
end

% Set the dataset back
conn.mImarisApplication.SetDataSet(dataset);

% Close the waitbar
close(hWaitbar);
