function bsseSegmentCellsInTransmittedLight(vImarisApplication, instr)
% Runs a standard deviation filter with user-defined structuring-element
% radius on the dataset
%
% Copyright Aaron Ponti, BSSE, ETH Zurich, 2013
%
% <CustomTools>
%  <Menu>
%   <Submenu name="BSSE">
%    <Submenu name="Dataset Functions">
%     <Submenu name="Segment cells in transmitted light">
%      <Item name="Run" icon="Matlab" tooltip="Segments cells in transmitted light.">
%       <Command>MatlabXT::bsseSegmentCellsInTransmittedLight(%i)</Command>
%      </Item>
%      <Item name="Help" icon="Matlab" tooltip="XTension's instructions.">
%       <Command>MatlabXT::bsseSegmentCellsInTransmittedLight(%i,1)</Command>
%      </Item>
%    </Submenu>
%   </Submenu>
%  </Menu>
% </CustomTools>

if nargin == 1
    instr = 0;
end

if instr == 1
    showInstr();
    return
end

% Instantiate an IceImarisConnector object
conn = IceImarisConnector(vImarisApplication);

% Is there something loaded?
if isempty(conn.mImarisApplication.GetDataSet())
    return
end

% Get the sizes
[~, ~, nPlanes, nChannels, nTimepoints] = conn.getSizes();
if nPlanes > 1
    uiwait(errordlg('Sorry, this XTension can only be run on 2D datasets.', ...
        'Error', 'modal'));
    return;
end

channel = 0;
if nChannels > 1
    names = cell(1, nChannels);
    for i = 1 : nChannels
        names{i} = char( ...
            conn.mImarisApplication.GetDataSet().GetChannelName(i - 1));
    end
    [s, v] = listdlg('PromptString', 'Choose the channel to segment:',...
        'SelectionMode', 'single', 'ListString', names);
    if v == 0
        return
    end
    channel = s - 1;
end

% =========================================================================
%
% If the user selected a Spots object, we will update that one
% This will allow us to re-run just the tracking (which MUST have been
% run in the original Spots object).
%
% =========================================================================

selectedSpots = conn.getSurpassSelection();

% =========================================================================
%
% Now process all timepoints
%
% =========================================================================

% Allocate memory to store the results
pos = zeros(1000, 3);
timepoints = zeros(1000, 1);

if nTimepoints > 1
    hWaitbar = waitbar(0, 'Please wait...');
end

% Turn off warning for imfindcircles
warning('off', 'images:imfindcircles:warnForSmallRadius');

c = 1;
for timepoint = 0 : nTimepoints - 1
   
    % Get image
    img = nrm(double(conn.getDataVolume(channel, timepoint)), 1);

    % Pre-process
    img = imclose(img, strel('disk', 5)) - img;

    % Use the circular Hough transform to find the cells
    [centers, ~, ~] = imfindcircles(img, [5 15], ...
        'ObjectPolarity', 'bright', 'Method', 'TwoStage');

    % Store the positions and time indices
    nPeaks = size(centers, 1);
    if nPeaks > 0
        pos(c : c + nPeaks - 1, 1 : 3) = ...
            conn.mapPositionsVoxelsToUnits([centers(:, 2), centers(:, 1), ...
            zeros(nPeaks, 1)]);
        timepoints( c : c + nPeaks - 1, 1) = timepoint;
        
        c = c + nPeaks;
    end
    
    if nTimepoints > 1
        waitbar(timepoint / (nTimepoints - 1), hWaitbar);
    end

end

% Turn the warning for imfindcircles back on
warning('on', 'images:imfindcircles:warnForSmallRadius');

if nTimepoints > 1
    close(hWaitbar);
end

% Delete the empty rows
if c <= size(pos, 1)
    pos(c : end, :) = [];
    timepoints(c : end, :) = [];
end

% Get voxel size
[vx, ~, ~] = conn.getVoxelSizes();

% Set or update the spots
if isempty(selectedSpots)
    % Create a new spots object
    conn.createAndSetSpots(pos, timepoints, 5 .* vx .* ones(size(timepoints)), ...
        'Segmented cells', [rand(1, 3) 0]);
else
    % Update selected one
    selectedSpots.Set(pos,timepoints,5 .* vx .* ones(size(timepoints)));
    selectedSpots.SetName([char(selectedSpots.GetName()), ' (updated)']);
    conn.mImarisApplication.GetSurpassScene().AddChild(selectedSpots, -1);
end

% =========================================================================
% =========================================================================

    function showInstr()
       
        tmpfile = [ getTmpOutputDir(), filesep, 'help.html' ];
        fid = fopen(tmpfile, 'w');
        if fid == -1
            uiwait(errordlg('Could not create help file.', 'Error', 'modal'));
            return;
        end

        txt = [ '<html><head></head><body>', ...
            '<h1>Segment cells in transmitted light</h1>', ...
            '<p>This XTension basically runs a spot detection on a ', ...
            'non-fluorescent image, where the interesting signal is NOT ', ...
            'some Gaussian-like intensity maximum.</p>', ...
            '<p>In the case the user wants to track the segmented objects ', ...
            'in Imaris, it is currently not possible to re-run the ', ...
            'tracking part if it has not been run before, which is the ', ...
            'case fot the spots object generated by this XTension.</p>', ...
            '<p>To work around this limitation, the XTension can "update" ', ...
            'an existing spots object, which must have been created and ', ...
            'tracked in advance. The user simply selects the spots to be ', ...
            'updated and then runs the XTension. ', ...
            'Once the XTension has finished, the user can re-run the ', ...
            'tracking steps of the algorithm.</p>', ...
            '<p>Aaron Ponti, 2013/04/24</p>', ...
            '</body></html>'];

        fprintf(fid, '%s', txt);

        fclose(fid);

        web(['file://',tmpfile], '-browser', '-noaddressbox');
        
    end

    % =====================================================================

    function OUTPUTDIR = getTmpOutputDir

        % Directory where to store the file
        if isunix()
            OUTPUTDIR = '/tmp';
            return;
        end

        % In Windows we try the TMP folders
        OUTPUTDIR=getenv('TMP');
        if isempty(OUTPUTDIR)
            OUTPUTDIR=getenv('TEMP');
            if isempty(OUTPUTDIR)
                OUTPUTDIR=getenv('HOME');
                if isempty(OUTPUTDIR)
                    OUTPUTDIR=pwd;
                end
            end
        end

    end

end
