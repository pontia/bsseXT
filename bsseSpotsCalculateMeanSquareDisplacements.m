%
%
%  Calculates the mean square displacement for tracked spots
%
%  Copyright Aaron Ponti (FMI, BSSE) 2008/09/29
%
%
%  Installation:
%
%  - Copy this file into the XTensions folder in the Imaris installation directory
%  - You will find this function in the Image Processing menu
%
%    <CustomTools>
%      <Menu>
%       <Submenu name="BSSE">
%        <Submenu name="Spots Functions">
%         <Item name="Calculate mean square displacements" icon="Matlab">
%           <Command>MatlabXT::bsseSpotsCalculateMeanSquareDisplacements(%i)</Command>
%         </Item>
%        </Submenu>
%       </Submenu>
%      </Menu>
%      <SurpassTab>
%        <SurpassComponent name="bpSpots">
%          <Item name="BSSE :: Calculate mean square displacements">
%            <Command>MatlabXT::bsseSpotsCalculateMeanSquareDisplacements(%i)</Command>
%          </Item>
%        </SurpassComponent>
%      </SurpassTab>
%    </CustomTools>
%
%

function bsseSpotsCalculateMeanSquareDisplacements(aImarisApplicationID)

% -------------------------------------------------------------------------
%
% INSTANTIATE ICEIMARISCONNECTOR
%
% -------------------------------------------------------------------------

conn = IceImarisConnector(aImarisApplicationID);

% -------------------------------------------------------------------------
%
% Check for the existing objects in the surpass scene
%
% If this function is started from MATLAB instead than from Imaris, first
% start the Imaris server and Imaris).
%
% -------------------------------------------------------------------------

vSurpassScene = conn.mImarisApplication.GetSurpassScene();
if isempty(vSurpassScene)
    msgbox('No surpass scene found!');
    return;
end

% The spots must be selected
vSpots = conn.getSurpassSelection('Spots');
if isempty(vSpots)
    uiwait(errordlg('Please select a ''Spots'' object!', 'Error', 'modal'));
    return
end

% -------------------------------------------------------------------------
%
% Ask the user for the time interval and the number of time points used to
% estimate the diffusion coefficient
%
% -------------------------------------------------------------------------

answer = inputdlg({...
    'Time interval between frames [s]:', ...
    'Timepoints to be used to calculate the mean square displacements (''0'' for all):' , ...
    'Timepoints to be used to estimate the diffusion coefficient (''0'' for all):'}, ...
    'User input requested',1, {'1', '0', '0'});
if isempty(answer)
    return
end
timeInterval = abs(str2double(answer{1}));
if isnan(timeInterval) || timeInterval <= 0
    uiwait(errordlg('Bad value for the time interval.', 'Error', 'modal'));
    return
end
numTimepoints = abs(str2double(answer{2}));
if isnan(numTimepoints) || numTimepoints < 0 || numTimepoints == 1
    uiwait(errordlg('Bad value for the number of time points to be used to calculate the mean square displacements.', 'Error', 'modal'));
    return
end
if numTimepoints == 0
    numTimepoints = Inf;
end
numLinModelTimepoints = abs(str2double(answer{3}));
if isnan(numLinModelTimepoints) || numLinModelTimepoints < 0 || numLinModelTimepoints == 1
    uiwait(errordlg('Bad value for the number of time points to be used to estimate the diffusion coefficient.', 'Error', 'modal'));
    return
end
if numLinModelTimepoints > numTimepoints
    uiwait(warndlg(['The number of timepoints used to calculate ', ...
        'the diffusion coefficient cannot be larger than the number of ', ...
        'timepoints used to calculate the mean square displacements.', ...
        'Setting them both to ', num2str(numTimepoints), '.'], ... 
        'Warning', 'modal'));
    numLinModelTimepoints = numTimepoints;
end
if numLinModelTimepoints == 0
    numLinModelTimepoints = numTimepoints;
end

% -------------------------------------------------------------------------
%
% Ask the user to specify where to save the results
%
% -------------------------------------------------------------------------

imarisFileName = char(conn.mImarisApplication.GetCurrentFileName());
[filePath, fileName, ~] = fileparts(imarisFileName);
if isempty(fileName)
    defaultName = 'MSDs.tsv';
else
    defaultName = fullfile(filePath, [fileName, '_MSDs.tsv']);
end
[fName, dirName] = uiputfile('*.tsv',...
    'Save results as...', defaultName);
if isequal(fName, 0)
    return;
end
resultOutputFile = fullfile(dirName, fName);

% -------------------------------------------------------------------------
%
% Extract all tracks and store them for later usage
%
% -------------------------------------------------------------------------

% Get the IDs of the tracks
ids     = vSpots.GetTrackIds;
uids    = unique(ids);
nTracks = numel(uids);

% Get all spot positions and the track edges
positions  = vSpots.GetPositionsXYZ;
trackEdges = vSpots.GetTrackEdges;

% We keep track of the length of the longest track (we will need it later)
longestTrack = 0;

% Now extract one track after the other and store them into a cell array
allTracks = cell(1, nTracks);
for i = 1 : nTracks
    
    % Get the positions and edges for current track (id)
    edges = (trackEdges(ids == uids(i), :) + 1)';
    edges = unique(edges(:));

    % Extract and store the track
    allTracks{i} = positions(edges, :);

    % Check if it is the longest track
    len = numel(edges); 
    if len > longestTrack
        longestTrack = len;
    end
    
end

% Generate the time vector
timeVector = (timeInterval .* (1 : longestTrack - 1))';

% -------------------------------------------------------------------------
%
% Calculate and store the mean square displacements
%
% Bannai et al., Nature Protocols, Vol. 1 No. 6, 2628-2634 (2006)
% Anderson et al., Journal of Cell Science, 101, 415-425 (1992)
%
% -------------------------------------------------------------------------

allMSDs = cell(1, nTracks);
D       = zeros(1, nTracks);
R2      = zeros(1, nTracks);

% Initializing waitbar
hWaitbar = waitbar(0, 'Please wait');

for track = 1 : nTracks

    % Calculate the square displacements over the requested number of
    % timepoints
    N = min(numTimepoints, size(allTracks{track}, 1));
    MSD = zeros(N - 1, 1);
    for n = 1 : (N - 1)

        last = N - n;
        
        tmp = 0;
        for i = 1 : last
            
            tmp = tmp + ...
                (allTracks{track}(i + n, 1) - ...
                allTracks{track}(i, 1)) ^ 2 + ...
                (allTracks{track}(i + n, 2) - ...
                allTracks{track}(i, 2)) ^ 2 + ...
                (allTracks{track}(i + n, 3) - ...
                allTracks{track}(i, 3)) ^ 2;
                
        end
        
        MSD(n) = 1 / last * tmp;
        
    end

    allMSDs{track} = MSD;
    
    % Calculate the diffusion coefficient of current MSD
    [d, r2] = calcDiffusion(timeVector, MSD, numLinModelTimepoints);
    D(track)  = d;
    R2(track) = r2;
    
    % Updating waitbar
    waitbar(track / nTracks, hWaitbar, [...
        'Processed track ', num2str(track), ...
        ' of ', num2str(nTracks), '...']);
    
end

% Closing waitbar
close(hWaitbar);

% -------------------------------------------------------------------------
%
% Export the mean square displacements to file
%
% -------------------------------------------------------------------------

% Prepare the report
fid = fopen(resultOutputFile, 'w');
if fid == -1
    disp('Could not create report file. Writing to console.');
    fid = 1;
end

% Write the parameters
if numTimepoints == Inf
    sTimepoints = 'all';
else
    sTimepoints = num2str(numTimepoints);
end
fprintf(fid, 'Number of timepoints used to calculate the mean square displacements: \t%s\n', sTimepoints);
if numLinModelTimepoints == Inf
    sLinModelTimepoints = 'all';
else
    sLinModelTimepoints = num2str(numLinModelTimepoints);
end
fprintf(fid, 'Number of timepoints used to calculate the diffusion coefficients: \t%s\n\n', sLinModelTimepoints);

% Write the time vector
fprintf(fid, '%12s\t%12s\t%12s\t', 'D[u^2/s]', 'R2', 'Track ID');
if isinf(numTimepoints)
    lastPos = longestTrack - 1;
else
    lastPos = numTimepoints - 1;
end
for i = 1 : lastPos
    fprintf(fid, '%8.4f\t', timeVector(i));
end
fprintf(fid, '\n');

% Print the MSDs
%    Each row one track, first three entries are D, R2 and the track unique id
for track = 1 : nTracks
    fprintf(fid, '%12.8f\t', D(track));
    fprintf(fid, '%12.8f\t', R2(track));
    fprintf(fid, '%12d\t', uids(track));
    for j = 1 : size(allMSDs{track}, 1)
        fprintf(fid, '%8.4f\t', allMSDs{track}(j));
    end
    fprintf(fid, '\n');
end

% Close the file
if fid ~= 1
    fclose(fid);
end

% -------------------------------------------------------------------------
%
% Functions
%
% -------------------------------------------------------------------------

function [D, R2] = calcDiffusion(t, msd, n)

n = min([numel(t) numel(msd) n]);
[b, R2] = lsfit(t(1 : n), msd(1 : n));
D = b(2); % * 1e-8; 
            % Multiplication by 1e-8 converts um^2 to cm^2 (cm^2/s is the
            % standard unit for the diffusion coefficient)

% =========================================================================

function [b, R2] = lsfit(t, m)

t     = [ones(numel(t), 1) t];
b     = t \ m;
mhat  = b(1) + b(2) .* t(:, 2);
mmean = mean(m);
ESS   = sum((mhat - mmean) .^ 2);
TSS   = sum((m    - mmean) .^ 2);
R2    = ESS / TSS;
% clf;
% plot(t(:, 2), m); hold on
% plot(t(:, 2), mhat, 'r-')
% title(['R2 = ', num2str(R2)]);
% pause
