function bsseSpotsClassifyByDistance(aImarisApplicationID)
% Classifies the Spots by distance from a selected Surface.
%
% The distance limits are specified by the user via input dialogs. The
% classified Spots are copied to new Spots objects.
%
%    <CustomTools> 
%      <Menu> 
%       <Submenu name="BSSE">
%        <Submenu name="Spots Functions">
%         <Item name="Classify spots by distance from surface" icon="Matlab"> 
%          <Command>MatlabXT::bsseSpotsClassifyByDistance(%i)</Command> 
%         </Item> 
%        </Submenu>
%       </Submenu>
%      </Menu> 
%      <SurpassTab> 
%        <SurpassComponent name="bpSpots"> 
%          <Item name="Classify spots by distance from surface" icon="Matlab"> 
%            <Command>MatlabXT::bsseSpotsClassifyByDistance(%i)</Command> 
%          </Item> 
%        </SurpassComponent> 
%      </SurpassTab> 
%    </CustomTools> 
%
% Copyright (c) 2015, Aaron Ponti

% Set up connection between Imaris and MATLAB
conn = IceImarisConnector(aImarisApplicationID);

% Get the currently selected Spots object in the surpass scene
vSpots = conn.getSurpassSelection('Spots');
if isempty(vSpots)
    errordlg('Please select a Spots object!');
    return
end

% Get the Surfaces objects
surfaces = conn.getAllSurpassChildren(0, 'Surfaces');
nSurfaces = numel(surfaces);
if nSurfaces == 0
        errordlg('There are no Surfaces in the scene!');
    return
end
if nSurfaces > 1
    % Ask the user to pick one surface
    vSurface = askUserToPickSurface(surfaces);
    if isempty(vSurface)
        return
    end
else
    vSurface = surfaces{1};
end

% Get the center of mass of the Surfaces object
surfaceVertexCoords = getAndSubsampleSurfaceVertes(vSurface);

% Ask the user to specify the distance limits to use
distanceLimits = askUserToSetDistanceLimits();
if isempty(distanceLimits)
    return
end

% Get the spot coordinates, radii and time indices
spotValues = vSpots.Get();
coords = spotValues.mPositionsXYZ;
radii = spotValues.mRadii;
timeIndices = spotValues.mIndicesT;

% Calculate all spot distances from the measurement point
D = createDistanceMatrix(double(coords), surfaceVertexCoords);
D = min(D, [], 2);

% Now classify the spots by distance and create new Spots objects
% containing the spots that fall in current distance bin
for i = 2 : numel(distanceLimits)
    indx = find(D >= distanceLimits(i - 1) & D < distanceLimits(i));
    if ~isempty(indx)
        name = ['Spots ', num2str(distanceLimits(i - 1)), ...
            ' <= D < ', num2str(distanceLimits(i)), ' [', ...
            num2str(numel(indx)), ']'];
        conn.createAndSetSpots(coords(indx, :), timeIndices(indx), ...
            radii(indx), name, [rand(1, 3) 0]);
    end
end

% ==== Helper functions ===================================================

% Ask the user to pick a surface from a list
function vSurface = askUserToPickSurface(surfaces)
nSurfaces = numel(surfaces);
surfaceNames = cell(1, nSurfaces);
for i = 1 : nSurfaces
    surfaceNames{i} = char(surfaces{i}.GetName());
end
[s, v] = listdlg(...
    'Name', 'Question', ...
    'PromptString', ...
    'Choose surface from which the distances will be calculated', ...
    'SelectionMode', 'single', ...
    'ListSize', [400 300], ...
    'ListString', surfaceNames);
if v == 0
    vSurface = [];
    return;
end
vSurface = surfaces{s};

% Ask the user to set the distance limits a surface from a list
% (we provide reasonable defaults)
function distanceLimits = askUserToSetDistanceLimits()
answer = inputdlg({'Please set distance limits:'}, 'Input', ...
    1, {'10, 20, 30, 40, 50'});
if isempty(answer)
    return;
end
distanceLimits = str2num(answer{1}); %#ok<ST2NM>
if isempty(distanceLimits)
    errordlg('Invalid distance limits!');
    distanceLimits = [];
    return;
end
distanceLimits = [0 distanceLimits Inf];

% -------------------------------------------------------------------------

function surfaceVertexCoords = getAndSubsampleSurfaceVertes(vSurface)

% Get all vertices for the selected surface
surfaceVertexCoords = [];
for i = 0 : vSurface.GetNumberOfSurfaces() - 1
    surfaceVertexCoords = cat(1, surfaceVertexCoords, vSurface.GetVertices(i));
end
vNumberOfVertices = size(surfaceVertexCoords, 1);

nEntries = fix(log10(vNumberOfVertices)) + 2;

% Prepare the listdlg
numberOfTriangleList = zeros(1, nEntries);
numberOfTriangleList(1) = 1;
numberOfTriangleList(nEntries) = vNumberOfVertices;
str = cell(1, nEntries);
str{1} = 'Use center of mass';
for i = 2 : nEntries - 1
    numberOfTriangleList(i) = 10 ^ (i - 1);
    str{i} = ['Use ', num2str(numberOfTriangleList(i)), ' triangles'];
end
str{nEntries} = ['Use ', num2str(vNumberOfVertices), ' (all) triangles'];
[s, v] = listdlg('PromptString', ...
    'Sub-sample surface:', ...
    'SelectionMode', 'single', ...
    'ListString', str);
if v == 0
    return
end
MAX_SURFACE_VERTICES = numberOfTriangleList(s);

% Sub-sample the surface(s)
if MAX_SURFACE_VERTICES == 1
    
    % Get the centers of mass
    allSurfaceVertexCoords = [];
    for i = 1 : vSurface.GetNumberOfSurfaces
        allSurfaceVertexCoords = cat(1, ...
            allSurfaceVertexCoords, vSurface.GetCenterOfMass(i - 1));
    end
    surfaceVertexCoords = double(mean(allSurfaceVertexCoords, 1));
    
else
    
    % Limit the number of surface vertices used to MAX_SURFACE_VERTICES
    if vNumberOfVertices > MAX_SURFACE_VERTICES
        vIndices = round(linspace(1, vNumberOfVertices, MAX_SURFACE_VERTICES));
        surfaceVertexCoords = surfaceVertexCoords(vIndices, :);
    end
    
    % Make sure the coordinates are of type 'double', or createDistanceMatrix
    % will fail
    surfaceVertexCoords = double(surfaceVertexCoords);

end

