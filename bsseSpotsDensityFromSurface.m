function bsseSpotsDensityFromSurface( aImarisApplicationID, showInfo )
%  Calculates spot density for different spot objects at different
%  distances from a surface object, as created by bpSpotsCloseToSurface.m
%
%  Installation:
%
%  - Copy this file into the XTensions folder in the Imaris installation directory
%  - You will find this function in the Image Processing menu
%
%    <CustomTools>
%      <Menu>
%       <Submenu name="BSSE">
%        <Submenu name="Spots Functions">
%         <Submenu name="Distance density from surface">
%          <Item name="Calculate and plot spot distance density from surface" icon="Matlab">
%            <Command>MatlabXT::bsseSpotsDensityFromSurface(%i, 0)</Command>
%          </Item>
%          <Item name="Instructions" icon="Matlab">
%            <Command>MatlabXT::bsseSpotsDensityFromSurface(%i, 1)</Command>
%          </Item>
%         </Submenu>
%        </Submenu>
%       </Submenu>
%      </Menu>
%      <SurpassTab>
%        <SurpassComponent name="bpSpots">
%          <Item name="BSSE :: Distance density from surface" icon="Matlab" tooltip="Calculate and plot spot distance density from surface.">
%            <Command>MatlabXT::bsseSpotsDensityFromSurface(%i, 0)</Command>
%          </Item>
%        </SurpassComponent>
%      </SurpassTab>
%    </CustomTools>
%
%  Description:
%
%  Copyright Aaron Ponti (FMI, BSSE) 2009/09/30

% Instantiate the IceImarisConnector object
conn = IceImarisConnector(aImarisApplicationID);

if nargin == 1
    showInfo = 0;
elseif nargin == 2
    if showInfo ~= 0 && showInfo ~= 1
        error( 'Wrong value for showInfo (must be either 0 or 1).' );
    end
else
    error( 'Wrong number of input arguments.' );
end

if showInfo == 1
    show;
    return
end

% Check for existance of the Surpass Scene
if isempty( conn.mImarisApplication.GetSurpassScene() )
    msgbox( 'Please create the relevant objects in the surpass scene!' )
    return
end

% =========================================================================
%
% Get spot and surface objects
%
% =========================================================================

% Find all spots and surface objects at the root level
spots = conn.getAllSurpassChildren(0, 'Spots');
nSpots = numel(spots);

surfaces = conn.getAllSurpassChildren(0, 'Surfaces');
nSurfaces = numel(surfaces);

if any( [ nSpots == 0, nSurfaces == 0 ] )
    uiwait( errordlg( ...
        'At least one surface and one spot object are required.', ...
        'Error', 'modal' ) );
    return;
end

if nSurfaces > 1
    surfaceNames = cell( 1, nSurfaces );
    for i = 1 : nSurfaces
        surfaceNames{ i } = char(surfaces{ i }.GetName());
    end
    
    [ s, v ] = listdlg( ...
        'Name', 'Question', ...
        'PromptString', ...        
        'Choose surface from which the distances will be calculated', ...
        'SelectionMode', 'single', ...
        'ListSize', [ 400 300 ], ...
        'ListString', surfaceNames );
    if v == 0
        return;
    end
    vSurface = surfaces{ s };
else
    vSurface = surfaces{ 1 };
end

% =========================================================================
%
% Get surface vertices
%
% =========================================================================

% Get all vertices for the selected surface
surfaceVertexCoords = [ ];
for i = 0 : vSurface.GetNumberOfSurfaces( ) - 1
    surfaceVertexCoords = [ surfaceVertexCoords; vSurface.GetVertices( i ) ];
end
vNumberOfVertices = size( surfaceVertexCoords, 1 );

nEntries = fix( log10( vNumberOfVertices ) ) + 2;

% Prepare the listdlg
numberOfTriangleList = zeros( 1, nEntries );
numberOfTriangleList( 1 ) = 1;
numberOfTriangleList( nEntries ) = vNumberOfVertices;
str = cell( 1, nEntries );
str{ 1 } = 'Use center of mass';
for i = 2 : nEntries - 1
    numberOfTriangleList( i ) = 10 ^ ( i - 1 );
    str{ i } = [ 'Use ', num2str( numberOfTriangleList( i ) ), ' triangles' ];
end
str{ nEntries } = [ 'Use ', num2str( vNumberOfVertices ), ' (all) triangles' ];
[ s, v ] = listdlg( 'PromptString', ...
    'Sub-sample surface:', ...
    'SelectionMode', 'single', ...
    'ListString', str );
if v == 0
    return
end
MAX_SURFACE_VERTICES = numberOfTriangleList( s );

% Sub-sample the surface(s)
if MAX_SURFACE_VERTICES == 1
    
    % Get the centers of mass
    allSurfaceVertexCoords = [ ];
    for i = 1 : vSurface.GetNumberOfSurfaces
        allSurfaceVertexCoords = ...
            [ allSurfaceVertexCoords; vSurface.GetCenterOfMass( i - 1 ) ];
    end
    surfaceVertexCoords = double( mean( allSurfaceVertexCoords, 1 ) );
    
else
    
    % Limit the number of surface vertices used to MAX_SURFACE_VERTICES
    if vNumberOfVertices > MAX_SURFACE_VERTICES
        vIndices = round( linspace( 1, vNumberOfVertices, MAX_SURFACE_VERTICES ) );
        surfaceVertexCoords = surfaceVertexCoords( vIndices, : );
    end
    
    % Make sure the coordinates are of type 'double', or createDistanceMatrix
    % will fail
    surfaceVertexCoords = double( surfaceVertexCoords );

end

% =========================================================================
%
% Ask the user to choose the solution approach
%
% =========================================================================

choice = questdlg( ...
    { 'Please choose the dataset subsampling factor to be used to ', ...
    'calculate the distance map.', ...
    'The choice will have a strong impact on the calculation time!' }, ...
    'Question ', ...
    'Full (VERY SLOW!)', '2x sub-sampling', '4x sub-sampling', '2x sub-sampling' );
if isempty( choice )
    return
end
switch choice,
    case 'Full (VERY SLOW!)',
        INTERPOLATION_STEP = 1;
    case '2x sub-sampling',
        INTERPOLATION_STEP = 2;
    case '4x sub-sampling',
        INTERPOLATION_STEP = 4;
    otherwise,
        error( 'Bad value for ''precision''.' );
end

% =========================================================================
%
% Get (and pool) spot coordinates
%
% =========================================================================

if nSpots > 1
    spotsNames = cell( 1, nSpots );
    for i = 1 : nSpots
        spotsNames{ i } = char(spots{ i }.GetName());
    end
    
    [ s, v ] = listdlg( ...
        'Name', 'Question', ...
        'PromptString', ...
        [ 'Choose spot objects to be considered in the calculation ', ...
        '(multiple choice possible)'], ...
        'SelectionMode', 'multiple', ...
        'ListSize', [ 400 300 ], ...
        'ListString', spotsNames );
    if v == 0
        return;
    end
    vSpots = spots( s );
else
    vSpots = spots( 1 );
end

% Get the spot size (for later)
spotDiameter = median( vSpots{ 1 }.GetRadii( ) );

% Get and pool all coordinates
spotCoords = [ ];
for i = 1 : numel( vSpots )

    spotCoords = cat( 1, spotCoords, vSpots{ i }.GetPositionsXYZ );
    
end

% Get rid of repetitions
spotCoords = unique( spotCoords, 'rows' );

% Make sure that the datatype is double
spotCoords = double( spotCoords );

% =========================================================================
%
% Ask the user to define the ranges
%
% =========================================================================

choice = inputdlg( ...
    { 'Please set the distances ( d1, d2, d3, ... ) to consider' }, ...
    'Question', 1, { '5, 10, 20, 50, 100, 200' } );
if isempty( choice )
    return
end
thresholds = sort( abs( str2num( choice{ 1 } ) ) );

% =========================================================================
%
% Prepare a distance matrix
%
% =========================================================================

% Get the voxel size
[voxelX, voxelY, voxelZ] = conn.getVoxelSizes();

% Remember to correct for the interpolation when calculating densities!!!
voxelVolume = voxelX .* voxelY .* voxelZ; 

% Get the sizes
[sX, sY, sZ] = conn.getSizes();

% Get the extends
[minX, ~, minY, ~, minZ, ~] = conn.getExtends();

stepsX  = 0 : INTERPOLATION_STEP : sX - 1;
nStepsX = numel( stepsX );
stepsY  = 0 : INTERPOLATION_STEP : sY - 1;
nStepsY = numel( stepsY );
stepsZ  = 0 : INTERPOLATION_STEP : sZ - 1;
nStepsZ = numel( stepsZ );

[ x, y ] = ndgrid( stepsX, stepsY );

planeCoords = [ minX + voxelX .* x( : ), minY + voxelY .* y( : ) ];

% Allocate space for the distance matrix
D = zeros( [ nStepsX, nStepsY, nStepsZ ], 'double' );

hWaitbar = waitbar( 0, 'Calculating distance matrix' );
nOperations = nStepsZ * nStepsX;
currentOp = 0;
indexCounterI = 0;
for i = 0 : INTERPOLATION_STEP : sZ - 1
    
    indexCounterI = indexCounterI + 1;
    
    % Calculate the z coordinates for current plane
    planeCoordsZ = ones( nStepsX, 1 ) .* i * voxelZ + minZ;
    
    % To prevent memory problems, we calculate distances for one row
    % at a time
    indexCounterJ = 0;
    for j = 0 : INTERPOLATION_STEP : sY - 1
        
        indexStart = indexCounterJ * nStepsX + 1;
        indexEnd   = indexStart + nStepsX - 1;
        indexCounterJ = indexCounterJ + 1;
        
        % Calculate and store the distances
        D( :, indexCounterJ, indexCounterI ) = ...
            min( createDistanceMatrix( ...
            [ planeCoords( indexStart : indexEnd, : ), planeCoordsZ ], ...
            surfaceVertexCoords ), [ ], 2 );
        
        currentOp = currentOp + 1;
        if mod( currentOp, 10 ) == 0
            waitbar( currentOp / nOperations, hWaitbar );
            drawnow;
        end
        
    end
    
end

close( hWaitbar );

% =========================================================================
%
% Calculate all spot distances from the surface
%
% =========================================================================

nSpotCoords = size( spotCoords, 1 );
spotDistances = zeros( nSpotCoords, 1 );
for i = 1 : nSpotCoords
    
    spotDistances( i ) = min( createDistanceMatrix( ...
        spotCoords( i, : ), surfaceVertexCoords ) );

end

% =========================================================================
%
% Count the spots that fall into the distance ranges
%
% =========================================================================

if thresholds( 1 ) > 0
    thresholds = [ 0 thresholds ];
end
if thresholds( end ) ~= Inf
    thresholds = [ thresholds Inf ];
end
nBins = numel( thresholds ) - 1;

spotNumbers   = zeros( 1, nBins );
spotDensities = zeros( 1, nBins );
rangeVolumes  = zeros( 1, nBins );
spotsInRange  = cell( 1, nBins );

for i = 1 : nBins

    spotsInRange{ i } = find( spotDistances >= thresholds( i ) & ...
        spotDistances < thresholds( i + 1 ) );
    spotNumbers( i ) = numel( spotsInRange{ i } );
    
    voxelsInRange  =  find( D >= thresholds( i ) & D < thresholds( i + 1 ) );
    rangeVolumes( i ) = numel( voxelsInRange );

    % Correct the voxel volume for the interpolation step
    spotDensities( i ) = spotNumbers( i ) / ...
        ( voxelVolume * INTERPOLATION_STEP ^ 3 * rangeVolumes( i ) );

end

% =========================================================================
%
% Create spots for quality control
%
% =========================================================================

% Create a new group and add it to the Surpass Scene
newGroup = conn.mImarisApplication.GetFactory().CreateDataContainer();
newGroup.SetName([ 'Spots (', num2str( sum( spotNumbers ) ), ...
    ') ordered by distance' ]);
conn.mImarisApplication.GetSurpassScene().AddChild(newGroup, -1);

% Create and add the spots to the container
for i = 1 : nBins
   
    currentNSpots = numel( spotsInRange{ i } );
    if currentNSpots == 0
        continue;
    end
    
    conn.createAndSetSpots(spotCoords( spotsInRange{ i }, : ), ...
        zeros( 1, currentNSpots ), ...
        spotDiameter .* ones( 1, currentNSpots ), ...
        [ num2str( thresholds( i ) ), ...
        ' =< distance < ', num2str( thresholds( i + 1 ) ), ...
        ' (', num2str( currentNSpots ), ')' ], ...
        [rand( 1 ), rand( 1 ), rand( 1 ), 0], ...
        newGroup);

end



% =========================================================================
%
% Report
%
% =========================================================================

% Directory where to store the file
OUTPUTDIR=getenv('TMP');
if isempty(OUTPUTDIR)
    OUTPUTDIR=getenv('TEMP');
    if isempty(OUTPUTDIR)
        OUTPUTDIR=getenv('HOME');
        if isempty(OUTPUTDIR)
            OUTPUTDIR=pwd;
        end
    end
end

% Prepare report
tmpfile = [ OUTPUTDIR, filesep, 'Spot_Densities.txt' ];

fid = fopen( tmpfile, 'w' );
if fid == -1
    disp( 'Could not create report file. Writing to console.' );
    fid = 1;
end

fprintf( fid, '%24s\t', '' ); % Blank
for i = 1 : nBins
    fprintf( fid, '%16s\t', [ num2str( thresholds( i ) ), ...
        ' =< d < ', num2str( thresholds( i + 1 ) ) ] ); % Bin
end
fprintf( fid, '\n' ); % New-line

% Spot number per bin
fprintf( fid, '%24s\t', 'Spot number' ); % Number
for i = 1 : nBins
    fprintf( fid, '%16d\t', spotNumbers( i ) );
end
fprintf( fid, '\n' ); % New-line

% Spot density per bin
fprintf( fid, '%24s\t', 'Spot density (1/units^3)' ); % Density
for i = 1 : nBins
        fprintf( fid, '%16.12f\t', spotDensities( i ) );
end
fprintf( fid, '\n' ); % New-line

% Range volumes per bin
fprintf( fid, '%24s\t', 'Range volume (units^3)' ); % Raneg volume
for i = 1 : nBins
        fprintf( fid, '%16.4f\t', rangeVolumes( i ) * voxelVolume * INTERPOLATION_STEP ^ 3  );
end
fprintf( fid, '\n' ); % New-line
fprintf( fid, ...
    '\nVolume of one voxel = %14.12f units^3\n', ...
    voxelVolume );

if fid ~= 1
    fclose( fid );
    edit( tmpfile );
end

% =========================================================================

function show

msg = { ...
    'This XTension calculates the density of spots at user-defined', ...
    'distance intervals from a surface. Since the computation can be', ...
    'extremely computation intensive, various levels of approximations', ...
    'of the solution are possible.', ...
    '',...
    'The level of accuracy can be influenced by varying the number of ', ...
    'surface triangles and dataset voxels to be used to calculate the ', ...
    'spot density at increasing distance from the surface.', ...
    '',...
    'The ''exact'' solution is obtained when all triangles and all voxels ', ...
    'are used to calculate the distance transform for range volume ', ...
    'estimation. This can take VERY long to compute!', ...
    '', ...
    'The choices for the number of triangles are 1, 10, 100, 1000, ..., ', ...
    'all triangles. The volume can be optionally downsampled 2x or 4x.', ...
    '',...
    'The lowest number of triangles to be used is 1: in this case, ', ...
    'though, the distances will be calculated from the CENTER OF MASS of ', ...
    'the surface, and not from one arbitrary tiangle.', ...
    '', ...
    'You can pool as many spot objects you want. Should they contain', ...
    'overlapping positions, each of them will be counted only once.', ...
    '', ...
    'Distance ranges are specified by passing a comma-separated list of', ...
    'upper bounds, e.g. 5, 10, 20, 50, 100, 200.', ...
    '', ...
    'Please mind that distances are considered from the surface triangles ', ...
    '(unless the center of mass is chosen): this means that spot INSIDE ', ...
    'a surface will have POSITIVE distance!', ...
    '', ...
    '' };

uiwait( msgbox( msg , 'Help' ) );

