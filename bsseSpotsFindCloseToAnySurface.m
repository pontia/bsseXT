function bsseSpotsFindCloseToAnySurface(aImarisApplicationID)
%  Finds and extracts all spots that are within a certain distance from any surface
%
%  Installation:
%
%  - Copy this file into the XTensions folder in the Imaris installation directory
%  - You will find this function in the Image Processing menu
%
%    <CustomTools>
%      <Menu>
%       <Submenu name="BSSE">
%        <Submenu name="Spots Functions">
%         <Item name="Find spots close to any surface" icon="Matlab">
%           <Command>MatlabXT::bsseSpotsFindCloseToAnySurface(%i)</Command>
%         </Item>
%        </Submenu>
%       </Submenu>
%      </Menu>
%      <SurpassTab>
%        <SurpassComponent name="bpSpots">
%          <Item name="BSSE :: Find spots close to any surface" icon="Matlab" tooltip="Sort spots by number.">
%            <Command>MatlabXT::bsseSpotsFindCloseToAnySurface(%i)</Command>
%          </Item>
%        </SurpassComponent>
%      </SurpassTab>
%    </CustomTools>
%
%  Copyright Aaron Ponti (FMI, BSSE) 2012
%

% Create an IceImarisConnector object
conn = IceImarisConnector( aImarisApplicationID );

% Check that a surpass scene exists
if isempty( conn.mImarisApplication.GetSurpassScene() )
    uiwait( errordlg( 'The surpass scene is empty!', 'Error', 'modal' ) );
    return
end
    
% The selected object must be a spot
vSpot = conn.getSurpassSelection('Spots');
if isempty(vSpot)
    uiwait( errordlg( 'Please select a ''Spots'' object!', 'Error', 'modal' ) );
    return
end

% Get all surfaces and ask the user to pick those to be processed
surfaces = conn.getAllSurpassChildren(0, 'Surfaces');
nSurfaces = numel(surfaces);
if nSurfaces == 0
    uiwait(errordlg('No surfaces found!', 'Error', 'modal'));
    return
end

if nSurfaces > 1
    names = cell(1, nSurfaces);
    for i = 1 : nSurfaces
        names{i} = char(surfaces{i}.GetName());
    end
    [ s, v ] = listdlg( 'PromptString', ...
        'Select the surface object to process...', ...
        'ListSize', [ 300 400 ], ...
        'SelectionMode', 'single', 'ListString', names );
    if v == 0
        return
    end
    vSurface = surfaces{s};
else
    vSurface = surfaces{1};
end

% Ask the user to specify a distance threshold
answer= inputdlg( ...
    {'Please specify a distance threshold (in dataset units):' }, ...
    'Question', 1, { '5' } );
if isempty( answer )
    return
end
threshold = str2double( answer );
if isnan( threshold )
    uiwait( errordlg( 'Bad value for the distance threshold', 'modal' ) );
    return
end

% Get the spot coordinates
meas = vSpot.Get();
coords = meas.mPositionsXYZ;
timepoints = meas.mIndicesT;
radii = meas.mRadii; 

% Coords must be of class double for createSparseDistancematrix
coords = double( coords );

% Initialize a vector to keep track of the spots that are close to at least
% one surface
indices = zeros( size( coords, 1 ), 1 );

% Number of child surfaces
nChildSurfaces = vSurface.GetNumberOfSurfaces( );

% Open a waitbar
mustStop = 0;

hWaitbar = waitbar( 0, 'Processing surfaces...', ...
    'CreateCancelBtn', { @cancelProcessing } );

% If a surface contains more than one subsurface, we consider them all
for i = 1 : nChildSurfaces
    
    % Should we abort
    if mustStop == 1
        uiwait( warndlg( 'Interrupted by user', 'Info' ), 'modal' );
        return
    end

    % Get the coordinates of the vertices
    vertices = double( vSurface.GetVertices( i - 1 ) );
    
    % Calculate the distances
    D = createSparseDistanceMatrix( coords, vertices, threshold );
    
    % Find all the spots which are close to at least one surface
    [ spotIndices, ~ ] = find( D );
    spotIndices = unique( spotIndices );
        
    % Add them to the indices vector
    indices( spotIndices ) = indices( spotIndices ) + 1;

    % Update the waitbar
    waitbar( i / nChildSurfaces, hWaitbar );
        
end

% Close the waitbar
close( hWaitbar )

indices = indices > 0;
if numel( find( indices ) ) == 0
    uiwait( warndlg( 'No spots close to surfaces were found!', ...
    'Warning' , 'modal' ) );
    return
end

% Now create a new spot object with the spots close to surfaces
% (we make sure to convert coords back to single precision)
newSpotObject = conn.mImarisApplication.mFactory.CreateSpots;
newSpotObject.Set( single( coords( indices, : ) ), ...
    timepoints( indices ), radii( indices ) );
newSpotObject.SetColor( rand( 1 ), rand( 1 ), rand( 1 ), 0 );
newSpotObject.mName = ...
    [ vSpot.mName, ' close to ', vSurface.mName, ...
    ' [ d <= ', num2str( threshold ), ' ]' ];
conn.mImarisApplication.mSurpassScene.AddChild( newSpotObject );

% ==============================================================================

% Cancel callbask
    function cancelProcessing( hObject, ~ )
        mustStop = 1;
        delete( hObject );
    end

end
