function bsseSpotsFindInSurfaces(aImarisApplicationID)
%  Finds and extracts all spots contained in (the convex hull of) surfaces
%
%  Installation:
%
%  - Copy this file into the XTensions folder in the Imaris installation directory
%  - You will find this function in the Image Processing menu
%
%    <CustomTools>
%      <Menu>
%       <Submenu name="BSSE">
%        <Submenu name="Spots Functions">
%         <Item name="Find spots contained in surfaces" icon="Matlab">
%           <Command>MatlabXT::bsseSpotsFindInSurfaces(%i)</Command>
%         </Item>
%        </Submenu>
%       </Submenu>
%      </Menu>
%      <SurpassTab>
%        <SurpassComponent name="bpSpots">
%          <Item name="BSSE :: Find spots contained in surfaces" icon="Matlab" tooltip="Sort spots by number.">
%            <Command>MatlabXT::bsseSpotsFindInSurfaces(%i)</Command>
%          </Item>
%        </SurpassComponent>
%      </SurpassTab>
%    </CustomTools>
%
%  Copyright Aaron Ponti (FMI, BSSE) 2008, 2014

% Create an ImarisConnector object
conn = IceImarisConnector(aImarisApplicationID);

% Check that a surpass scene exists
if isempty(conn.mImarisApplication.GetSurpassScene())
    uiwait(errordlg('The surpass scene is empty!', 'Error', 'modal'));
    return
end

% The selected object must be a spot
vSpot = conn.getSurpassSelection('Spots');
if isempty(vSpot)
    uiwait(errordlg('Please select a ''Spots'' object!', 'Error', 'modal'));
    return
end

% Get all surfaces and ask the user to pick those to be processed
surfaces = conn.getAllSurpassChildren(1, 'Surfaces');
nSurfaces = numel(surfaces);

if nSurfaces == 0
    uiwait(errordlg('At least one ''Surfaces'' object is needed!', ...
        'Error', 'modal'));
    return
end

% If there is more than one surface, we ask the user to choose which ones
% to use (more than one possible)
if nSurfaces > 1
    names = cell(1, nSurfaces);
    for i = 1 : nSurfaces
        names{i} = char(surfaces{i}.GetName());
    end
    
    [s, v] = listdlg('PromptString', 'Select the surface(s) to process...',...
        'SelectionMode', 'multiple', 'ListString', names, ...
        'ListSize', [200 300]);
    if v == 0
        return
    end
    surfaces = surfaces(s);
else
    surfaces = surfaces(1);
end
nSurfaces = numel(surfaces);

% Ask the user which algorithm he wants to use
answer = questdlg('Test for containment using which algorithm?', ...
    'Question', 'Surface mask', 'Surface normals', 'Surface mask');
switch answer,
    case 'Surface normals',
        algorithm = 'normals';
    case 'Surface mask',
        algorithm  = 'mask';
    otherwise,
        return
end % switch

% Ask the user to pick a folder where to save the results
OUTPUTDIR = uigetdir('.', ...
    'Please choose directory where to save your results');
if isequal(OUTPUTDIR, 0)
    return
end

% Get the spot coordinates
vSpotValues = vSpot.Get();
coords = vSpotValues.mPositionsXYZ;
if isempty(coords)
    uiwait(errordlg(['The selected ''Spots'' object does not ', ...
         'contain any positions!'], ...
        'Error', 'modal'));
    return
end
coords = double(coords);
timepoints = vSpotValues.mIndicesT;
radii = vSpotValues.mRadii; 

% Allocate space to store the results
nChildSurfacesTotal = 0;
res = cell(1, nSurfaces);
for i = 1 : nSurfaces
    nChildSurfaces = surfaces{i}.GetNumberOfSurfaces;
    nChildSurfacesTotal = nChildSurfacesTotal + nChildSurfaces;
    res{i} = cell(1, nChildSurfaces);
end

% Store distance information
allDist  = inf(size(coords, 1), 1);
surfacesID = cell(size(coords, 1), 1);
surfacesChildID = zeros(size(coords, 1), 1);

% Open a waitbar
mustStop = 0;

hWaitbar = waitbar(0, 'Processing surfaces...', ...
    'CreateCancelBtn', {@cancelProcessing});

% Check for spot containment in surfaces

% In case the algorithm is 'mask', we need to do some preparation
if strcmp(algorithm, 'mask')
    
    % Get the dataset extends and size
    extends = conn.getExtends();
    sizes = conn.getSizes();
    
    % Map spot coords from units to voxels
    coordsVoxels = round(conn.mapPositionsUnitsToVoxels(coords));
    
end

% Go over all individual surfaces in the Surfaces object and check how
% many spots are contained in each
n = 0;
for i = 1 : nSurfaces
    
    for j = 1 : surfaces{i}.GetNumberOfSurfaces()
        
        if mustStop == 1
            uiwait(warndlg('Interrupted by user', 'Info'), 'modal');
            return
        end

        % In case the algorithm is 'mask', we get the mask
        if strcmp(algorithm, 'mask')
            
            % Create a full mask
            mask = surfaces{i}.GetSingleMask(j - 1, ...
                extends(1), extends(3), extends(5), ...
                extends(2), extends(4), extends(6), ...
                sizes(1), sizes(2), sizes(3));
            mask = conn.getDataVolume(0, 0, mask);
            
        end
        
        % Get vertices
        vertices = double(surfaces{i}.GetVertices(j - 1));
        
        % Get normals
        normals = double(surfaces{i}.GetNormals(j - 1));
        
        % Calculate the distances to the spot coordinates
        D = createDistanceMatrix(coords, vertices);
        
        % Now go over all spots, find the closest surface edge, and
        % check if they are contained in the surface
        nSpotsInside = 0;
        for k = 1 : size(D, 1)
            
            % Find the closest vertex
            row = D(k, :);
            indxMinD = find(row == min(row), 1);
            
            switch algorithm
                
                case 'normals',
                    
                    % Check the sign of the dot product of the vertex normal to
                    % the spot to edge vector. If it is negative, the spot is
                    % inside
                    vertex = vertices(indxMinD, :);
                    v = coords(k, :) - vertex;
                    d = dot(v, normals(indxMinD, :));
                    if d <= 0
                        nSpotsInside = nSpotsInside + 1;
                    end
                    
                case 'mask',
                    
                    % Check whether the coords are in the white part of the
                    % mask
                    curr = coordsVoxels(k, :);
                    d = 1;
                    if mask(curr(1), curr(2), curr(3)) ~= 0
                        nSpotsInside = nSpotsInside + 1;
                        d = -1; % Mark this as being inside
                    end
                    
                otherwise,
                    
                    error('Bad value for algorithm');
            end
            
            % Store all distances. If one spot was inside, we make the
            % distance negative
            currentD = row(indxMinD);
            if d <= 0
                currentD = -1 .* currentD;
            end
            if currentD < allDist(k)
                allDist(k) = currentD;
                surfacesID{k} = char(surfaces{i}.GetName());
                surfacesChildID(k) = j;
            end
        end
        
        % Store
        res{i}{j} = nSpotsInside;
        
        % Update the waitbar
        n = n + 1;
        waitbar(n / nChildSurfacesTotal, hWaitbar);
    end
    
end

% Close the waitbar
close(hWaitbar)

% Now create a new spot object with objects contained in the surface(s)
conn.createAndSetSpots(single(coords(allDist <= 0, :)), ...
    timepoints(allDist <= 0), radii(allDist <= 0), ...
    [char(vSpot.GetName()), ' inside surfaces'], [rand(1, 3), 0]);


% Export the results to file
% -------------------------------------------------------------------------

% Prepare report
tmpfile = [OUTPUTDIR, filesep, char(vSpot.GetName()), '_', ...
    algorithm, '_numSpotsPerSurface.txt'];
fid = fopen(tmpfile, 'w');
if fid == -1
    disp('Could not create report file. Writing to console.');
    fid = 1;
end

% Write header
fprintf(fid, ...
    [...
    '%16s\t', ...    % Surface
    '%16s\t', ...    % Surface Child ID
    '%16s\n'], ...  % Number of Spots
    'Surface', ...
    'Surface Child ID', ...
    'Number of Spots');

% Write results
for i = 1 : numel(res)
    
    for j = 1 : numel(res{i})
        
        fprintf(fid, '%16s\t%16d\t%16d\n', ...
            char(surfaces{i}.GetName()), j - 1, res{i}{j});
        
    end
    
end

% Close the file and display the result
if fid ~= 1
    fclose(fid);
    edit(tmpfile);
end

% Export the distances
    
% Prepare report
tmpfile = [OUTPUTDIR, filesep, char(vSpot.GetName()), '_', ...
    algorithm, '_allSpotDistances.txt'];
fid = fopen(tmpfile, 'w');
if fid == -1
    disp('Could not create report file. Writing to console.');
    fid = 1;
end

% Write header
fprintf(fid, ...
    [...
    '%16s\t', ...    % Spot ID
    '%16s\t', ...    % Surface ID
    '%16s\t', ...    % Surface Child ID
    '%16s\t', ...    % Distance
    '%16s\n'], ...  % Is inside
    'Spot ID', ...
    'Surface ID', ...
    'Surface Child ID', ...
    'Distance', ...
    'Is inside');

% Write results
for i = 1 : numel(allDist)
    fprintf(fid, '%16d\t%16s\t%16d\t%16.4f\t%16d\n', i - 1, ...
        surfacesID{i}, surfacesChildID(i) - 1, ...
        allDist(i, 1), allDist(i) <= 0);
end
            
% Close the file
if fid ~= 1
    fclose(fid);
end

% ==============================================================================

% Cancel callbask
    function cancelProcessing(hObject, ~)
        mustStop = 1;
        delete(hObject);
    end

end