function bsseSubtractColocChannel(vImarisApplication)
% Remove all voxel from one channel for which there is a coloc signal.
%
% Copyright Aaron Ponti, BSSE, ETH Zurich, 2014
%
% <CustomTools>
%  <Menu>
%   <Submenu name="BSSE">
%    <Submenu name="Dataset Functions">
%     <Submenu name="Subtract coloc channel">
%      <Item name="Run" icon="Matlab" tooltip="Remove all voxel from one channel for which there is a coloc signal.">
%       <Command>MatlabXT::bsseSubtractColocChannel(%i)</Command>
%      </Item>
%     </Submenu>
%    </Submenu>
%   </Submenu>
%  </Menu>
% </CustomTools>

% Instantiate an IceImarisConnector object
conn = IceImarisConnector(vImarisApplication);

% Is there something loaded?
if isempty(conn.mImarisApplication.GetDataSet())
    return
end

% Ask the user to pick the relevant channels
nChannels = conn.mImarisApplication.GetDataSet().GetSizeC();
channelNames = cell(1, nChannels);
for c = 1 : nChannels
    channelNames{c} = char(conn.mImarisApplication.GetDataSet().GetChannelName(c - 1));
end

% Select channel to be masked
[maskIndx, v] = listdlg('PromptString', ...
    'Please pick channel to be masked', ...
    'SelectionMode', 'single', ...
    'ListSize', [400 300], ...
    'ListString', channelNames);
if v == 0
    return
end

% Select coloc channel
[colocIndx, v] = listdlg('PromptString', ...
    'Please colocalization channel', ...
    'SelectionMode', 'single', ...
    'ListSize', [400 300], ...
    'ListString', channelNames);
if v == 0
    return
end

% Now process the channels
maskChannel = conn.getDataVolume(maskIndx - 1, 0);
colocChannel = conn.getDataVolume(colocIndx - 1, 0);

% Make the colocChannel binary
bwColocChannel = colocChannel == 0;

% Mask
maskedChannel = double(maskChannel) .* bwColocChannel;

% Cast back
maskedChannel = cast(maskedChannel, conn.getMatlabDatatype());

% Now push the result back
conn.mImarisApplication.GetDataSet().SetSizeC(nChannels + 1);
conn.setDataVolume(maskedChannel, nChannels, 0);
conn.mImarisApplication.GetDataSet().SetChannelName(nChannels, ...
    'Masked by coloc');
