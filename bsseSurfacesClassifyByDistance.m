function bsseSurfacesClassifyByDistance(aImarisApplicationID)
% Classifies the Surfaces by distance from a selected Surface.
%
% The distance limits are specified by the user via input dialogs. The
% classified Surfaces are copied to new Surfaces objects.
%
%    <CustomTools> 
%      <Menu> 
%       <Submenu name="BSSE">
%        <Submenu name="Surfaces Functions">
%         <Item name="Classify surfaces by distance from surface" icon="Matlab"> 
%          <Command>MatlabXT::bsseSurfacesClassifyByDistance(%i)</Command> 
%         </Item> 
%        </Submenu>
%       </Submenu>
%      </Menu> 
%      <SurpassTab> 
%        <SurpassComponent name="bpSurfaces"> 
%          <Item name="Classify surfaces by distance from surface" icon="Matlab"> 
%            <Command>MatlabXT::bsseSurfacesClassifyByDistance(%i)</Command> 
%          </Item> 
%        </SurpassComponent> 
%      </SurpassTab> 
%    </CustomTools> 
%
% Copyright (c) 2015, Aaron Ponti

% Set up connection between Imaris and MATLAB
conn = IceImarisConnector(aImarisApplicationID);

% Get the currently selected Surfaces object in the surpass scene
sourceSurfaces = conn.getSurpassSelection('Surfaces');
if isempty(sourceSurfaces)
    errordlg('Please select a Surfaces object!');
    return
end

% Get the Surfaces objects
surfaces = conn.getAllSurpassChildren(0, 'Surfaces');
nSurfaces = numel(surfaces);
if nSurfaces == 0
        errordlg('There are no other Surfaces in the scene!');
    return
end
if nSurfaces > 1
    % Ask the user to pick one surface
    vSurface = askUserToPickSurface(surfaces);
    if isempty(vSurface)
        return
    end
else
    vSurface = surfaces{ 1 };
end

% Target surface vertex coordinates (subsampled if requested)
surfaceVertexCoords = getAndSubsampleSurfaceVertes(vSurface);

% Ask the user to specify the distance limits to use
distanceLimits = askUserToSetDistanceLimits();
if isempty(distanceLimits)
    return
end

nSurfacesToProcess = sourceSurfaces.GetNumberOfSurfaces();
if nSurfacesToProcess > 1
    hWaitbar = waitbar(0, 'Calculating distances...');
end

% Process all the surfaces
indices = zeros(1, nSurfacesToProcess);
for s = 0 : nSurfacesToProcess - 1
    
    % Current surface vertex coordinates
    currentCoords = sourceSurfaces.GetVertices(s);

    % Calculate all spot distances from the measurement point
    D = createDistanceMatrix(double(currentCoords), surfaceVertexCoords);
    D = min(D(:));

    % Now classify the Surfaces by distance and create new Surfaces objects
    % containing the Surfaces that fall in current distance bin
    indices(s + 1) = find(distanceLimits >= D, 1) - 1;

    % Update waitbar
    if nSurfacesToProcess > 1
        hWaitbar = waitbar((s + 1) / nSurfacesToProcess, hWaitbar);
    end

end

% Close waitbar
if nSurfacesToProcess > 1
    close(hWaitbar);
end

% Get unique bins
bins = unique(indices);

% Number of new surfaces to create
nSurfacesToCreate = numel(bins);
if nSurfacesToCreate > 1
    hWaitbar = waitbar(0, 'Creating new Surfaces...');
end

% Create and add surfaces
for i = 1 : numel(bins)

    % Create a new Surfaces bject
    newSurfaces = conn.mImarisApplication.GetFactory().CreateSurfaces();

    % Get the indices of the surfaces that fall il current bin
    indx = find(indices == bins(i));

    % Name for the new surfaces object
    name = ['Surfaces ', num2str(distanceLimits(bins(i))), ...
        ' <= D < ', num2str(distanceLimits(bins(i) + 1)), ' [', ...
        num2str(numel(indx)), ']'];

    % Set the name
    newSurfaces.SetName(name);

    for j = 1 : numel(indx)
    
       newSurfaces.AddSurface( ...
           sourceSurfaces.GetVertices(indx(j) - 1), ...
           sourceSurfaces.GetTriangles(indx(j) - 1), ...
           sourceSurfaces.GetNormals(indx(j) - 1), ...
           0);
    end

    % Set the color
    newSurfaces.SetColorRGBA(conn.mapRgbaVectorToScalar([rand(1, 3) 0]));

    % Add the new Spots object to the container
    conn.mImarisApplication.GetSurpassScene().AddChild(newSurfaces, -1);
    
    % Update waitbar
    if nSurfacesToCreate > 1
        hWaitbar = waitbar(i / nSurfacesToCreate, hWaitbar);
    end

end

% Close waitbar
if nSurfacesToCreate > 1
    close(hWaitbar);
end

% ==== Helper functions ===================================================

% Ask the user to pick a surface from a list
function vSurface = askUserToPickSurface(surfaces)
nSurfaces = numel(surfaces);
surfaceNames = cell(1, nSurfaces);
for i = 1 : nSurfaces
    surfaceNames{i} = char(surfaces{i}.GetName());
end
[s, v] = listdlg( ...
    'Name', 'Question', ...
    'PromptString', ...
    'Choose surface from which the distances will be calculated', ...
    'SelectionMode', 'single', ...
    'ListSize', [400 300], ...
    'ListString', surfaceNames);
if v == 0
    vSurface = [];
    return;
end
vSurface = surfaces{ s };

% Ask the user to set the distance limits a surface from a list
% (we provide reasonable defaults)
function distanceLimits = askUserToSetDistanceLimits()
answer = inputdlg({'Please set distance limits:'}, 'Input', ...
    1, {'10, 20, 30, 40, 50'});
if isempty(answer)
    return;
end
distanceLimits = str2num(answer{1}); %#ok<ST2NM>
if isempty(distanceLimits)
    errordlg('Invalid distance limits!');
    distanceLimits = [];
    return;
end
distanceLimits = [0 distanceLimits Inf];

% -------------------------------------------------------------------------

function surfaceVertexCoords = getAndSubsampleSurfaceVertes(vSurface)

% Get all vertices for the selected surface
surfaceVertexCoords = [];
for i = 0 : vSurface.GetNumberOfSurfaces() - 1
    surfaceVertexCoords = cat(1, surfaceVertexCoords, vSurface.GetVertices(i));
end
vNumberOfVertices = size(surfaceVertexCoords, 1);

nEntries = fix(log10(vNumberOfVertices)) + 2;

% Prepare the listdlg
numberOfTriangleList = zeros(1, nEntries);
numberOfTriangleList(1) = 1;
numberOfTriangleList(nEntries) = vNumberOfVertices;
str = cell(1, nEntries);
str{1} = 'Use center of mass';
for i = 2 : nEntries - 1
    numberOfTriangleList(i) = 10 ^ (i - 1);
    str{i} = ['Use ', num2str(numberOfTriangleList(i)), ' triangles'];
end
str{nEntries} = ['Use ', num2str(vNumberOfVertices), ' (all) triangles'];
[s, v] = listdlg('PromptString', ...
    'Sub-sample surface:', ...
    'SelectionMode', 'single', ...
    'ListString', str);
if v == 0
    return
end
MAX_SURFACE_VERTICES = numberOfTriangleList(s);

% Sub-sample the surface(s)
if MAX_SURFACE_VERTICES == 1
    
    % Get the centers of mass
    allSurfaceVertexCoords = [];
    for i = 1 : vSurface.GetNumberOfSurfaces
        allSurfaceVertexCoords = cat(1, ...
            allSurfaceVertexCoords, vSurface.GetCenterOfMass(i - 1));
    end
    surfaceVertexCoords = double(mean(allSurfaceVertexCoords, 1));
    
else
    
    % Limit the number of surface vertices used to MAX_SURFACE_VERTICES
    if vNumberOfVertices > MAX_SURFACE_VERTICES
        vIndices = round(linspace(1, vNumberOfVertices, MAX_SURFACE_VERTICES));
        surfaceVertexCoords = surfaceVertexCoords(vIndices, :);
    end
    
    % Make sure the coordinates are of type 'double', or createDistanceMatrix
    % will fail
    surfaceVertexCoords = double(surfaceVertexCoords);

end

