function bsseSurfacesMorphologicalOperations(aImarisApplicationID)
% Classifies the Surfaces by distance from a selected Surface.
%
% The distance limits are specified by the user via input dialogs. The
% classified Surfaces are copied to new Surfaces objects.
%
%    <CustomTools> 
%      <Menu> 
%       <Submenu name="BSSE">
%        <Submenu name="Surfaces Functions">
%         <Item name="Shrink or expand surface mask" icon="Matlab"> 
%          <Command>MatlabXT::bsseSurfacesMorphologicalOperations(%i)</Command> 
%         </Item> 
%        </Submenu>
%       </Submenu>
%      </Menu> 
%      <SurpassTab> 
%        <SurpassComponent name="bpSurfaces"> 
%          <Item name="Shrink or expand surface mask" icon="Matlab"> 
%            <Command>MatlabXT::bsseSurfacesMorphologicalOperations(%i)</Command> 
%          </Item> 
%        </SurpassComponent> 
%      </SurpassTab> 
%    </CustomTools> 
%
% Copyright (c) 2015, Aaron Ponti

% Set up connection between Imaris and MATLAB
conn = IceImarisConnector(aImarisApplicationID);

% Get the currently selected Surfaces object in the surpass scene
surfaces = conn.getSurpassSelection('Surfaces');
if isempty(surfaces)
    uiwait(errordlg('Please select a Surfaces object!'));
    return
end

% Ask the user to provide the structuring element diameter
answer=inputdlg(...
    'Radius of the structuring element:', ...
    'Input requested', ...
    1, ...
    {'1'});
if isempty(answer)
    return
end
radius = str2double(answer{1});
if isnan(radius) || radius < 1
   uiwait(errordlg('Please enter a valid radius!'));
   return;
end

% Ask the user if he wants to filate or erode
choice = questdlg('Which operation do you want to apply?', ...
    'Input requested', 'Erosion', 'Dilation', 'Erosion');
if isempty(choice)
    return
end

if strcmp(choice, 'Erosion') == 1
    operation = @imerode;
else
    operation = @imdilate;
end

% Ask the user to pick the file where to save the results
[fName, dirName] = uiputfile('volumes.txt', 'Save results as...');
if isequal(fName, 0)
    return
end
resultFName = fullfile(dirName, fName);

% Get the dataset extents and size
[minX, maxX, minY, maxY, minZ, maxZ] = conn.getExtends();
[sizeX, sizeY, sizeZ] = conn.getSizes();

% Get the voxel size
[vX, vY, vZ] = conn.getVoxelSizes();

% Volume of one voxel
voxelVolume = vX * vY * vZ;

% Get the mask
nSurfaces = surfaces.GetNumberOfSurfaces();

% Dataset datatype
datatype = conn.getMatlabDatatype();

% Structuring element
se = strel('disk', radius, 4);

% Initialize waitbar
hWaitbar = waitbar(0, 'Processing...');

% Allocate space to store results
volumes= zeros(nSurfaces, 2);

nSteps = 3 * nSurfaces;

step = 0;
for i = 1 : nSurfaces
    
    % GEt the binary mask
    aMask = surfaces.GetSingleMask(i - 1, ...
        minX, minY, minZ, ...
        maxX, maxY, maxZ, ...
        sizeX, sizeY, sizeZ);

    out = conn.getDataVolume(0, 0, aMask);

    % Estimate the volume before operation
    volume = 0;
    for j = 1 : size(out, 3)
        plane = out(:, :, j);
        volume = volume + voxelVolume .* sum(plane(:));
    end
    volumes(i, 1) = volume;
    
    % Erode on all sizes
    for j = 1 : size(out, 3)
       
        plane = out(:, :, j);
        
        if sum(plane(:)) == 0
            continue;
        end
        
        out(:, :, j) = operation(plane, se);
        
    end
    
    % Update waitbar
    step = step + 1;
    waitbar(step/nSteps, hWaitbar);
    
    out = permute(out, [2 3 1]);
    
    for j = 1 : size(out, 3)
       
        plane = out(:, :, j);
        
        if sum(plane(:)) == 0
            continue;
        end
        
        out(:, :, j) = operation(plane, se);
        
    end
    
    % Update waitbar
    step = step + 1;
    waitbar(step/nSteps, hWaitbar);

    out = permute(out, [3 1 2]);
    
    for j = 1 : size(out, 3)
       
        plane = out(:, :, j);
        
        if sum(plane(:)) == 0
            continue;
        end
        
        out(:, :, j) = operation(plane, se);
        
    end

    % Update waitbar
    step = step + 1;
    waitbar(step/nSteps, hWaitbar);
    
    out = permute(out, [1 2 3]);
    
    % Estimate the volume after operation
    volume = 0;
    for j = 1 : size(out, 3)
        plane = out(:, :, j);
        volume = volume + voxelVolume .* sum(plane(:));
    end
    volumes(i, 2) = volume;

    % Cast
    out = cast(intmax(datatype) .* cast(out, datatype), datatype);

    % Push the channel back
    nChannels = conn.mImarisApplication.GetDataSet().GetSizeC();
    conn.mImarisApplication.GetDataSet().SetSizeC(nChannels + 1);

    conn.setDataVolume(out, nChannels, 0);

end

% Close the waitbar
close(hWaitbar);

fid = fopen(resultFName, 'w' );
if fid == -1
    disp( 'Could not create report file. Writing to console.' );
    fid = 1;
end

% Save the results to file
fprintf(fid, ...
    ['%12s\t', ...   % Surface ID
    '%12s\t', ...    % Volume before
    '%12s\n'], ...   % Volume after
    'Surface ID', ...
    'Volume before', ...
    'Volume after');


% Write the results
for i = 1 : size(volumes, 1)

    fprintf(fid, ...
    ['%12d\t', ...    % Surface ID
    '%12.4f\t', ...   % Volume before
    '%12.4f\n'], ...  % Volume after
    (i - 1), ...
    volumes(i, 1), ...
    volumes(i, 2));

end

% Close the file
fclose(fid);


